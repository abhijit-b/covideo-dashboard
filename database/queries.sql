CREATE USER 'covideo'@'localhost' IDENTIFIED BY 'd@em0n51';
GRANT ALL PRIVILEGES ON covideo_dashboard.* TO 'covideo'@'localhost';
FLUSH PRIVILEGES;


CREATE TABLE sms_codes (
  id int(11) NOT NULL AUTO_INCREMENT,
  phone_number int(11) DEFAULT NULL,
  code int(11) DEFAULT NULL,
  is_verified int(5) DEFAULT 0,
  sent_on int(11),
  PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE `campaigns` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `code` varchar(40) DEFAULT NULL,
  `list_group_id` int(11) DEFAULT NULL,
  `sms_template_id` int(11) DEFAULT NULL,
  `forward_url` varchar(255) DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

CREATE TABLE `csv_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `csv_filename` varchar(255) DEFAULT NULL,
  `csv_header` varchar(255) DEFAULT NULL,
  `csv_data` longtext,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

CREATE TABLE `list_groups` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

INSERT INTO `list_groups` VALUES (1,1,'my-sample-test-list',1551345834,1551345834),(16,1,'mitul and mukesh',1588167649,1588167649),(17,1,'Mumbai-CC-Covideo-test',1588169558,1588169558),(18,1,'CoVideo-SMS-Database-30th-April----Sheet1',1588203618,1588203618),(19,1,'CoVideo-SMS-May-1st-Pus-Entrx---Sheet1',1588327093,1588327093),(20,1,'CoVideo-SMS-May-1st-(6-pm)-Pus-Entrx----Sheet1',1588336098,1588336098),(21,1,'CoVideo-SMS-May-1st-(6-pm)-Pus-Entrx----Sheet1-(1)',1588338496,1588338496),(22,1,'MM-02May2020-1_4000',1588424912,1588424912),(23,1,'MM-02May2020-4001_8000',1588425082,1588425082),(24,1,'MM-02May2020-8001_End',1588425171,1588425171),(25,1,'whatsapp_enabled_numbers_7may_730pm',1588860148,1588860148),(26,1,'Doctor-Number-11-may-2020',1589205730,1589205730);

CREATE TABLE `lists` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) DEFAULT NULL,
  `first_name` varchar(255) DEFAULT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  `mobile_number` varchar(255) DEFAULT NULL,
  `amount` varchar(255) DEFAULT NULL,
  `code` varchar(20) DEFAULT NULL,
  `vars` longtext,
  `created_date` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

CREATE TABLE `sms_credits` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sender_id` varchar(255) DEFAULT NULL,
  `promotional` double(15,2) DEFAULT NULL,
  `transactional` double(15,2) DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

INSERT INTO `sms_credits` VALUES (1,'KYCBOT',0.00,99.00,1553768466,1553768466),(3,'ALTIUS',0.00,363582.00,1554543093,1554543093),(4,'TESTBK',NULL,NULL,1553677845,1553677845),(5,'COVDEO',10.00,52205.00,1589211713,1589211713);

CREATE TABLE `sms_sender_id` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `sender_id` varchar(255) DEFAULT NULL,
  `created_date` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

INSERT INTO `sms_sender_id` VALUES (1,1,'TATSTL',1554470355);

CREATE TABLE `sms_templates` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `message` text,
  `created_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `sms_templates` VALUES (1,1,'Covideo.in Test','Welcome to Covideo.in! Human Connections Simplified !',1588165475),(2,1,'Covideo first sms blast','CoVideo is coming soon to keep you connected. No downloads. No installs. No frustrations. India\'s very own secure video & audio service to help you stay connected. Sign up for early access to be the 1st to get invited & win over Rs. 10,000 in Gold & prizes. Click https://go.covideo.in/erlyxstxt now to get on the list.',1588169436),(3,1,'CoVideo SMS 160 ','India\'s very own secure video service. Join now to be the 1st to get invited. Win over Rs.10,000 in prizes. Click to join https://go.covideo.in/erlyxstx ',1588173306),(4,1,'covideo sms blast small','CoVideo is coming soon. India\'s very own secure video service without downloads. Click https://go.covideo.in/erlyxstxt & win over Rs. 10,000 in prizes.',1588173481),(5,1,'CoVideo SMS 160 (B) ','Join India\'s very own secure video service. Be the 1st to get invited & Win over Rs.10,000 in prizes. Click now to join https://go.covideo.in/erlyxstx2',1588327619),(6,1,'CoVideo SMS 160 (C)','Check out this #MadeinIndia multi-lingual video conferencing service. Join CoVideo FREE. Win Rs.10,000 in prizes. Click now https://go.covideo.in/erlyxstx2',1588855595);


CREATE TABLE `tracking` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ip` varchar(255) DEFAULT NULL,
  `code` varchar(255) DEFAULT NULL,
  `useragent` varchar(255) DEFAULT NULL,
  `created_date` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `tracking_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(255) DEFAULT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  `mobile_number` varchar(255) DEFAULT NULL,
  `amount` varchar(255) DEFAULT NULL,
  `code` varchar(20) DEFAULT NULL,
  `group_id` varchar(255) DEFAULT NULL,
  `ip` varchar(255) DEFAULT NULL,
  `user_agent` varchar(255) DEFAULT NULL,
  `created_date` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE plans (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `planName` varchar(255) DEFAULT NULL,
  `usersPerRoom` int(10) DEFAULT NULL,
  `talkTime` int(10) DEFAULT NULL,
  `data` varchar(255) DEFAULT NULL,
  `created_date` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

free - 8 - 120mints talk time - 5gb data
paid - 12 - 200mint talk time - 15gb data

create table feedback (
  id int(11) NOT NULL AUTO_INCREMENT,
  username varchar(255),
  joiningExp int(10),
  audioQuality int(10),
  videoQuality int(10),
  overallExp int(10),
  wouldYou varchar(100),
  feedback text,
  params text,
  created_at int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

alter table users add signup_type varchar(255) after email;
alter table users add profile_image_url text after email;
alter table users add invite_code varchar(255) after signup_type;
alter table users add phonecode varchar(50) after company;
alter table users add code varchar(255) after signup_type;

create table login_activity (
  id int(11) NOT NULL AUTO_INCREMENT,
  userid varchar(255),
  ip varchar(50),
  user_agent text,
  created_at int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

create table drip_api_responses (
  id int(11) NOT NULL AUTO_INCREMENT,
  userid varchar(255),
  dripid varchar(255),
  link varchar(255),
  status varchar(50),
  ip varchar(50),
  user_agent text,
  created_at int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


create table domain_master (
  id int(11) NOT NULL AUTO_INCREMENT,
  userid varchar(255),
  domain_name varchar(255),
  site_end_point varchar(255),
  description varchar(255),
  link varchar(255),
  created_at int(11),
  is_active varchar(20),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO domain_master (userid, domain_name, site_end_point, description, created_at, is_active) VALUES (1, 'our.covideo.in', 'dev.covideo.in', 'CoVideo India is created to take the frustrations out of human connections. An unbelievably easy, streamlined video and audio platform.', unix_timestamp(), 'active');

INSERT INTO domain_master (userid, domain_name, site_end_point, description, created_at, is_active) VALUES (1, 'our.covideo.in', 'signup.covideo.in', 'CoVideo India is created to take the frustrations out of human connections. An unbelievably easy, streamlined video and audio platform.', unix_timestamp(), 'active');


create table room_scheduler (
  id int(11) NOT NULL AUTO_INCREMENT,
  userid varchar(255),
  roomid varchar(50),
  meeting_title varchar(255),
  meeting_start_date varchar(50),
  meeting_start_time varchar(50),
  meeting_duration varchar(50),
  meeting_description varchar(255),
  number_of_participants varchar(50),
  created_at int(11),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
