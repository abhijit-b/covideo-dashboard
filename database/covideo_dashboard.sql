-- MySQL dump 10.13  Distrib 8.0.18, for osx10.15 (x86_64)
--
-- Host: localhost    Database: covideo_dashboard
-- ------------------------------------------------------
-- Server version	8.0.18

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `email_reports`
--

DROP TABLE IF EXISTS `email_reports`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `email_reports` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `campaign_id` int(11) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `is_open` int(11) NOT NULL DEFAULT '0',
  `is_sent` int(11) NOT NULL DEFAULT '0',
  `is_bounce` int(11) NOT NULL DEFAULT '0',
  `is_in_queue` int(1) NOT NULL DEFAULT '0',
  `sent_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `email_reports`
--

LOCK TABLES `email_reports` WRITE;
/*!40000 ALTER TABLE `email_reports` DISABLE KEYS */;
/*!40000 ALTER TABLE `email_reports` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `groups`
--

DROP TABLE IF EXISTS `groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `groups` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `description` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `groups`
--

LOCK TABLES `groups` WRITE;
/*!40000 ALTER TABLE `groups` DISABLE KEYS */;
/*!40000 ALTER TABLE `groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `keys`
--

DROP TABLE IF EXISTS `keys`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `keys` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `key` varchar(40) NOT NULL,
  `level` int(2) NOT NULL,
  `ignore_limits` tinyint(1) NOT NULL DEFAULT '0',
  `is_private_key` tinyint(1) NOT NULL DEFAULT '0',
  `ip_addresses` text,
  `date_created` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `keys`
--

LOCK TABLES `keys` WRITE;
/*!40000 ALTER TABLE `keys` DISABLE KEYS */;
/*!40000 ALTER TABLE `keys` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `login_attempts`
--

DROP TABLE IF EXISTS `login_attempts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `login_attempts` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `ip_address` varchar(45) NOT NULL,
  `login` varchar(100) NOT NULL,
  `time` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `login_attempts`
--

LOCK TABLES `login_attempts` WRITE;
/*!40000 ALTER TABLE `login_attempts` DISABLE KEYS */;
/*!40000 ALTER TABLE `login_attempts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `master_config`
--

DROP TABLE IF EXISTS `master_config`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `master_config` (
  `config_id` int(11) NOT NULL AUTO_INCREMENT,
  `config_group` varchar(255) DEFAULT NULL COMMENT 'CONSTANT GROUP: WEBSITE,IMAGE,THIRDPARTY',
  `config_title` varchar(255) DEFAULT NULL COMMENT 'CONSTANT TITLE for Admin',
  `config_key` varchar(255) DEFAULT NULL COMMENT 'CONSTANT NAME',
  `config_val` mediumtext COMMENT 'CONSTANT VALUE',
  `config_desc` mediumtext COMMENT 'DESCRIPTION for Admin',
  `input_type` enum('text','textarea','password','checkbox','select','radio','file') DEFAULT 'text',
  `input_options` mediumtext,
  `allow_to_disable` int(1) DEFAULT '0',
  `is_disabled` int(1) DEFAULT '0',
  `u_date` int(11) DEFAULT NULL,
  `c_date` int(11) DEFAULT NULL,
  `admin_id` int(11) DEFAULT '0',
  PRIMARY KEY (`config_id`),
  KEY `config_group` (`config_group`),
  KEY `input_type` (`input_type`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `master_config`
--

LOCK TABLES `master_config` WRITE;
/*!40000 ALTER TABLE `master_config` DISABLE KEYS */;
INSERT INTO `master_config` VALUES (1,'DASHBOARD','URL Endpoint','URL_ENDPOINT','https://our.covideo.in','This is the production URL for Covideo.in meeting','text','',0,0,1543917000,1543917000,1),(3,'DASHBOARD','Config enum type','CONFIG_ENUM_TYPES','[\"text\",\"textarea\",\"password\",\"checkbox\",\"select\",\"radio\",\"file\"]','Options to used for type of config','select','',0,0,1544238900,1544238900,1),(8,'DASHBOARD','Email Service Provider','MAILCONNECT_API_KEY','bMhgFeRSrAO43wfKglveSiihS24nNkIfwcyh9f1slllX83azPa','API Key for making calls to the 3rd party email provider','text','',0,0,1545805401,1545805401,1),(10,'DASHBOARD','Email Service Provider','MAILCONNECT_API_URL','http://api.nestedlogics.net/API/','API URL for making calls to the 3rd party email provider','text','',0,0,1545805401,1545805401,1),(12,'DASHBOARD','Elastic Email API','ELASTICEMAIL_API_KEY','b94872fe-2ad4-43c0-a6b8-a608e0774008','API Key to make calls to elastic email','text','',0,0,1547632858,1547632858,1),(13,'DASHBOARD','Elastic Email API URL','ELASTICEMAIL_API_URL','https://api.elasticemail.com/v2/','API URL to make calls to Elastic Email','text','',0,0,1547632858,1547632858,1),(14,'DASHBOARD','ELASTICEMAIL PUBLIC ACCOUNT ID','ELASTICEMAIL_PUBLIC_ACCOUNT_ID','f912624d-671b-422d-9a9a-6f357e0cc65b','Public account id needed to add email addresses','text','',0,0,1547645900,1547645900,1),(16,'DASHBOARD','Google Maps API Key','GOOGLE_MAPS_API_KEY','AIzaSyAe5dyPPNaCweknQYnN9U7gzSbWsTBJdQw','api key to make geocoding api calls to google','text',NULL,0,0,1551094885,1551094885,1);
/*!40000 ALTER TABLE `master_config` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rest_api_keys`
--

DROP TABLE IF EXISTS `rest_api_keys`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `rest_api_keys` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `key` varchar(40) NOT NULL,
  `level` int(2) NOT NULL,
  `ignore_limits` tinyint(1) NOT NULL DEFAULT '0',
  `is_private_key` tinyint(1) NOT NULL DEFAULT '0',
  `ip_addresses` text,
  `date_created` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rest_api_keys`
--

LOCK TABLES `rest_api_keys` WRITE;
/*!40000 ALTER TABLE `rest_api_keys` DISABLE KEYS */;
INSERT INTO `rest_api_keys` VALUES (1,1,'Y6SfPM4ysrxJ5BzciW3GK2XRjDoAnOQuh0pbZtTg',10,0,0,NULL,0);
/*!40000 ALTER TABLE `rest_api_keys` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rest_api_logs`
--

DROP TABLE IF EXISTS `rest_api_logs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `rest_api_logs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uri` varchar(255) NOT NULL,
  `method` varchar(6) NOT NULL,
  `params` text,
  `api_key` varchar(40) NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `time` int(11) NOT NULL,
  `rtime` float DEFAULT NULL,
  `authorized` varchar(1) NOT NULL,
  `response_code` smallint(3) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rest_api_logs`
--

LOCK TABLES `rest_api_logs` WRITE;
/*!40000 ALTER TABLE `rest_api_logs` DISABLE KEYS */;
/*!40000 ALTER TABLE `rest_api_logs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rest_logs_table`
--

DROP TABLE IF EXISTS `rest_logs_table`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `rest_logs_table` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `domain` varchar(255) DEFAULT NULL,
  `uri` varchar(255) NOT NULL,
  `method` varchar(6) NOT NULL,
  `params` text,
  `api_key` varchar(40) NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `time` int(11) NOT NULL,
  `rtime` float DEFAULT NULL,
  `authorized` varchar(1) NOT NULL,
  `response_code` smallint(3) DEFAULT '0',
  `response` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rest_logs_table`
--

LOCK TABLES `rest_logs_table` WRITE;
/*!40000 ALTER TABLE `rest_logs_table` DISABLE KEYS */;
/*!40000 ALTER TABLE `rest_logs_table` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `room_master`
--

DROP TABLE IF EXISTS `room_master`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `room_master` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `room_type` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `room_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `language` varchar(255) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `params` text CHARACTER SET utf8 COLLATE utf8_general_ci,
  `created` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `room_master`
--

LOCK TABLES `room_master` WRITE;
/*!40000 ALTER TABLE `room_master` DISABLE KEYS */;
/*!40000 ALTER TABLE `room_master` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sms_reports`
--

DROP TABLE IF EXISTS `sms_reports`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sms_reports` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `campaign_id` int(11) DEFAULT NULL,
  `mobile_number` varchar(255) DEFAULT NULL,
  `message_id` varchar(255) DEFAULT NULL,
  `message` tinytext,
  `status` varchar(255) DEFAULT NULL,
  `shortcode` varchar(255) DEFAULT NULL,
  `code` varchar(255) DEFAULT NULL,
  `vars` varchar(255) DEFAULT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `sms_reports_status` (`status`),
  KEY `sms_reports_code` (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sms_reports`
--

LOCK TABLES `sms_reports` WRITE;
/*!40000 ALTER TABLE `sms_reports` DISABLE KEYS */;
/*!40000 ALTER TABLE `sms_reports` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `users` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `ip_address` varchar(45) NOT NULL,
  `userid` varchar(255) DEFAULT NULL,
  `fullname` varchar(255) DEFAULT NULL,
  `username` varchar(100) DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `salt` varchar(255) DEFAULT NULL,
  `email` varchar(254) NOT NULL,
  `activation_code` varchar(40) DEFAULT NULL,
  `forgotten_password_code` varchar(40) DEFAULT NULL,
  `forgotten_password_time` int(11) unsigned DEFAULT NULL,
  `remember_code` varchar(40) DEFAULT NULL,
  `created_on` int(11) unsigned NOT NULL,
  `last_login` int(11) unsigned DEFAULT NULL,
  `active` tinyint(1) unsigned DEFAULT '1',
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `company` varchar(100) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `type` enum('user','admin','super') DEFAULT NULL,
  `entity_id` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'127.0.0.1',NULL,'Administrator','admin@admin.com','4d27b7677bd96f7ba00c4bd0541c9588',NULL,'admin@admin.com','Jk8mtuR2yGWw',NULL,NULL,NULL,11012018,1587922770,1,'Admin',' istrator',NULL,NULL,'super','1');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users_groups`
--

DROP TABLE IF EXISTS `users_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `users_groups` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned NOT NULL,
  `group_id` mediumint(8) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uc_users_groups` (`user_id`,`group_id`),
  KEY `fk_users_groups_users1_idx` (`user_id`),
  KEY `fk_users_groups_groups1_idx` (`group_id`),
  CONSTRAINT `fk_users_groups_groups1` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`) ON DELETE CASCADE,
  CONSTRAINT `fk_users_groups_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users_groups`
--

LOCK TABLES `users_groups` WRITE;
/*!40000 ALTER TABLE `users_groups` DISABLE KEYS */;
/*!40000 ALTER TABLE `users_groups` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-04-26 23:25:06
