<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>CoVideo.in - <?php echo $title; ?></title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="icon" href="<?php echo base_url(); ?>favicon.ico"/>
    <link rel="icon" href="<?php echo base_url(); ?>assets/images/cropped-Covideo-Website-Favicon-32x32.png" sizes="32x32" />
    <link rel="icon" href="<?php echo base_url(); ?>assets/images/cropped-Covideo-Website-Favicon-192x192.png" sizes="192x192" />
    <link rel="apple-touch-icon" href="<?php echo base_url(); ?>assets/images/cropped-Covideo-Website-Favicon-180x180.png" />

    <!-- Font Awesome -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- overlayScrollbars -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/adminlte.min.css">

    <!-- custom css -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/style.css">
    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Open Sans" rel="stylesheet">

    <!-- DataTables -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/responsive.bootstrap4.min.css">

    <!-- Social login scripts -->
    <script src="<?php echo base_url(); ?>assets/js/facebook-login.js"></script>
    <script src="https://apis.google.com/js/platform.js?onload=init" async defer></script>

    <!-- jQuery -->
    <script src="<?php echo base_url(); ?>assets/js/jquery.min.js"></script>
    <!-- Bootstrap 4 -->
    <script src="<?php echo base_url(); ?>assets/js/bootstrap.bundle.min.js"></script>
    <!-- AdminLTE App -->
    <script src="<?php echo base_url(); ?>assets/js/adminlte.min.js"></script>

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-165629866-2"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-165629866-2');
    </script>
    <!-- Hey Oliver -->
<script type='text/javascript'>
     var _hoid = _hoid || []; _hoid.push('ho_5NCcexY6y9EaJuPFjmsvTphRZ7SdwBz1Vf4GqXDtk2WU3nH');
     var heyopath = (('https:' == document.location.protocol) ? 'https://www.heyoliver.com/webroot/ho-ui/v2/' :
     'http://www.heyoliver.com/webroot/ho-ui/v2/');
     var heyop = (('https:' == document.location.protocol) ? 'https://' : 'http://');
     var heyospt = document.createElement('script'); heyospt.type = 'text/javascript';
     heyospt.async = true; heyospt.src = heyopath + 'ho2.js';
     var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(heyospt, s);
     </script>
 <!-- End of Hey Oliver  -->
</head>

<body class="hold-transition sidebar-mini">
    <!-- Site wrapper -->
    <div class="wrapper">
        <!-- Navbar -->
        <nav class="main-header navbar navbar-expand navbar-orange navbar-light">
            <!-- Left navbar links -->
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
                </li>
            </ul>
            <ul class="navbar-nav ml-auto">
                <li class="nav-item dropdown">
                    <a class="nav-link" data-toggle="dropdown" href="#" style="background-color: #ffffff; text-decoration: none; color:#000000;">
                    <?php echo $this->lang->line("menu_language"); ?>
                    </a>
                    <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
                        <div class="dropdown-divider"></div>
                        <a href="/language/switch/english" class="dropdown-item">
                            English
                        </a>
                        <div class="dropdown-divider"></div>
                        <a href="/language/switch/hindi" class="dropdown-item">
                            Hindi
                        </a>
                        <div class="dropdown-divider"></div>
                        <a href="/language/switch/marathi" class="dropdown-item">
                            Marathi
                        </a>
                        <div class="dropdown-divider"></div>
                        <a href="/language/switch/gujarati" class="dropdown-item">
                            Gujarati
                        </a>
                        <div class="dropdown-divider"></div>
                    </div>
                </li>
            </ul>
        </nav>
        <!-- /.navbar -->

        <!-- Main Sidebar Container -->
        <aside class="main-sidebar sidebar-light-orange elevation-4">
            <!-- Brand Logo -->
            <a href="/dashboard" class="site_title">
                <img class="img-fluid elevation-3" src="<?php echo base_url(); ?>assets/images/logo.png">
            </a>
            <br><br>
            <!-- Sidebar -->
            <div class="sidebar">
                <!-- Sidebar Menu -->
                <nav class="mt-2">
                    <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                        <!-- Add icons to the links using the .nav-icon class with font-awesome or any other icon font library -->
                        <li class="nav-item">
                            <a href="/dashboard" class="nav-link <?php if(strstr($_SERVER['REQUEST_URI'], 'dashboard')){ echo "active";}?>">
                                <i class="nav-icon fas fa-tachometer-alt"></i> 
                                <p><?php echo $this->lang->line("menu_mission_control"); ?></p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="/rooms" class="nav-link <?php if(strstr($_SERVER['REQUEST_URI'], 'rooms')){ echo "active";}?>">
                                <i class="nav-icon fas fa-comments"></i> 
                                <p><?php echo $this->lang->line("menu_my_rooms"); ?></p>
                            </a>
                        </li>
                        <!-- <li class="nav-item">
                            <a href="/subscriptions" class="nav-link <?php if(strstr($_SERVER['REQUEST_URI'], 'subscriptions')){ echo "active";}?>">
                                <i class="nav-icon fas fa-list-ul"></i> 
                                <p>Subscriptions</p>
                            </a>
                        </li> -->
                        <!-- <li class="nav-item">
                            <a href="/scheduler" class="nav-link <?php if(strstr($_SERVER['REQUEST_URI'], 'scheduler')){ echo "active";}?>">
                                <i class="nav-icon fas fa-calendar-alt"></i> 
                                <p>Scheduler</p>
                            </a>
                        </li> -->
                        <li class="nav-item">
                            <a href="/user/view/" class="nav-link <?php if(strstr($_SERVER['REQUEST_URI'], 'view')){ echo "active";}?>">
                                <i class="nav-icon fas fa-user"></i> 
                                <p><?php echo $this->lang->line("menu_my_profile"); ?></p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="/user/support/" class="nav-link <?php if(strstr($_SERVER['REQUEST_URI'], 'support')){ echo "active";}?>">
                                <i class="nav-icon fas fa-headset"></i> 
                                <p><?php echo $this->lang->line("menu_support"); ?></p>
                            </a>
                        </li>
                        <?php if (in_array($this->session->userdata('role'), array('admin', 'super'))) { ?>
                            <li class="nav-item">
                                <a href="/user" class="nav-link <?php if(strstr($_SERVER['REQUEST_URI'], 'user')){ echo "active";}?>">
                                    <i class="nav-icon fas fa-comments"></i> 
                                    <p>Users</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="/settings" class="nav-link <?php if(strstr($_SERVER['REQUEST_URI'], 'settings')){ echo "active";}?>">
                                    <i class="nav-icon fas fa-cog"></i> 
                                    <p>Settings</p>
                                </a>
                            </li>
                        <?php } ?>
                        <li class="nav-item" id="logout">
                            <a href="javascript:;" class="nav-link">
                                <i class="nav-icon fas fa-sign-out-alt"></i> 
                                <p><?php echo $this->lang->line("menu_signout"); ?></p>
                            </a>
                        </li>
                    </ul>
                </nav>
                <!-- /.sidebar-menu -->
            </div>
            <!-- /.sidebar -->
        </aside>

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">

            <!-- Main content -->
            <section class="content">
                <?php echo $contents;?>
            </section>
            <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->

        <footer class="main-footer">
            <div class="float-right d-none d-sm-block">
                <b><?php echo $this->lang->line("footer_version"); ?></b> 1.0.1
            </div>
            <?php echo $this->lang->line('footer_copyright'); ?>
        </footer>

    </div>
    <!-- ./wrapper -->

    <!-- DataTables -->
    <script src="<?php echo base_url(); ?>assets/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/dataTables.bootstrap4.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/dataTables.responsive.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/responsive.bootstrap4.min.js"></script>

    <!-- Custom Script -->
    <script>
        function init() {
            gapi.load('auth2', function() {
                gUser = gapi.auth2.init({
                    'client_id': '879497195593-730scmceuq7ee0eb5m6hfrffh346cbhs.apps.googleusercontent.com',
                    fetch_basic_profile: true,
                    scope: 'profile'
                });
            });
        }

        $('#logout').on('click', function(){
            FB.getLoginStatus(function(response) {
                if (response.status === 'connected') {
                    FB.logout(function(response) {
                        location.href = '/user/logout';
                    });
                }
            });
            if(typeof gUser !== "undefined") {
                if(gUser.isSignedIn.get()) {
                    gUser.signOut().then(function(){
                        location.href = '/user/logout';
                    });
                } else {
                    location.href = '/user/logout';
                }
            } else {
                location.href = '/user/logout';
            }
        });
    </script>

<!-- Drip -->
<script type="text/javascript">
  var _dcq = _dcq || [];
  var _dcs = _dcs || {};
  _dcs.account = '4406547';

  (function() {
    var dc = document.createElement('script');
    dc.type = 'text/javascript'; dc.async = true;
    dc.src = '//tag.getdrip.com/4406547.js';
    var s = document.getElementsByTagName('script')[0];
    s.parentNode.insertBefore(dc, s);
  })();
</script>
<!-- end Drip -->

</body>

</html>