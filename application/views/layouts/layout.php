<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>CoVideo.in - <?php echo $title; ?></title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="icon" href="<?php echo base_url(); ?>favicon.ico"/>
    <link rel="icon" href="<?php echo base_url(); ?>assets/images/cropped-Covideo-Website-Favicon-32x32.png" sizes="32x32" />
    <link rel="icon" href="<?php echo base_url(); ?>assets/images/cropped-Covideo-Website-Favicon-192x192.png" sizes="192x192" />
    <link rel="apple-touch-icon" href="<?php echo base_url(); ?>assets/images/cropped-Covideo-Website-Favicon-180x180.png" />

    <!-- Font Awesome -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- overlayScrollbars -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/adminlte.min.css">

    <!-- custom css -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/style.css">
    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Open Sans" rel="stylesheet">

    <!-- Social login scripts -->
    <script src="<?php echo base_url(); ?>assets/js/facebook-login.js"></script>
    <script src="https://apis.google.com/js/platform.js?onload=init" async defer></script>

    <!-- jQuery -->
    <script src="<?php echo base_url(); ?>assets/js/jquery.min.js"></script>
    <!-- Bootstrap 4 -->
    <script src="<?php echo base_url(); ?>assets/js/bootstrap.bundle.min.js"></script>
    <!-- AdminLTE App -->
    <script src="<?php echo base_url(); ?>assets/js/adminlte.min.js"></script>
    
</head>
<body class="hold-transition">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-3"></div>
            <div class="col-md-6">
                <div class="login-logo">
                    <img class="img-fluid" src="<?php echo base_url(); ?>assets/images/logo.png">
                </div>
                <?php echo $contents;?>
            </div>
            <div class="col-md-3"></div>
        </div>
    </div>
</body>
</html>
