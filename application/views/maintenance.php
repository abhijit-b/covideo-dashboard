<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>CoVideo.in - Site Under Maintenance</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="icon" href="/favicon.ico"/>
    <link rel="icon" href="/assets/images/cropped-Covideo-Website-Favicon-32x32.png" sizes="32x32" />
    <link rel="icon" href="/assets/images/cropped-Covideo-Website-Favicon-192x192.png" sizes="192x192" />
    <link rel="apple-touch-icon" href="/assets/images/cropped-Covideo-Website-Favicon-180x180.png" />

    <link rel="stylesheet" href="/assets/css/adminlte.min.css">
    <link href="https://fonts.googleapis.com/css?family=Open Sans" rel="stylesheet">

</head>
<body class="bg" style="background: linear-gradient(135deg, #ffa365 0%,#ffffff 20%,#ffffff 80%,#80c06f 100%);">
    <br><br><br><br>
    <h1 class="head text-center">Site Under Maintenance</h1>
    <div class="container">
    <div class="row">
        <div class="col-md-12 text-center">
            <div class="error-template">
                <br><br>
                <h1>:) Oops!</h1>
                <br><br>
                <h2>Temporarily down for maintenance</h2>
                <br><br>
                <h1>We’ll be back soon!</h1>
                <div>
                    <p>
                        Sorry for the inconvenience but we’re performing some maintenance at the moment.
                        we’ll be back online shortly!</p>
                    <p>
                        — The Team</p>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>