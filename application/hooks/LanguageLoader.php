<?php
defined('BASEPATH') or exit('No direct script access allowed');

class LanguageLoader
{

    public function __construct(){
        $this->ci =& get_instance();
        if(!isset($this->ci->session)){
              $this->ci->load->library('session');
        }
    }
    
    function initialize() {
        $this->ci->load->helper('language');

        $siteLang = get_cookie('site_lang', TRUE);
        if(empty($siteLang)) {
            $siteLang = $this->ci->session->userdata('site_lang');
        }
        if ($siteLang) {
            $this->ci->lang->load('content', $siteLang);
            $this->ci->session->set_userdata('site_lang', $siteLang);
        } else {
            $this->ci->lang->load('content', 'english');
            $this->ci->session->set_userdata('site_lang', 'english');
        }
    }
}
