<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| Display Debug backtrace
|--------------------------------------------------------------------------
|
| If set to TRUE, a backtrace will be displayed along with php errors. If
| error_reporting is disabled, the backtrace will not display, regardless
| of this setting
|
*/
defined('SHOW_DEBUG_BACKTRACE') OR define('SHOW_DEBUG_BACKTRACE', TRUE);

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
defined('FILE_READ_MODE')  OR define('FILE_READ_MODE', 0644);
defined('FILE_WRITE_MODE') OR define('FILE_WRITE_MODE', 0666);
defined('DIR_READ_MODE')   OR define('DIR_READ_MODE', 0755);
defined('DIR_WRITE_MODE')  OR define('DIR_WRITE_MODE', 0755);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/
defined('FOPEN_READ')                           OR define('FOPEN_READ', 'rb');
defined('FOPEN_READ_WRITE')                     OR define('FOPEN_READ_WRITE', 'r+b');
defined('FOPEN_WRITE_CREATE_DESTRUCTIVE')       OR define('FOPEN_WRITE_CREATE_DESTRUCTIVE', 'wb'); // truncates existing file data, use with care
defined('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE')  OR define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE', 'w+b'); // truncates existing file data, use with care
defined('FOPEN_WRITE_CREATE')                   OR define('FOPEN_WRITE_CREATE', 'ab');
defined('FOPEN_READ_WRITE_CREATE')              OR define('FOPEN_READ_WRITE_CREATE', 'a+b');
defined('FOPEN_WRITE_CREATE_STRICT')            OR define('FOPEN_WRITE_CREATE_STRICT', 'xb');
defined('FOPEN_READ_WRITE_CREATE_STRICT')       OR define('FOPEN_READ_WRITE_CREATE_STRICT', 'x+b');

/*
|--------------------------------------------------------------------------
| Exit Status Codes
|--------------------------------------------------------------------------
|
| Used to indicate the conditions under which the script is exit()ing.
| While there is no universal standard for error codes, there are some
| broad conventions.  Three such conventions are mentioned below, for
| those who wish to make use of them.  The CodeIgniter defaults were
| chosen for the least overlap with these conventions, while still
| leaving room for others to be defined in future versions and user
| applications.
|
| The three main conventions used for determining exit status codes
| are as follows:
|
|    Standard C/C++ Library (stdlibc):
|       http://www.gnu.org/software/libc/manual/html_node/Exit-Status.html
|       (This link also contains other GNU-specific conventions)
|    BSD sysexits.h:
|       http://www.gsp.com/cgi-bin/man.cgi?section=3&topic=sysexits
|    Bash scripting:
|       http://tldp.org/LDP/abs/html/exitcodes.html
|
*/
defined('EXIT_SUCCESS')        OR define('EXIT_SUCCESS', 0); // no errors
defined('EXIT_ERROR')          OR define('EXIT_ERROR', 1); // generic error
defined('EXIT_CONFIG')         OR define('EXIT_CONFIG', 3); // configuration error
defined('EXIT_UNKNOWN_FILE')   OR define('EXIT_UNKNOWN_FILE', 4); // file not found
defined('EXIT_UNKNOWN_CLASS')  OR define('EXIT_UNKNOWN_CLASS', 5); // unknown class
defined('EXIT_UNKNOWN_METHOD') OR define('EXIT_UNKNOWN_METHOD', 6); // unknown class member
defined('EXIT_USER_INPUT')     OR define('EXIT_USER_INPUT', 7); // invalid user input
defined('EXIT_DATABASE')       OR define('EXIT_DATABASE', 8); // database error
defined('EXIT__AUTO_MIN')      OR define('EXIT__AUTO_MIN', 9); // lowest automatically-assigned error code
defined('EXIT__AUTO_MAX')      OR define('EXIT__AUTO_MAX', 125); // highest automatically-assigned error code

defined('VALUEFIRST_SMS_URL') OR define('VALUEFIRST_SMS_URL', 'http://api.myvaluefirst.com/psms/servlet/psms.Eservice2');
defined('VALUEFIRST_USER') OR define('VALUEFIRST_USER','enterux');
defined('VALUEFIRST_PASSWORD') OR define('VALUEFIRST_PASSWORD','enterux@123');
defined('VALUEFIRST_SENDER_ID') OR define('VALUEFIRST_SENDER_ID','CVIDEO');

defined('MOBINEXT_SMS_URL') OR define('MOBINEXT_SMS_URL', 'http://vas.mobinext.in/vendorsms/pushsms.aspx');
defined('MOBINEXT_XML_URL') OR define('MOBINEXT_XML_URL', 'http://vas.mobinext.in/Rest/Messaging.svc/mtsms');
defined('MOBINEXT_CREDIT_URL') OR define('MOBINEXT_CREDIT_URL', 'http://vas.mobinext.in/vendorsms/CheckBalance.aspx');
defined('MOBINEXT_USER') OR define('MOBINEXT_USER','TATSTL');
defined('MOBINEXT_PASSWORD') OR define('MOBINEXT_PASSWORD','tata@123');
defined('MOBINEXT_SENDER_ID') OR define('MOBINEXT_SENDER_ID','COVDEO');

defined('ROUTEMOBILE_URL') OR define('ROUTEMOBILE_URL', 'http://sms6.rmlconnect.net:8080/bulksms/bulksms');
defined('ROUTEMOBILE_USERNAME') OR define('ROUTEMOBILE_USERNAME', 'enteruxotp1');
defined('ROUTEMOBILE_PASSWORD') OR define('ROUTEMOBILE_PASSWORD', 'PktYqQ4R');
defined('ROUTEMOBILE_SENDER_ID') OR define('ROUTEMOBILE_SENDER_ID', 'COVDEO');

defined('COVIDEO_API_URL') OR define('COVIDEO_API_URL', 'https://api.covideo.in/api/');
defined('COVIDEO_API_TOKEN') OR define('COVIDEO_API_TOKEN','P6vkHQGDv8KntDFE1INw2vI6Ejj9MaSEbRzSXotR6oV1ijnk7sQ0t9CZfHSwc2Z8');

if(ENVIRONMENT == 'development') {
    defined('COVIDEO_APP_URL') OR define('COVIDEO_APP_URL', 'https://our.covideo.in/');
    defined('MAX_USERS_PER_ROOM') OR define('MAX_USERS_PER_ROOM', 50);
} else {
    defined('COVIDEO_APP_URL') OR define('COVIDEO_APP_URL', 'https://in.covideo.in/');
    defined('MAX_USERS_PER_ROOM') OR define('MAX_USERS_PER_ROOM', 8);
}

defined('DOCROOT') OR define('DOCROOT', $_SERVER['DOCUMENT_ROOT']);

defined('RAZORPAY_ID') OR DEFINE('RAZORPAY_ID', 'rzp_live_qSzrgVwbwhiHjj');
defined('RAZORPAY_SECRET') OR DEFINE('RAZORPAY_SECRET', 'ZC7gF5SlCWs8MFdY7wDPUfIg');

defined('YBANQ_ID') OR DEFINE('YBANQ_ID', '7692af1e-56e3-4405-8911-85cdc910baf3');
defined('YBANQ_SECRET') OR DEFINE('YBANQ_SECRET', 'cGTfgf29vbD3dHJlzb70sZ2hhbnRh');

defined('INSTAMOJO_API_KEY') OR DEFINE('INSTAMOJO_API_KEY', '2632b5b3682c83b05f9ca4a4dd946e1a');
defined('INSTAMOJO_AUTH_TOKEN') OR DEFINE('INSTAMOJO_AUTH_TOKEN', '0593607621cf5c9aa786d88a2d590689');
defined('INSTAMOJO_SALT') OR DEFINE('INSTAMOJO_SALT', '9cdda41795d9419c87922a11940c36d2');
defined('INSTAMOJO_SANDBOX_URL') OR DEFINE('INSTAMOJO_SANDBOX_URL', 'https://test.instamojo.com/api/1.1/');
defined('INSTAMOJO_LIVE_URL') OR DEFINE('INSTAMOJO_LIVE_URL', 'https://www.instamojo.com/api/1.1/');

defined('DRIP_API_URL') OR define('DRIP_API_URL', 'https://api.getdrip.com/v2/');
defined('DRIP_API_TOKEN') OR define('DRIP_API_TOKEN','64nnbnvx6ezfm7dfpcum');
defined('DRIP_ACCOUNT_ID') OR define('DRIP_ACCOUNT_ID','4406547');
