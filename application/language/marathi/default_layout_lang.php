<?php

$lang['menu_mission_control'] = "मिशन नियंत्रण";
$lang['menu_my_profile'] = "माझे प्रोफाइल";
$lang['menu_support'] = "मदत";
$lang['menu_my_rooms'] = "माझे રૂમ";
$lang['menu_signout'] = "साइन आउट";

$lang['menu_language'] = "भाषा सेट करा";

$lang['footer_version'] = "आवृत्ती";
$lang['footer_copyright'] = '<strong>कॉपीराइट &copy; 2020 <a href="https://covideo.in" target="_blank">CoVideo.in</a>.</strong> सर्व हक्क राखीव';

$lang["emptyTable"] = "कोणताही डेटा उपलब्ध नाही";
$lang["info"] = "_TOTAL_ रेकॉर्डपैकी _START_ ते _END_";
$lang["infoEmpty"] = "0 रेकॉर्डपैकी 0 ते 0";
$lang["infoFiltered"] = "(_MAX_ एकूण रेकॉर्डद्वारे फिल्टर केलेले)";
$lang["lengthMenu"] = "_MENU_ रेकॉर्ड दर्शवा";
$lang["search"] = "शोधा:";
$lang["zeroRecords"] = "कोणतीही रेकॉर्ड आढळली नाहीत";
$lang["first"] = "पहिला";
$lang["last"] = "शेवटचे";
$lang["next"] = "पुढे";
$lang["previous"] = "मागील";
