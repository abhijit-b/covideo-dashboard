<?php

$lang['profile_tab'] = "प्रोफाइल";
$lang['profile_name'] = "नाव";
$lang['profile_email'] = "ई-मेल";
$lang['profile_phone_number'] = "फोन नंबर";
$lang['profile_submit'] = "जतन करा";

$lang['profile_login_activity'] = "लॉगिन क्रियाकलाप";
$lang['profile_la_table_header1'] = "ब्राउझर";
$lang['profile_la_table_header2'] = "आयपी पत्ता";
$lang['profile_la_table_header3'] = "लॉगिन तारीख आणि वेळ";

$lang['support_text'] = 'मदत हवी आहे? कृपया <a href="mailto:support@covideo.in"> समर्थन@covideo.in </a> वर ईमेल पाठवा. आम्ही 24 तासांच्या आत सर्व ईमेलला उत्तर देतो.';