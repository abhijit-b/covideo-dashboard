<?php

$lang['menu_mission_control'] = "Mission Control";
$lang['menu_my_profile'] = "My Profile";
$lang['menu_support'] = "Support";
$lang['menu_my_rooms'] = "My Rooms";
$lang['menu_signout'] = "Sign Out";

$lang['menu_language'] = "Change Language";

$lang['footer_version'] = "Version";
$lang['footer_copyright'] = '<strong>Copyright &copy; 2020 <a href="https://covideo.in" target="_blank">CoVideo.in</a>.</strong> All rights
reserved.';

$lang["emptyTable"] = "No data available in table";
$lang["info"] = "Showing _START_ to _END_ of _TOTAL_ entries";
$lang["infoEmpty"] = "Showing 0 to 0 of 0 entries";
$lang["infoFiltered"] = "(filtered from _MAX_ total entries)";
$lang["lengthMenu"] = "Show _MENU_ entries";
$lang["search"] = "Search:";
$lang["zeroRecords"] = "No matching records found";
$lang["first"] = "First";
$lang["last"] = "Last";
$lang["next"] = "Next";
$lang["previous"] = "Previous";
