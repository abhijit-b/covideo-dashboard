<?php

$lang['content_welcome'] = "Welcome to CoVideo.";
$lang['content_header'] = "This is the space to see all your information and stats. This page will have ongoing additions during the beta phase.";
$lang['content_message'] = "We encourage you to use CoVideo for your personal use. We are committed to provide you with clear audio and stable video. During the beta phase, we will be collecting feedback from all our early access users, so feel free to send us any challenges and issues you face while on CoVideo. Here are a few pointers to have a great experience:";
$lang['content_point1'] = "1. Connect from a desktop or laptop for a superior experience.";
$lang['content_point2'] = "2. Please clear your browser cache and use an updated browser. CoVideo works best on Chrome and Safari.";
$lang['content_point3'] = "3. Ideally, use a headset and good quality mic. This will ensure echo is reduced and there is less ambient noise heard by other participants.";
$lang['content_point4'] = "4. Occasionally, you may get kicked out of the room. This is usually due to very low network. Simply refresh your browser or enter the same room again with your room link.";
$lang['content_point5'] = "5. Use wi-fi for better connection and stability.";


$lang['room_detail1'] = "Max Persons Per Room :";
$lang['room_detail2'] = "Created on :";
$lang['room_link'] = 'Go to Room';
$lang['room_copy_link'] = 'Copy Link';
