<?php

$lang['profile_tab'] = "Profile";
$lang['profile_name'] = "Name";
$lang['profile_email'] = "Email";
$lang['profile_phone_number'] = "Phone Number";
$lang['profile_submit'] = "Submit";

$lang['profile_login_activity'] = "Login Activity";
$lang['profile_la_table_header1'] = "Browser";
$lang['profile_la_table_header2'] = "IP Address";
$lang['profile_la_table_header3'] = "Login Date and Time";

$lang['support_text'] = 'Need help? Please send an email to <a href="mailto:support@covideo.in">support@covideo.in</a>. We respond to all emails within 24 hours.';