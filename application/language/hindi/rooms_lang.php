<?php

$lang['heading'] = "कमरों की सूची";
$lang['table_heading1'] = "कमरे का नाम";
$lang['table_heading2'] = "कमरे की क्षमता";
$lang['table_heading3'] = "रचना समय";
$lang['table_heading4'] = "अपना कमरा साझा करें";
$lang['table_heading5'] = "लिंक";

$lang['room_link'] = 'कमरे में जाओ';
$lang['room_copy_link'] = 'यूआरएल कॉपी करें';


