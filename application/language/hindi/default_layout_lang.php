<?php

$lang['menu_mission_control'] = "योजना नियंत्रण";
$lang['menu_my_profile'] = "मेरी प्रोफाइल";
$lang['menu_support'] = "सहयोग";
$lang['menu_my_rooms'] = "मेरे कमरे";
$lang['menu_signout'] = "प्रस्थान करें";

$lang['menu_language'] = "भाषा सेट करें";

$lang['footer_version'] = "संस्करण";
$lang['footer_copyright'] = '<strong>
कॉपीराइट &copy; 2020 <a href="https://covideo.in" target="_blank">CoVideo.in</a>.</strong> सभी अधिकार सुरक्षित';

$lang["emptyTable"] = "कोई डेटा उपलब्ध नहीं है";
$lang["info"] = "_TOTAL_ में से _START_ से _END_ रिकॉर्ड";
$lang["infoEmpty"] = "0 में से 0 से 0 रिकॉर्ड";
$lang["infoFiltered"] = "(_MAX_ कुल रिकॉर्ड से फ़िल्टर किया गया)";
$lang["lengthMenu"] = "_MENU_ रिकॉर्ड दिखाएं";
$lang["search"] = "खोज:";
$lang["zeroRecords"] = "कोई रिकॉर्ड नहीं मिला";
$lang["first"] = "प्रथम";
$lang["last"] = "अंतिम";
$lang["next"] = "आगे";
$lang["previous"] = "पिछला";
