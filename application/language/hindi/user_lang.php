<?php

$lang['profile_tab'] = "प्रोफ़ाइल";
$lang['profile_name'] = "नाम";
$lang['profile_email'] = "ईमेल";
$lang['profile_phone_number'] = "फ़ोन नंबर";
$lang['profile_submit'] = "सहेजें";

$lang['profile_login_activity'] = "लॉगिन गतिविधि";
$lang['profile_la_table_header1'] = "ब्राउज़र";
$lang['profile_la_table_header2'] = "आईपी ​​पता";
$lang['profile_la_table_header3'] = "लॉगिन दिनांक और समय";

$lang['support_text'] = 'मदद की ज़रूरत है? कृपया <a href="mailto:support@covideo.in">support@covideo.in</a> पर ईमेल भेजें। हम 24 घंटे के भीतर सभी ईमेल का जवाब देते हैं।';