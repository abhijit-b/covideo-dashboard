<?php

$lang['profile_tab'] = "પ્રોફાઇલ";
$lang['profile_name'] = "નામ";
$lang['profile_email'] = "ઇમેઇલ";
$lang['profile_phone_number'] = "ફોન નંબર";
$lang['profile_submit'] = "સબમિટ કરો";

$lang['profile_login_activity'] = "લોગીન પ્રવૃત્તિ";
$lang['profile_la_table_header1'] = "બ્રાઉઝર";
$lang['profile_la_table_header2'] = "આઈપી એડ્રેસ";
$lang['profile_la_table_header3'] = "લોગીન ની તારીખ અને સમય";

$lang['support_text'] = 'મદદ જોઈતી? કૃપા કરીને <a href="mailto:support@covideo.in"> support@covideo.in </a> પર ઇમેઇલ મોકલો. અમે 24 કલાકની અંદર બધા ઇમેઇલ્સનો જવાબ આપીશું.';