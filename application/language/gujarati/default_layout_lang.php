<?php

$lang['menu_mission_control'] = "મિશન નિયંત્રણ";
$lang['menu_my_profile'] = "મારી પ્રોફાઈલ";
$lang['menu_support'] = "મદદ";
$lang['menu_my_rooms'] = "મારો રૂમ";
$lang['menu_signout'] = "સાઇન આઉટ";

$lang['menu_language'] = "ભાષા બદલો";

$lang['footer_version'] = "સંસ્કરણ";
$lang['footer_copyright'] = '<strong>કપિરાઇટ &copy; 2020 <a href="https://covideo.in" target="_blank">CoVideo.in</a>.</strong> બધા હક અનામત.';

$lang["emptyTable"] = "કોષ્ટકમાં કોઈ ડેટા ઉપલબ્ધ નથી";
$lang["info"] = "_TOTAL_ પ્રવેશોની _START_ થી _END_ બતાવી રહ્યું છે";
$lang["infoEmpty"] = "0 પ્રવેશોની 0 થી 0 બતાવી રહ્યું છે";
$lang["infoFiltered"] = "(_MAX_ કુલ પ્રવેશોથી ફિલ્ટર કરેલ)";
$lang["lengthMenu"] = "_MENU_ પ્રવેશો બતાવો";
$lang["search"] = "શોધો:";
$lang["zeroRecords"] = "કોઈ મેળ ખાતા રેકોર્ડ મળ્યાં નથી";
$lang["first"] = "પ્રથમ";
$lang["last"] = "છેલ્લા";
$lang["next"] = "આગળ";
$lang["previous"] = "અગાઉના";
