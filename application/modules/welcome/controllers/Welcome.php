<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

    public $data;
    
    public function __construct() {
        parent::__construct();
    }

	public function index()	{
        $this->template->set('title', '');
        $this->template->load('plain_layout', 'contents' , 'index');
    }
}
