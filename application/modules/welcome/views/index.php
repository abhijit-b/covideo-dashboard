<div class="spacer10"></div>
<div class="spacer10"></div>
<br><br>
<?php if(isset($_SESSION['error'])) { ?>
    <div class="alert alert-danger"><?php echo $this->session->flashdata('error'); ?></div>
<?php } ?>
<?php if(isset($_SESSION['success'])) { ?>
    <div class="alert alert-success"><?php echo $this->session->flashdata('success'); ?></div>
<?php } ?>
<div class="clearfix"></div>
<div class="card text-center">
  <h3>Welcome to CoVideo!</h3>
  Let us help you get started.
  <br><br>
  <label>Already have an account?
      <a href="<?php echo base_url(); ?>login" class="to_register" style="color:#FFA365;font-weight:bold;margin:0px"> Login </a> to start connecting.
  </label>
  <br>
  <h2>OR</h2>
  <br>
  <p>
    Don't have an account?
    <a href="<?php echo base_url(); ?>signup" class="to_register"  style="text-decoration: underline;color:#80C06F;font-weight:bold;margin:0px">Sign up for free</a>
  </p>

</div>

