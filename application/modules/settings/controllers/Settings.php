<?php

class Settings extends MX_Controller {

    public $data;
    
    public function __construct() {
        parent::__construct();
        $this->load->model('settings_model');
        if(!isset($_SESSION['user_logged'])) {
            $this->session->set_flashdata("error", "Please login first to view this page");
            redirect("login");
        } else if(!in_array($this->session->userdata('role'), array('admin','super'))) {
            $this->session->set_flashdata("error", "403 FORBIDDEN : Access Denied You don't have permission to access.");
            redirect('/dashboard');
        }
    }

    public function index() {

        $settings = array();
        $settings = $this->settings_model->get();
        if(!in_array($this->session->userdata('role'), array('admin','super'))) {
            foreach($settings as $index => $setting) {
                if($setting->admin_id != $this->userid)
                    unset($settings[$index]);
            }
        }
        $this->data['settings'] = $settings;

        $this->load->model('settings_model');
        $user_id = $this->session->userdata('userid');
		$this->data['user_id'] = $user_id;
        $this->data['api_key'] = $this->settings_model->getUserApiKey($user_id);
        
        $this->template->set('title', 'Settings');
        $this->template->load('default_layout', 'contents' , 'settings/list', $this->data);
    }

    public function generate_api_key() {
		if(!empty($this->input->post())) {
			$data['key'] = random_string('alnum', 40);
			$data['level'] = 10;
			$data['ignore_limits'] = 0;
			$data['is_private_key'] = 0;

			if($this->settings_model->updateApiKey($this->input->post('user_id'), $data)) {
				echo json_encode(array(
					'status' => 'success',
					'api_key' => $data['key']
				));
			} else {
				echo json_encode(array(
					'status' => 'error',
					'api_key' => ''
				));
	
			}
		} else {
			echo json_encode(array(
				'status' => 'error',
				'api_key' => ''
			));
		}
    }
    
    public function view() {
        if(isset($_POST['done'])) {
            redirect('settings', 'refresh');
        } else {
            if(empty($this->uri->segment(3)))  { 
                $this->session->set_flashdata("error", "No setting selected");
            } else {
                $settingid = $this->uri->segment(3);
                $settinsginfo = $this->settings_model->getSettingById($settingid);
                $this->data['settinginfo'] = $settinsginfo;
                
            }
        }
        $this->template->set('title', 'Settings View');
        $this->template->load('default_layout', 'contents' , 'settings/view', $this->data);
    }

    public function edit() {
        if(isset($_POST['save'])) {
            $settingData = array(
                'config_group' => $this->input->post('config_group'),
                'config_title' => $this->input->post('config_title'),
                'config_key' => $this->input->post('config_key'),
                'config_val' => $this->input->post('config_val'),
                'config_desc' => $this->input->post('config_desc'),
                'input_type' => $this->input->post('input_type'),
                'input_options' => $this->input->post('input_options'),
                'allow_to_disable' => ($this->input->post('allow_to_disable') == 'yes') ? TRUE : FALSE,
                'is_disabled' => ($this->input->post('is_disabled') == 'yes') ? TRUE : FALSE
            );
            $this->settings_model->updateSetting($settingData, $this->input->post('userid'));
            $this->session->set_flashdata("success", "Setting updated successfully.");
            redirect('settings', 'refresh');
        } else {
            if(empty($this->uri->segment(3)))  { 
                $this->session->set_flashdata("error", "No setting selected");
            } else {
                $settingid = $this->uri->segment(3);
                $this->data['settinginfo'] = $this->settings_model->getSettingById($settingid);
            }
        }

        $enum_types = $this->settings_model->getSettingInputType();
        $this->data['input_type'] = explode(",", str_replace("'", "", substr($enum_types->COLUMN_TYPE, 5, (strlen($enum_types->COLUMN_TYPE)-6))));

        $this->template->set('title', 'Settings Edit');
        $this->template->load('default_layout', 'contents' , 'settings/edit', $this->data);
    }

    public function add() {
        if(isset($_POST['save'])) {
            $this->form_validation->set_rules('config_title', 'Config Title', 'trim|required');
            $this->form_validation->set_rules('config_key', 'Config Key', 'trim|required');
            $this->form_validation->set_rules('config_val', 'Config Value', 'trim|required');
            $this->form_validation->set_rules('config_desc', 'Config Description', 'trim|required');
            $this->form_validation->set_rules('input_type', 'Input Type', 'trim|required');

            if($this->form_validation->run() == TRUE) {
                $config_group = str_replace(' ', '', $this->input->post('config_group'));
                $settingData = array(
                    'config_group' => strtoupper($config_group),
                    'config_title' => $this->input->post('config_title'),
                    'config_key' => $this->input->post('config_key'),
                    'config_val' => $this->input->post('config_val'),
                    'config_desc' => $this->input->post('config_desc'),
                    'input_type' => $this->input->post('input_type'),
                    'input_options' => $this->input->post('input_options'),
                    'allow_to_disable' => ($this->input->post('allow_to_disable') == 'yes') ? TRUE : FALSE,
                    'is_disabled' => ($this->input->post('is_disabled') == 'yes') ? TRUE : FALSE,
                    'u_date' => time(),
                    'c_date' => time(),
                    'admin_id' => $this->session->userdata('userid')
                );
                $this->settings_model->insertSetting($settingData);
                $this->session->set_flashdata("success", "Setting updated successfully.");
                redirect('settings', 'refresh');
            }
        }
        
        $enum_types = $this->settings_model->getSettingInputType();
        $this->data['input_type'] = explode(",", str_replace("'", "", substr($enum_types->COLUMN_TYPE, 5, (strlen($enum_types->COLUMN_TYPE)-6))));
        $this->template->set('title', 'Settings Add');
        $this->template->load('default_layout', 'contents' , 'settings/add', $this->data);
    }
  }
