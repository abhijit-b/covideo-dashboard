    <main role="main" class="col-md-10 ml-sm-auto col-lg-10 px-4">
      <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h4 class="h4">Edit setting details</h4>
      </div>  
      <div class="col-md-8">
        <?php if(isset($_SESSION['error'])) { ?>
            <div class="alert alert-danger"><?php echo $this->session->flashdata('error'); ?></div>
        <?php } ?>
        <?php if(isset($_SESSION['success'])) { ?>
            <div class="alert alert-success"><?php echo $this->session->flashdata('success'); ?></div>
        <?php } ?>
        <?php echo validation_errors('<div class="alert alert-danger">', '</div>'); ?>
        <form method="POST" action="/settings/add">
            <div class="form-group">
              <label>Group</label>
              <input class="form-control" name="config_group" id="config_group" type="text" value="<?php echo set_value('config_group'); ?>" placeholder="Config Group">
            </div>
            <div class="form-group">
              <label>Title</label>
              <input class="form-control" name="config_title" id="config_title" type="text" value="<?php echo set_value('config_title'); ?>" placeholder="Config Title">
            </div>
            <div class="form-group">
              <label>Key</label>
              <input class="form-control" name="config_key" id="config_key" type="text" value="<?php echo set_value('config_key'); ?>" placeholder="Config Key">
            </div>
            <div class="form-group">
              <label>Value</label>
              <input class="form-control" name="config_val" id="config_val" type="text" value="<?php echo set_value('config_val'); ?>" placeholder="Config Value">
            </div>
            <div class="form-group">
              <label>Description</label>
              <textarea class="form-control" name="config_desc" id="config_desc" rows="5"><?php echo set_value('config_desc'); ?></textarea>
            </div>
            <div class="form-group">
              <label>Input Type</label>
              <select class="form-control" name="input_type" id="input_type">
                  <?php foreach($input_type as $type) { ?>
                    <option><?php echo $type; ?></option>
                  <?php }?>
              </select>
            </div>
            <div class="form-group">
              <label>Input Options</label>
              <input class="form-control" name="input_options" id="input_options" type="text" value="<?php echo set_value('input_options'); ?>" placeholder="Input Options">
            </div>
            <div class="form-group">
                <label>Allow To Disable</label>
                <div class="spacer10"></div>
                <div class="form-check form-check-inline">
                    <input class="form-check-input" name="allow_to_disable" id="allow_to_disable_yes" type="radio" value="yes">
                    <label class="form-check-label" for="allow_to_disable_yes">Yes</label>
                </div>
                <div class="form-check form-check-inline">
                    <input class="form-check-input" name="allow_to_disable" id="allow_to_disable_no" type="radio" value="no" checked>
                    <label class="form-check-label" for="allow_to_disable_no">No</label>
                </div>
            </div>
            <div class="form-group">
                <label>Disabled</label>
                <div class="spacer10"></div>
                <div class="form-check form-check-inline">
                    <input class="form-check-input" name="is_disabled" id="is_disabled_yes" type="radio" value="yes">
                    <label class="form-check-label" for="allow_to_disable_yes">Yes</label>
                </div>
                <div class="form-check form-check-inline">
                    <input class="form-check-input" name="is_disabled" id="is_disabled_no" type="radio" value="no" checked>
                    <label class="form-check-label" for="is_disabled_no">No</label>
                </div>
            </div>
            <div class="spacer10"></div>
            <div class="spacer10"></div>
            <div>
                <button class="btn btn-primary mr-2" name="save">Save</button>
                <a href="/settings" class="btn btn-outline-primary" name="cancel">Cancel</a>
            </div>
            <div class="spacer10"></div>
            <div class="spacer10"></div>
            <div class="spacer10"></div>
        </form>
      </div>
    </main>
<div class="spacer10"></div>
