    <main role="main" class="col-md-10 ml-sm-auto col-lg-10 px-4">
      <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3">
        <h4>Setting details</h4>
      </div>  
      <div class="col-md-12">
        <table class="table table-hover table-sm nowrap">
        <tr>
            <td style="width:20%" class="font-weight-bold">Id</td>
            <td style="width:80%"><?php echo $settinginfo->config_id; ?></td>
          </tr>
          <tr>
            <td class="font-weight-bold">Group</td>
            <td><?php echo $settinginfo->config_group; ?></td>
          </tr>
          <tr>
            <td class="font-weight-bold">Title</td>
            <td><?php echo $settinginfo->config_title; ?></td>
          </tr>
          <tr>
            <td class="font-weight-bold">Key</td>
            <td><?php echo $settinginfo->config_key; ?></td>
          </tr>
          <tr>
            <td class="font-weight-bold">Value</td>
            <td><?php echo htmlentities($settinginfo->config_val); ?></td>
          </tr>
          <tr>
            <td class="font-weight-bold">Description</td>
            <td><?php echo $settinginfo->config_desc; ?></td>
          </tr>
          <tr>
            <td class="font-weight-bold">Disabled</td>
            <td><?php echo $settinginfo->is_disabled; ?></td>
          </tr>
          <tr>
            <td class="font-weight-bold">Type</td>
            <td><?php echo $settinginfo->input_type; ?></td>
          </tr>
          <tr>
            <td class="font-weight-bold">Options</td>
            <td><?php echo $settinginfo->input_options; ?></td>
          </tr>
          <tr>
            <td class="font-weight-bold">Allow to Disable</td>
            <td><?php echo $settinginfo->allow_to_disable; ?></td>
          </tr>
          <tr>
            <td class="font-weight-bold">Disabled</td>
            <td><?php echo $settinginfo->is_disabled; ?></td>
          </tr>
          <tr>
            <td class="font-weight-bold">Created</td>
            <td><?php echo date('d-m-Y H:i:s', $settinginfo->c_date); ?></td>
          </tr>
          <tr>
            <td class="font-weight-bold">Updated</td>
            <td><?php echo date('d-m-Y H:i:s',$settinginfo->u_date); ?></td>
          </tr>
        </table>
      </div>
      <div class="spacer10"></div>
      <div>
          <a href="/settings" class="btn btn-primary" name="done">Done</a>
      </div>
    </main>
<div class="spacer10"></div>
<div class="spacer10"></div>
