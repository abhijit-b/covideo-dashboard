
<div class="x_panel">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h3><i class="fa fa-cogs"></i> Settings</h3>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                    <?php if(in_array($this->session->userdata('role'), array('super'))) { ?>
                        <li class="active" id="config-tab"><a href="#config" data-toggle="tab">Config</a></li>
                    <?php } ?>
                    <li id="api-tab"><a href="#api" data-toggle="tab">API</a></li>
                </ul>
                <div class="tab-content">
                    <div class="active tab-pane" id="config" style="display: none;">
                        <p>      
                            <div class="btn-toolbar">
                            <div class="btn-group">
                                <label class="btn btn-primary" id="edit"><i class="fa fa-edit"></i> Edit</label>
                                <label class="btn btn-primary" id="view"><i class="fa fa-file"></i> View</label>
                                <label class="btn btn-primary" id="add"><i class="fa fa-plus"></i> Add</label>
                                <label class="btn btn-primary" id="delete"><i class="fa fa-trash"></i> Delete</label>
                            </div>
                            </div>
                        </p>
                    
                        <div class="spacer10"></div>
                        <?php if(isset($_SESSION['error'])) { ?>
                            <div class="alert alert-danger"><?php echo $this->session->flashdata('error'); ?></div>
                        <?php } ?>
                        <?php if(isset($_SESSION['success'])) { ?>
                            <div class="alert alert-success"><?php echo $this->session->flashdata('success'); ?></div>
                        <?php } ?>
                        <div class="spacer10"></div>

                        <div class="alert alert-danger hidden" id="delete-confirmation-message"> 
                            <p class="lead">Are you sure you want to delete the setting ?</p> 
                            <p>
                                <button type="button" class="btn btn-danger" id="delete-confirmation">Delete</button> 
                                <button type="button" class="btn btn-default" id="cancel">Cancel</button> 
                            </p> 
                        </div>

                        <div class="alert alert-danger hidden" id="error-message">No setting selected.</div>

                        <div class="table-responsive">
                            <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive" cellspacing="0" width="100%">
                                <thead class="thead-dark">
                                    <tr>
                                    <th>#</th>
                                    <th>Id</th>
                                    <th>Group</th>
                                    <th>Title</th>
                                    <th>Key</th>
                                    <th>Value</th>
                                    <th>Description</th>
                                    <th>Type</th>
                                    <th>Disabled</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                        foreach($settings as $setting){
                                    ?>
                                        <tr>
                                            <td>
                                                <div class="form-check">
                                                    <input class="form-check-input" type="checkbox" value="<?php echo $setting->config_id; ?>" id="">
                                                </div>
                                            </td>
                                            <td class="word-wrap"><?php echo $setting->config_id; ?></td>
                                            <td><?php echo $setting->config_group; ?></td>
                                            <td><?php echo $setting->config_title; ?></td>
                                            <td><?php echo $setting->config_key; ?></td>
                                            <td><?php echo htmlentities($setting->config_val); ?></td>
                                            <td><?php echo $setting->config_desc; ?></td>
                                            <td><?php echo $setting->input_type; ?></td>
                                            <td><?php echo $setting->is_disabled; ?></td>
                                        </tr>
                                    <?php
                                        }
                                    ?>
                                </tbody>
                            </table>

                        </div>
                    </div>
                    <div class="tab-pane" id="api">
                        <p>
                            <input type="hidden" name="user_id" value="<?php echo $user_id; ?>" id="user_id">
                            <div class="form-group">
                                <label for="inputName" class="col-sm-2 control-label">API KEY</label>

                                <div class="col-sm-6">
                                    <input type="text" class="form-control" id="api-key" value="<?php echo $api_key['key'];?>" readonly>
                                </div>
                                <div class="col-sm-4">
                                    <button type="button" id="generate-key" class="btn btn-info">Generate New Key</button>
                                </div>
                            </div>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="<?php echo base_url(); ?>assets/js/settings.js"></script>
<?php if(in_array($this->session->userdata('role'), array('super'))) { ?>
<script>
    $(document).ready(function(){
        $('#config').show();
    });
</script>
<?php } else { ?>
    <script>
    $(document).ready(function(){
        $('#api-tab').addClass('active');
        $('#api').show();
    });
</script>

<?php } ?>