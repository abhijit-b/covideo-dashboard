<?php

class Settings_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function get() {
      $query = $this->db->get('master_config');
      return $query->result();
    }

    public function count() {
      return $this->db->count_all('master_config');
    }

    public function getSettings($limit, $id) {
      $this->db->limit($limit, $limit * ($id -1));
      $query = $this->db->get("master_config");
      if ($query->num_rows() > 0) {
        foreach ($query->result() as $row) {
          $data[] = $row;
        }
        return $data;
      }
      return array();
    }

    public function getSettingById($id) {
      $this->db->from('master_config');
      $this->db->where(array('config_id' => $id));
      $result = $this->db->get();
      return $result->row();
    }

    public function getSettingInputType() {
      $sql = "SELECT COLUMN_TYPE FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'master_config' AND COLUMN_NAME = 'input_type'";
      return $this->db->query($sql)->result()[0];
    }

    public function updateSetting($data, $id) {
      $this->db->where('master_config.config_id', $id);
      return $this->db->update('master_config', $data);
    }

    public function insertSetting($data) {
      $this->db->insert('master_config', $data);
      return $this->db->insert_id();
    }

  public function getUserApiKey($user_id) {
    $this->db->from('rest_api_keys');
    $this->db->where(array('user_id' => $user_id));
    $result = $this->db->get();
    return $result->row_array();
  }

  public function updateApiKey($id, $data) {
      $this->db->where('user_id', $id);
      $q = $this->db->get('rest_api_keys');
      $this->db->reset_query();
      if ($q->num_rows() > 0) {
          return $this->db->where('user_id', $id)->update('rest_api_keys', $data);
      } else {
          return $this->db->set('user_id', $id)->insert('rest_api_keys', $data);
      }
  }
}