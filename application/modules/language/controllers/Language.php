<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Language extends MX_Controller {

    public $data;
    
    public function __construct() {
        parent::__construct();
        $this->load->helper('url');
    }

    function switch() {
        $language = $this->uri->segment(3);
        $language = ($language != "") ? $language : "english";
        $this->session->set_userdata('site_lang', $language);
        set_cookie('site_lang', $language, 86400, $this->input->server('HTTP_HOST'), '/', '', TRUE, TRUE);
        redirect('/dashboard');
    }
}