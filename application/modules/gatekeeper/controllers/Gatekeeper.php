<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Gatekeeper extends CI_Controller {

    public $data;
    
    public function __construct() {
        parent::__construct();
    }

    public function index()	{

        // check if path entered is a room
        $roomid = $this->uri->segment(1);

        $this->load->library('covideoapi');
        $covideo_api = new CovideoAPI();
        $room_details = $covideo_api->find($roomid);
        if(isset($room_details['status']) && $room_details['status'] == 'error') { 
            redirect('/', 'refresh');
        }
        
        if(!empty($room_details)) {
            // if user is logged in forward the username
            $username = $this->session->userdata('username');
            if(!empty($username)) {
                $params['username'] = $username;
            }
            // if user is the host pass that info
            if($username == $room_details['username']) {
                $params['host'] = 'true';
            }
            if(!empty($params)) {
                $param_str = http_build_query($params);
                $param_str = $this->base64url_encode($param_str);
            }

            if(!empty($param_str)) {
                $url = COVIDEO_APP_URL.$roomid.'?'.$param_str;
            } else {
                $url = COVIDEO_APP_URL.$roomid;
            }
            redirect($url);
        }
        redirect('/', 'refresh');
    }

	// public function index()	{

    //     // check if path entered is a room
    //     $roomid = $this->uri->segment(1);

    //     $this->load->library('covideoapi');
    //     $covideo_api = new CovideoAPI();
    //     $room_details = $covideo_api->find($roomid);
    //     if(isset($room_details['status']) && $room_details['status'] == 'error') { 
    //         redirect('/', 'refresh');
    //     }
        
    //     if(!empty($room_details)) {
    //         // room name
    //         $params['roomId'] = $room_details['code'];

    //         // if user is logged in forward the username
    //         $username = $this->session->userdata('username');
    //         $params['peerId'] = (!empty($username)) ? $username : '';

    //         // users display name
    //         $fullname = $this->session->userdata('fullname');
    //         $params['displayName'] = (!empty($fullname)) ? $fullname : '';

    //         // users avatar
    //         if(!empty($username)) {
    //             $this->load->model('user/users_model');
    //             $user_details = $this->users_model->getUserByCode($username);
    //             $picture =  $user_details->profile_image_url;
    //         } else {
    //             $picture = '';
    //         }
    //         $params['picture'] = (!empty($picture)) ? $picture : '';
            
    //         // if user is the host pass that info
    //         // $params['host'] = ($username == $room_details['username']) ? true : false;
            
    //         if(!empty($params)) {
    //             $param_str = json_encode($params, JSON_UNESCAPED_SLASHES);
    //             $param_str = $this->base64url_encode($param_str);
    //         }
            
    //         $this->data['COVIDEO_APP_URL'] = COVIDEO_APP_URL;
    //         $this->data['username'] = $username;
    //         $this->data['param_str'] = $param_str;
    //         $this->data['room'] = $params['roomId'];
    //         $this->data['host'] = ($username == $room_details['username']) ? true : false;

    //         $this->template->set('title', 'GateKeeper');
    //         $this->template->load('plain_layout', 'contents' , 'index', $this->data);
    //     } else {
    //         redirect('/', 'refresh');
    //     }
    // }

    public function base64url_encode($data) { 
        return rtrim(strtr(base64_encode($data), '+/', '-_'), '='); 
    }

    public function base64url_decode($data) { 
        return base64_decode(str_pad(strtr($data, '-_', '+/'), strlen($data) % 4, '=', STR_PAD_RIGHT)); 
    } 

}
