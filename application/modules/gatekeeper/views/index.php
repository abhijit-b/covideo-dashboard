<br><br>
<?php if (isset($_SESSION['error'])) { ?>
	<div class="alert alert-danger"><?php echo $this->session->flashdata('error'); ?></div>
<?php } ?>
<?php if (isset($_SESSION['success'])) { ?>
	<div class="alert alert-success"><?php echo $this->session->flashdata('success'); ?></div>
<?php } ?>
<form role="form" id="gatekeeper" name="gatekeeper" method="POST" action="<?php echo $COVIDEO_APP_URL; ?>auth/logincvm">
	<input type="hidden" name="room" value="<?php echo $room; ?>"/>
	<input type="hidden" name="host" value="<?php echo ($host) ? 'true' : 'false' ;?>"/>
	<input type="hidden" name="username" value="<?php echo !empty($username) ? $username : 'participant'; ?>">
	<input type="hidden" name="password" value="<?php echo $param_str; ?>">
	<div class="card-body">
		<div class="form-group" style="text-align: center;">
			<h4>Please wait while we check some settings to make sure that you have a smooth experience.</h4>
			<br><br>
			<div class="fa-3x text-center">
				<i class="fas fa-sync fa-spin"></i>
			</div>
		</div>
	</div>
</form>
<script>
	jQuery(document).ready(function() {
		setTimeout('document.gatekeeper.submit()',2000);
	}); 
</script>