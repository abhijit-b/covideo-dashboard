
    <main role="main" class="col-md-10 ml-sm-auto col-lg-10 px-4">
      <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3">
        <h4>REST API Log details</h4>
      </div>  
      <div class="col-md-12">
        <table class="table table-hover table-sm nowrap">
        <tr>
            <td style="width:20%" class="font-weight-bold">Id</td>
            <td style="width:80%"><?php echo $loginfo->id; ?></td>
          </tr>
          <tr>
            <td class="font-weight-bold">Domain</td>
            <td><?php echo $loginfo->domain; ?></td>
          </tr>
          <tr>
            <td class="font-weight-bold">Uri</td>
            <td><?php echo $loginfo->uri; ?></td>
          </tr>
          <tr>
            <td class="font-weight-bold">Method</td>
            <td><?php echo $loginfo->method; ?></td>
          </tr>
          <tr>
            <td class="font-weight-bold">Params</td>
            <td><?php echo $loginfo->params; ?></td>
          </tr>
          <tr>
            <td class="font-weight-bold">API Key</td>
            <td><?php echo $loginfo->api_key; ?></td>
          </tr>
          <tr>
            <td class="font-weight-bold">IP Address</td>
            <td><?php echo $loginfo->ip_address; ?></td>
          </tr>
          <tr>
            <td class="font-weight-bold">Time</td>
            <td><?php echo date('d-m-Y H:i:s', $loginfo->time); ?></td>
          </tr>
          <tr>
            <td class="font-weight-bold">Response Time</td>
            <td><?php echo $loginfo->rtime; ?></td>
          </tr>
          <tr>
            <td class="font-weight-bold">Authorized</td>
            <td><?php echo $loginfo->authorized; ?></td>
          </tr>
          <tr>
            <td class="font-weight-bold">Response Code</td>
            <td><?php echo $loginfo->response_code; ?></td>
          </tr>
          <tr>
            <td class="font-weight-bold">Response</td>
            <td><?php echo $loginfo->response; ?></td>
          </tr>
        </table>
      </div>
      <div class="spacer10"></div>
      <div>
          <a href="/restapilog" class="btn btn-primary" name="done">Done</a>
      </div>
    </main>
<div class="spacer10"></div>
<div class="spacer10"></div>
