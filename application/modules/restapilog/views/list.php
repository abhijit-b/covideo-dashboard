    <main role="main" class="col-md-10 ml-sm-auto col-lg-10 px-4">
      <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h3><i class="fa fa-cubes"></i> REST API Logs</h3>
      </div>

      <div class="btn-toolbar">
        <div class="btn-group">
          <label class="btn btn-outline-primary" id="view"><span data-feather="eye"></span> View</label>
        </div>
      </div>
      
      <div class="spacer10"></div>
      <?php if(isset($_SESSION['error'])) { ?>
        <div class="alert alert-danger"><?php echo $this->session->flashdata('error'); ?></div>
      <?php } ?>
      <?php if(isset($_SESSION['success'])) { ?>
          <div class="alert alert-success"><?php echo $this->session->flashdata('success'); ?></div>
      <?php } ?>
      <div class="spacer10"></div>

      <div class="alert alert-danger hidden" id="delete-confirmation-message"> 
        <p class="lead">Are you sure you want to delete the log(s) ?</p> 
        <p>
          <button type="button" class="btn btn-danger" id="delete-confirmation">Delete</button> 
          <button type="button" class="btn btn-default" id="cancel">Cancel</button> 
        </p> 
      </div>

      <div class="alert alert-danger hidden" id="error-message">No logs selected.</div>

      <div class="table-responsive">
        <table class="table table-hover table-sm" id="file-list">
          <thead class="thead-dark">
            <tr>
              <th>#</th>
              <th>Id</th>
              <th>Domain</th>
              <th>Uri</th>
              <th>Method</th>
              <th>Params</th>
              <th>Api Key</th>
              <th>Ip Address</th>
              <th>Time</th>
              <th>Response time</th>
              <th>Authorized</th>
              <th>Response Code</th>
              <th>Response</th>
            </tr>
          </thead>
          <tbody>
          <?php
            foreach($logs as $log){
              ?>
              <tr>
                <td>
                  <div class="form-check">
                    <input class="form-check-input" type="checkbox" value="<?php echo $log->id; ?>" id="">
                  </div>
                </td>
                <td class="word-wrap"><?php echo $log->id; ?></td>
                <td><?php echo $log->domain; ?></td>
                <td><?php echo $log->uri; ?></td>
                <td><?php echo $log->method; ?></td>
                <td><?php echo $log->params; ?></td>
                <td><?php echo $log->api_key; ?></td>
                <td><?php echo $log->ip_address; ?></td>
                <td><?php echo date('d-m-Y H:i:s',$log->time); ?></td>
                <td><?php echo $log->rtime; ?></td>
                <td><?php echo $log->authorized; ?></td>
                <td><?php echo $log->response_code; ?></td>
                <td><?php echo $log->response; ?></td>
              </tr>
              <?php
            }
          ?>
          </tbody>
        </table>
      </div>
      <!-- Show pagination links -->
      <nav aria-label="Page navigation example">
      <ul class="pagination">
        <?php foreach ($links as $link) {
          if(strpos($link, 'span') > 0) {
            echo "<li class='page-item active'>". $link."</a></li>";
          } else {
            echo "<li class='page-item'>". $link."</a></li>";
          } 
        }?>
        </ul>
      </nav>
    </main>
<form id="log-download-form"  method="post" action="/documents/download"></form>
<div class="spacer10"></div>
<div class="spacer10"></div>
<script src="<?php echo base_url(); ?>assets/js/log.js"></script>
