<?php

class Restapilog extends MX_Controller {

    public $data;
    
    public function __construct() {
        parent::__construct();
        $this->load->model('restapilog_model');
        if(!isset($_SESSION['user_logged'])) {
            $this->session->set_flashdata("error", "Please login first to view this page");
            redirect("login");
        } else if(!in_array($this->session->userdata('role'), array('admin','super'))) {
            $this->session->set_flashdata("error", "403 FORBIDDEN : Access Denied You don't have permission to access.");
            redirect('/');
        }
    }

    public function index() {
      $this->load->library('pagination');

      $config = array();
      $config["base_url"] = base_url() . "restapilog";
      $total_row = $this->restapilog_model->count();
      $config["total_rows"] = $total_row;
      $config["per_page"] = 5;
      $config['use_page_numbers'] = TRUE;
      $config['num_links'] = $total_row;
      $config['cur_tag_open'] = '<span class="page-link">';
      $config['attributes'] = array('class' => 'page-link');
      $config['cur_tag_close'] = '</a>';
      $config['next_link'] = 'Next';
      $config['prev_link'] = 'Previous';

      $this->pagination->initialize($config);
      $page = (!empty($this->uri->segment(2))) ? ($this->uri->segment(2)) : 1;
      $this->data["logs"] = $this->restapilog_model->getLogs($config["per_page"], $page);
      $str_links = $this->pagination->create_links();
      $this->data["links"] = array_filter(explode('</a>', $str_links));
      if(strpos($this->data['links'][0], 'Previous') == 0) {
          array_unshift($this->data['links'], '<a href="'.base_url().'restapilog" class="page-link" data-ci-pagination-page="1" rel="prev">Previous</a>');
      }
      $last_element = $this->data['links'][count($this->data['links']) - 1];
      if(strpos($last_element, 'Next') == 0) {
          $last_page = count($this->data['links']) - 1;
          $next_link = '<a href="'.base_url().'restapilog/'.$last_page.'" class="page-link" data-ci-pagination-page="1" rel="prev">Next</a>';
          array_push($this->data['links'], $next_link);
      }
      
      $this->template->set('title', 'REST API Log');
      $this->template->load('default_layout', 'contents' , 'restapilog/list', $this->data);
    }

    public function view() {
        if(isset($_POST['done'])) {
            redirect('restapilogs', 'refresh');
        } else {
            if(empty($this->uri->segment(3)))  { 
                $this->session->set_flashdata("error", "No user selected");
            } else {
                $id = $this->uri->segment(3);
                $this->data['loginfo'] = $this->restapilog_model->getLogById($id);
            }
        }
        $this->template->set('title', 'REST API Log - View');
        $this->template->load('default_layout', 'contents' , 'restapilog/view', $this->data);
    }
}
