<?php

class Restapilog_model extends CI_Model {
    public function __construct() {
        parent::__construct();
    }

    public function get() {
      $query = $this->db->get('rest_logs_table');
      return $query->result();
    }

    public function count() {
      return $this->db->count_all('rest_logs_table');
    }

    public function getLogs($limit, $id) {
      $this->db->limit($limit, $limit * ($id -1));
      $query = $this->db->get("rest_logs_table");
      if ($query->num_rows() > 0) {
        foreach ($query->result() as $row) {
          $data[] = $row;
        }
        return $data;
      }
      return false;
    }

    public function getLogById($id) {
      $this->db->from('rest_logs_table');
      $this->db->where(array('id' => $id));
      $result = $this->db->get();
      return $result->row();
    }
  }