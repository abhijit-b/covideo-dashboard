<?php

class Rooms_model extends CI_Model {
    public function __construct() {
        parent::__construct();
        $this->load->database();
    }

    public function getAllRooms() {
        $query = $this->db->get('room_master');
        return $query->result();
    }

    public function getAllRoomsById($id) {
        $this->db->where(array('user_id' => $id));
        $query = $this->db->get('room_master');
        return $query->result();
    }

    public function insert($room) {
        $this->db->insert('room_master', $room);
        return $this->db->insert_id();
    }
    
    public function getRoom($id) {
        $this->db->from('room_master');
        $this->db->where(array('id' => $id));
        $result = $this->db->get();
        return $result->row();
    }

    public function updateRoomInfo($data, $id) {
        $this->db->where('room_master.id', $id);
        return $this->db->update('room_master', $data);
    }

    public function deleteById($userid) {
        $this->db->where('id', $userid);
        $this->db->delete('room_master');
    }

    public function getRoomConfig($domain_name) {
        $this->db->where(array('domain_name' => $domain_name));
        $query = $this->db->get('domain_master');
        return $query->row_array();
    }
}
