<?php

class Rooms extends MX_Controller {

    public $data;
    
    public function __construct() {
        parent::__construct();
        $this->load->model('rooms_model');
        $this->data['userid'] = $this->session->userdata('userid');
        $this->data['rooms'] = $this->rooms_model->getAllRoomsById($this->data['userid']);
        $this->lang->load("default_layout", $this->session->userdata('site_lang'));
        $this->lang->load("rooms", $this->session->userdata('site_lang'));
    }

    public function config() {
        if(!empty($this->input->server('HTTP_REFERER'))) {
            $referer = parse_url($this->input->server('HTTP_REFERER'));
            $domain_details = $this->rooms_model->getRoomConfig($referer['host']);
            if(!empty($domain_details)) {
                $this->output->set_header('Content-Type: application/javascript');
                $this->data['domain_name'] = $domain_details['domain_name'];
                $this->data['site_end_point'] = $domain_details['site_end_point'];
                $this->data['description'] = $domain_details['description'];
                $this->template->load('no_layout', 'contents' , 'rooms/config', $this->data);
            }
        }
    }
    
    public function edit() {
        if(!isset($_SESSION['user_logged'])) {
            $this->session->set_flashdata("error", "Please login first to view this page");
            redirect("login");
        }

        if(!empty($this->input->post('save'))) {
            $user = array(
                'name' => $this->input->post('name'),
                'company' => $this->input->post('company'),
            );
            $this->rooms_model->updateRoomInfo($user, $this->input->post('userid'));
            $this->session->set_flashdata("success", "Room data updated successfully.");
            redirect('rooms', 'refresh');
        } else {
            if(empty($this->uri->segment(3)))  { 
                $this->session->set_flashdata("error", "No user selected");
            } else {
                $entityid = $this->uri->segment(3);
                $this->data['entityinfo'] = $this->rooms_model->getEntity($entityid);
            }
        }
        $this->template->set('title', 'Rooms - Edit');
        $this->template->load('default_layout', 'contents' , 'rooms/edit', $this->data);
    }
    
    public function index() {
        if(!isset($_SESSION['user_logged'])) {
            $this->session->set_flashdata("error", "Please login first to view this page");
            redirect("login");
        }

        $this->load->library('covideoapi');
        $covideo_api = new CovideoAPI();
        $this->data['rooms'] = $covideo_api->fetchRooms($this->session->userdata('username'));
        
        $this->template->set('title', 'Rooms - List');
        $this->template->load('default_layout', 'contents' , 'rooms/list', $this->data);
    }

    public function add() {
        if(count($this->input->post()) > 0) {
            $this->form_validation->set_rules('room_name', 'room_name', 'trim|required');
            if($this->form_validation->run() == TRUE) {
                $room = array(
                    'room_name' => $this->input->post('room_name'),
                    'room_type' => !empty($this->input->post('type')) ? $this->input->post('type') : '',
                    'language' => !empty($this->input->post('language')) ? $this->input->post('language') : 'en',
                    'user_id' => $this->data['userid'],
                    'created' => time()
                );
                $id = $this->rooms_model->insert($room);
                $this->session->set_flashdata("success", "Room added successfully.");
                redirect("rooms", "refresh");
            }
        } else {
            $this->template->set('title', 'Rooms - Add');
            $this->template->load('default_layout', 'contents' , 'rooms/add', $this->data);
        }
    }
    /*
    public function delete() {
        if(!in_array($this->session->userdata('role'), array('admin','super'))) {
            $this->session->set_flashdata("error", "403 FORBIDDEN : Access Denied You don't have permission to access.");
            redirect('/');
        }
        $userid = json_decode($this->input->post('userid']);
        $this->users_model->deleteById($userid);
        $this->session->set_flashdata("success", "User deleted successfully.");
        echo json_encode(array("success" => "User deleted successfully."));
    }
*/
}
