// eslint-disable-next-line
var domainName = window.location.hostname;
var configPath = `/config/${domainName}/`;
var imagePath = `${configPath}images/`;
var config =
{
	loginEnabled    : false,
	developmentPort : 443,
	productionPort  : 443,

	/**
	 * Supported browsers version 
	 * in bowser satisfy format.
	 * See more:
	 * https://www.npmjs.com/package/bowser#filtering-browsers
	 * Otherwise you got a unsupported browser page
	 */
	supportedBrowsers :
	{
		'windows' : {
			'internet explorer' : '>12',
			'microsoft edge'    : '>18'
		},
		'safari'                       : '>12',
		'firefox'                      : '>=60',
		'chrome'                       : '>=74',
		'chromium'                     : '>=74',
		'opera'                        : '>=62',
		'samsung internet for android' : '>=11.1.1.52'
	},

	/**
	 * Resolutions:
	 * 
	 * low ~ 320x240
	 * medium ~ 640x480
	 * high ~ 1280x720
	 * veryhigh ~ 1920x1080
	 * ultra ~ 3840x2560
	 * 
	 **/

	/**
	 * Frame rates:
	 * 
	 * 1, 5, 10, 15, 20, 25, 30
	 * 
	 **/
	// The aspect ratio of the videos as shown on
	// the screen. This is changeable in client settings.
	// This value must match one of the defined values in
	// viewAspectRatios EXACTLY (e.g. 1.333)
	viewAspectRatio               : 1.333,
	// These are the selectable aspect ratios in the settings
	viewAspectRatios              : [ {
		value : 1.333, // 4 / 3
		label : '4 : 3'
	}, {
		value : 1.777, // 16 / 9
		label : '16 : 9'
	} ],
	// The aspect ratio of the video from the camera
	// this is not changeable in settings, only config
	videoAspectRatio              : 1.333,
	defaultResolution             : 'medium',
	defaultFrameRate              : 15,
	defaultScreenResolution       : 'veryhigh',
	defaultScreenSharingFrameRate : 5,
	// Enable or disable simulcast for webcam video
	simulcast          : true,
	// Enable or disable simulcast for screen sharing video
	simulcastSharing   : false,
	// Simulcast encoding layers and levels
	simulcastEncodings :
	[
		{ scaleResolutionDownBy: 4 },
		{ scaleResolutionDownBy: 2 },
		{ scaleResolutionDownBy: 1 }
	],

	/**
	 * Alternative simulcast setting:
	 * [
	 *   { maxBitRate: 50000 }, 
	 *	 { maxBitRate: 1000000 },
	 *	 { maxBitRate: 4800000 }
	 *],
	 **/

	/**
	 * White listing browsers that support audio output device selection.
	 * It is not yet fully implemented in Firefox.
	 * See: https://bugzilla.mozilla.org/show_bug.cgi?id=1498512
	 */
	audioOutputSupportedBrowsers :
	[
		'chrome', 
		'opera'
	],
	// Socket.io request timeout
	requestTimeout   : 5000,
	requestRetries   : 3,
	transportOptions :
	{
		tcp : true
	},
  	defaultAudio : 
	{
		sampleRate       : 48000,
		channelCount     : 1,
		volume           : 1.0,
		autoGainControl  : true,
		echoCancellation : true,
		noiseSuppression : true,
		voiceActivityMute : false,
		sampleSize       : 16
	},
  	/**
	 * Set the auto mute / Push To Talk threshold
	 * default value is 4
	 * 
	 * Set it to 0 to disable auto mute functionality, 
	 * but use it with caution
	 * full mesh audio strongly decrease room capacity! 
	 */
	autoMuteThreshold : 4, 
	defaultLayout    : 'democratic', // democratic, filmstrip
	// If true, will show media control buttons in separate
	// control bar, not in the ME container.
	buttonControlBar : true,
	// If false, will push videos away to make room for side
	// drawer. If true, will overlay side drawer over videos
	drawerOverlayed  : false,
	// Position of notifications
	notificationPosition : 'center',
	// Timeout for autohiding topbar and button control bar
	hideTimeout          : 3000,
	// max number of participant that will be visible in 
	// as speaker
	lastN                : 30,
	mobileLastN          : 30,
	// Highest number of lastN the user can select manually in 
	// userinteface
	maxLastN             : 30,
	// If truthy, users can NOT change number of speakers visible
	lockLastN        : true,
	background  : 'linear-gradient(135deg, #ffa365 0%,#ffffff 20%,#ffffff 80%,#80c06f 100%)',
	// Add file and uncomment for adding logo to appbar
	logo        : `/config/${domainName}/covideo-logo.png`,
	logosmall   : `/config/${domainName}/covideo-logo-small.png`,
	title: 'CoVideo India - Video Conferencing Solution | Made in India.',
	// Service & Support URL
	website: "<?php echo $domain_name ;?>",
  	// if not set then not displayed on the about modals
	audioJoin	: false,
	movieShow	: false,
	siteEndPoint: "https://<?php echo $site_end_point; ?>",
	//supportUrl           : 'https://support.example.com',
	// Privacy and dataprotection URL or path
	// by default privacy/privacy.html
	// that is a placeholder for your policies
	//
	// but an external url could be also used here	 
	//privacyUrl           : 'privacy/privacy.html',
	theme       :
	{
		palette :
		{
			primary :
			{
				main : '#FFA365'
			},
			secondary :
			{
				main : '#80C06F',
			}
		},
		overrides :
		{
			MuiAppBar :
			{
				colorPrimary :
				{
					backgroundColor : '#fff' //#313131
				}
			},
			MuiFab :
			{
				primary :
				{
					backgroundColor : '#5F9B2D',
					'&:hover'       :
					{
						backgroundColor : '#518029'
					}
				}
			},
			MuiBadge :
			{
				colorPrimary :
				{
					backgroundColor : '#5F9B2D',
					'&:hover'       :
					{
						backgroundColor : '#518029'
					}
				}
			}
		},
		typography :
		{
			useNextVariants: true,
			fontFamily: 'Open Sans',
			color: '#000',
		},
		button: {
			fontFamily: 'Open Sans',
		}
	}
};
			
document.title = window.config.title;
document.querySelector('#description').setAttribute('content', "<?php echo $description; ?>");
document.querySelector('#my-manifest-placeholder').setAttribute('href', `${configPath}manifest.json`);
document.querySelector('#favicon').setAttribute('href', `${imagePath}apple-touch-icon.png`);
document.querySelector('#ati57').setAttribute('href', `${imagePath}apple-touch-icon-57x57.png`);
document.querySelector('#ati72').setAttribute('href', `${imagePath}apple-touch-icon-72x72.png`);
document.querySelector('#ati76').setAttribute('href', `${imagePath}apple-touch-icon-76x76.png`);
document.querySelector('#ati114').setAttribute('href', `${imagePath}apple-touch-icon-114x114.png`);
document.querySelector('#ati120').setAttribute('href', `${imagePath}apple-touch-icon-120x120.png`);
document.querySelector('#ati144').setAttribute('href', `${imagePath}apple-touch-icon-144x144.png`);
document.querySelector('#ati152').setAttribute('href', `${imagePath}apple-touch-icon-152x152.png`);
document.querySelector('#ati180').setAttribute('href', `${imagePath}apple-touch-icon-180x180.png`);