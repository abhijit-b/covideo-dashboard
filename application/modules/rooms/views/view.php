
    <main role="main" class="col-md-10 ml-sm-auto col-lg-10 px-4">
      <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3">
        <h4>Entity details</h4>
      </div>  
      <div class="col-md-12">
        <table class="table table-hover table-sm nowrap">
        <tr>
            <td style="width:20%" class="font-weight-bold">Id</td>
            <td style="width:80%"><?php echo $entityinfo->id; ?></td>
          </tr>
          <tr>
            <td class="font-weight-bold">Name</td>
            <td><?php echo $entityinfo->name; ?></td>
          </tr>
          <tr>
            <td class="font-weight-bold">Type</td>
            <td><?php echo $entityinfo->type; ?></td>
          </tr>
          <tr>
            <td class="font-weight-bold">Company</td>
            <td><?php echo $entityinfo->company; ?></td>
          </tr>
          <tr>
            <td class="font-weight-bold">Parent Id</td>
            <td><?php echo $entityinfo->parent_id; ?></td>
          </tr>
          <tr>
            <td class="font-weight-bold">Username</td>
            <td><?php echo $entityinfo->username; ?></td>
          </tr>
          <tr>
            <td class="font-weight-bold">Email</td>
            <td><?php echo $entityinfo->email; ?></td>
          </tr>
          <tr>
            <td class="font-weight-bold">Created</td>
            <td><?php echo date('d-m-Y H:i:s', $entityinfo->timestamp); ?></td>
          </tr>
          <tr>
            <td class="font-weight-bold">Status</td>
            <td><?php echo $entityinfo->status; ?></td>
          </tr>
        </table>
      </div>
      <div class="spacer10"></div>
      <div>
          <a href="/entities" class="btn btn-primary" name="done">Done</a>
      </div>
    </main>
<div class="spacer10"></div>
<div class="spacer10"></div>
