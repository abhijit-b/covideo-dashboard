
    <main role="main" class="col-md-10 ml-sm-auto col-lg-10 px-4">
      <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h4 class="h4">Add new room</h4>
      </div>  
      <div class="col-md-6">
        <?php if(isset($_SESSION['error'])) { ?>
            <div class="alert alert-danger"><?php echo $this->session->flashdata('error'); ?></div>
        <?php } ?>
        <?php if(isset($_SESSION['success'])) { ?>
            <div class="alert alert-success"><?php echo $this->session->flashdata('success'); ?></div>
        <?php } ?>
        <?php echo validation_errors('<div class="alert alert-danger">', '</div>'); ?>

        <form method="POST">
          <div class="form-group">
            <label>Room name </label>
            <input class="form-control" name="room_name" id="room_name" type="text" value="<?php echo set_value('room_name'); ?>" placeholder="Room Name" autofocus required>
          </div>
          <div class="spacer10"></div>
          <div class="form-group">
            <label>Language</label>
            <br>
            <select name="language" id="language" class="form-control" style="width:280px;display:inline;">
              <option value="-1">Select</option>
              <option value="en">English</option>
            </select>
          </div>
          <div class="spacer10"></div>
          <div>
            <button class="btn btn-primary mr-2" name="add">Add</button>
            <a href="/rooms" class="btn btn-outline-primary" name="cancel">Cancel</a>
          </div>
        </form>
      </div>
    </main>
<div class="spacer10"></div>
