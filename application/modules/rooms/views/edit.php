
    <main role="main" class="col-md-10 ml-sm-auto col-lg-10 px-4">
      <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h4 class="h4">Edit enitity details</h4>
      </div>  
      <div class="col-md-6">
        <?php if(isset($_SESSION['error'])) { ?>
            <div class="alert alert-danger"><?php echo $this->session->flashdata('error'); ?></div>
        <?php } ?>
        <?php if(isset($_SESSION['success'])) { ?>
            <div class="alert alert-success"><?php echo $this->session->flashdata('success'); ?></div>
        <?php } ?>
        <?php echo validation_errors('<div class="alert alert-danger">', '</div>'); ?>
        <form method="POST" action="/entities/edit">
            <input type="hidden" name="userid" value="<?php echo $entityinfo->id; ?>">
            <div class="form-group">
              <label>Name</label>
              <input class="form-control" name="name" id="Name" type="text" value="<?php echo set_value('name', $entityinfo->name); ?>" placeholder="First Name" autofocus required>
            </div>
            <div class="form-group">
              <label>Type</label>
              <input class="form-control" name="type" id="type" type="text" value="<?php echo set_value('type', $entityinfo->type); ?>" placeholder="Type" readonly>
            </div>
            <div class="form-group">
              <label>Company</label>
              <input class="form-control" name="company" id="company" type="text" value="<?php echo set_value('company', $entityinfo->company); ?>" placeholder="Company (optional)">
            </div>
            <div class="spacer10"></div>
            <div>
                <button class="btn btn-primary mr-2" name="save">Save</button>
                <a href="/entities" class="btn btn-outline-primary" name="cancel">Cancel</a>
            </div>
        </form>
      </div>
    </main>
<div class="spacer10"></div>
