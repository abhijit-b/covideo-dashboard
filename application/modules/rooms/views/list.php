
<link href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" rel="stylesheet"/>
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
<main role="main">
  <div class="pt-3 pb-2 mb-3 border-bottom">
    <h3><i class="fa fa-comments"></i> <?php echo $this->lang->line('heading'); ?></h3>
  </div>
  <div class="spacer10"></div>
  <?php if(isset($_SESSION['error'])) { ?>
      <div class="alert alert-danger"><?php echo $_SESSION['error']; ?></div>
  <?php } ?>
  <?php if(isset($_SESSION['success'])) { ?>
      <div class="alert alert-success"><?php echo $_SESSION['success']; ?></div>
  <?php } ?>
  <?php echo validation_errors('<div class="alert alert-danger">', '</div>'); ?>
  <div class="spacer10"></div>

  <p>
    <div class="btn-toolbar">
      <div class="btn-group">
        <!-- <label class="btn btn-primary" id="add"><i class="fa fa-plus"></i> Add</label> -->
      </div>
    </div>
  </p>
  <div class="table-responsive">
    <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive" cellspacing="0" width="100%">
      <thead>
        <tr class="thead-dark">
          <th>#</th>
          <th><?php echo $this->lang->line('table_heading1'); ?></th>
          <th><?php echo $this->lang->line('table_heading2'); ?></th>
          <th><?php echo $this->lang->line('table_heading3'); ?></th>
          <th><?php echo $this->lang->line('table_heading4'); ?></th>
          <th><?php echo $this->lang->line('table_heading5'); ?></th>
        </tr>
      </thead>
      <tbody>
      <?php
        foreach($rooms as $index => $row){
          ?>
          <tr>
            <td><?php echo $index + 1 ?></td>
            <td><?php echo $row['name']; ?></td>
            <td><?php echo $row['maxUsersPerRoom']; ?></td>
            <td><?php echo empty($row['dt_created']) ? '-' : date("d-m-Y H:i:s", strtotime($row['dt_created'])); ?></td>
            <td><a href="javascript:;" onclick="copyText('<?php echo $row['code'];?>')" class="btn btn-primary" ><?php echo $this->lang->line('room_copy_link'); ?></a></td>
            <td>
              <a href="/<?php echo $row['code'];?>" class="btn btn-primary" target="_blank" ><?php echo $this->lang->line('room_link'); ?></a>
            </td>
          </tr>
          <?php
        }
      ?>
      </tbody>
    </table>
  </div>
</main>
<script>
  $(document).ready(function(){
    $('#datatable-responsive').DataTable({
      "pageLength" : 50,
      "language" : {
          "emptyTable":     "<?php echo $this->lang->line('emptyTable'); ?>",
          "info":           "<?php echo $this->lang->line('info'); ?>",
          "infoEmpty":      "<?php echo $this->lang->line('infoEmpty'); ?>",
          "infoFiltered":   "<?php echo $this->lang->line('infoFiltered'); ?>",
          "lengthMenu":     "<?php echo $this->lang->line('lengthMenu'); ?>",
          "search":         "<?php echo $this->lang->line('search'); ?>",
          "zeroRecords":    "<?php echo $this->lang->line('zeroRecords'); ?>",
          "paginate": {
              "first":      "<?php echo $this->lang->line('first'); ?>",
              "last":       "<?php echo $this->lang->line('last'); ?>",
              "next":       "<?php echo $this->lang->line('next'); ?>",
              "previous":   "<?php echo $this->lang->line('previous'); ?>"
          }
      }
    });
    toastr.options = {
      "closeButton": false,
      "debug": false,
      "newestOnTop": false,
      "progressBar": false,
      "positionClass": "toast-top-full-width",
      "preventDuplicates": false,
      "onclick": null,
      "showDuration": "300",
      "hideDuration": "1000",
      "timeOut": "3000",
      "extendedTimeOut": "1000",
      "showEasing": "swing",
      "hideEasing": "linear",
      "showMethod": "fadeIn",
      "hideMethod": "fadeOut"
    }
  });
  function copyText(id) {
    var roomurl = 'https://<?php echo $this->input->server('HTTP_HOST'); ?>/' + id;
    if (navigator.share) {
        navigator.share({
            title: 'CoVideo Invitation',
            text: 'CoVideo Invitation',
            url: roomurl,
        }).then(() => toastr["success"]("Link shared successfully!"))
        .catch((error) => {
            if (event.ctrlKey || event.shiftKey || event.metaKey ||
                (event.button && event.button === 1)) {
                return;
            }
            event.preventDefault();
            navigator.clipboard.writeText(roomurl).then(() => toastr["success"]("Link copied to clipboard!"));
        });
    } else {
        if (event.ctrlKey || event.shiftKey || event.metaKey ||
            (event.button && event.button === 1)) {
            return;
        }
        event.preventDefault();
        navigator.clipboard.writeText(roomurl).then(() => toastr["success"]("Link copied to clipboard!"));
    }
  }
</script>
<script src="<?php echo base_url(); ?>assets/js/rooms.js"></script>
