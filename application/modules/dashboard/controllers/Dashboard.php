<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

    public $data;
    
    public function __construct() {
        parent::__construct();
        if(!isset($_SESSION['user_logged'])) {
            $this->session->set_flashdata("error", "Please login first to view this page");
            redirect("login");
        }
        $this->load->model('dashboard_model');
        $this->userid = $this->session->userdata('userid');
        $this->lang->load("default_layout", $this->session->userdata('site_lang'));
        $this->lang->load("dashboard", $this->session->userdata('site_lang'));
    }

	public function index()	{

        $this->load->library('covideoapi');
        $covideo_api = new CovideoAPI();
        $this->data['rooms'] = $covideo_api->fetchRooms($this->session->userdata('username'));

        $this->template->set('title', 'Home');
        $this->template->load('default_layout', 'contents' , 'dashboard', $this->data);
    }
}
