<div class="spacer10"></div>
<div class="spacer10"></div>
<br><br>
<?php if(isset($_SESSION['error'])) { ?>
    <div class="alert alert-danger"><?php echo $this->session->flashdata('error'); ?></div>
<?php } ?>
<?php if(isset($_SESSION['success'])) { ?>
    <div class="alert alert-success"><?php echo $this->session->flashdata('success'); ?></div>
<?php } ?>
<div class="clearfix"></div>
<div class="row">
  <div class="col-md-12">
    <h1><?php echo $this->lang->line("content_welcome"); ?></h1>
    <p><?php echo $this->lang->line("content_header"); ?></p>

    <p><?php echo $this->lang->line("content_message"); ?></p>

    <ul style="list-style:none;">
      <li><?php echo $this->lang->line("content_point1"); ?></li>
      <li><?php echo $this->lang->line("content_point2"); ?></li>
      <li><?php echo $this->lang->line("content_point3"); ?></li>
      <li><?php echo $this->lang->line("content_point4"); ?></li>
      <li><?php echo $this->lang->line("content_point5"); ?></li>
    </ul>
  <div>
</div>
<br><br><br>
<div class="row">
  <?php foreach($rooms as $room) { ?>

  <div class="col-md-4">
    <div class="card card-primary card-outline">
      <div class="card-body">
        <h5 class="card-title"><?php echo $room['name']; ?></h5>
        <br><br>
        <p class="card-text">
          <?php echo $this->lang->line('room_detail1') . ' ' . $room['maxUsersPerRoom']; ?>
        </p>
        <p class="card-text">
          <?php echo $this->lang->line('room_detail2'); ?>
          <?php echo empty($room['dt_created']) ? '-' : date("d-m-Y H:i:s", strtotime($room['dt_created'])); ?>
        </p>
        <p class="card-text">
          <a href="/<?php echo $room['code']; ?>" class="btn btn-primary" style="margin: 5px;"> <?php echo $this->lang->line('room_link'); ?> </a>&nbsp;
          <a href="javascript:;" class="btn btn-primary" onclick="copyText('<?php echo $room['code']; ?>')" style="margin: 5px;"> <?php echo $this->lang->line('room_copy_link'); ?> </a>
        </p>
      </div>
    </div>
  </div>
  <?php } ?>
</div>
<script>
  function copyText(id) {
    var roomurl = 'https://<?php echo $this->input->server('HTTP_HOST'); ?>/' + id;
    if (navigator.share) {
        navigator.share({
            title: 'CoVideo Invitation',
            text: 'CoVideo Invitation',
            url: roomurl,
        }).then(() => toastr["success"]("Link shared successfully!"))
        .catch((error) => {
            if (event.ctrlKey || event.shiftKey || event.metaKey ||
                (event.button && event.button === 1)) {
                return;
            }
            event.preventDefault();
            navigator.clipboard.writeText(roomurl).then(() => toastr["success"]("Link copied to clipboard!"));
        });
    } else {
        if (event.ctrlKey || event.shiftKey || event.metaKey ||
            (event.button && event.button === 1)) {
            return;
        }
        event.preventDefault();
        navigator.clipboard.writeText(roomurl).then(() => toastr["success"]("Link copied to clipboard!"));
    }
  }
</script>
