<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Subscriptions extends CI_Controller {

    public $data;
    
    public function __construct() {
        parent::__construct();
        if(!isset($_SESSION['user_logged'])) {
            $this->session->set_flashdata("error", "Please login first to view this page");
            redirect("login");
        }
        $this->load->model('subscriptions_model');
        $this->userid = $this->session->userdata('userid');
    }

	public function index()	{
        $this->template->set('title', 'Subscriptions');
        $this->template->load('default_layout', 'contents' , 'index', $this->data);
    }
}
