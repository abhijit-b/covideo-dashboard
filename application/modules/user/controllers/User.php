<?php

class User extends MX_Controller {
    
    public $data;

    public function __construct() {
        parent::__construct();
        $this->load->model('users_model');
        $this->data['userid'] = $this->session->userdata('userid');
        if($this->data['userid'] == 1) {
            $this->data['users'] = $this->users_model->getAllUsers();
        }
        $this->lang->load("user", $this->session->userdata('site_lang'));
        $this->lang->load("default_layout", $this->session->userdata('site_lang'));
    }

    public function edit() {
        if(!isset($_SESSION['user_logged'])) {
            $this->session->set_flashdata("error", "Please login first to view this page");
            redirect("login");
        }

        if(!empty($this->input->post())) {
            $user = array(
                'fullname' =>$this->input->post('fullname'),
                'username' => (!empty($this->input->post('username'))) ? $this->input->post('username') : $this->input->post('email'),
                'email' => $this->input->post('email'),
                'company' => $this->input->post('company'),
                'phone' => $this->input->post('phone'),
            );
            $this->users_model->updateUserInfo($user, $this->data['userid']);
            $this->session->set_flashdata("success", "User details updated successfully.");
            redirect('/user/view', 'refresh');
        } else {
            $this->data['userinfo'] = $this->users_model->getUser($this->data['userid']);
        }

        $this->template->set('title', 'Users - Edit');
        $this->template->load('default_layout', 'contents' , 'user/edit', $this->data);
    }

    public function view() {
        if(!isset($_SESSION['user_logged'])) {
            $this->session->set_flashdata("error", "Please login first to view this page");
            redirect("login");
        }
        if(!empty($this->input->post('done'))) {
            redirect('user', 'refresh');
        } else {
            $this->load->library('Parseuseragent');
            $this->data['login_activity'] = $this->users_model->fetchLoginActivity($this->session->userdata('username'));
            $this->data['userinfo'] = $this->users_model->getUser($this->data['userid']);
        }

        $this->template->set('title', 'Users - View');
        $this->template->load('default_layout', 'contents' , 'user/view', $this->data);
    }

    public function privacy() {
        if(!isset($_SESSION['user_logged'])) {
            $this->session->set_flashdata("error", "Please login first to view this page");
            redirect("login");
        }

        $this->template->set('title', 'Users - Privacy');
        $this->template->load('default_layout', 'contents' , 'user/privacy', $this->data);
    }

    public function index() {
        if(!isset($_SESSION['user_logged'])) {
            $this->session->set_flashdata("error", "Please login first to view this page");
            redirect("login");
        } else if(!in_array($this->session->userdata('role'), array('admin','super'))) {
            $this->session->set_flashdata("error", "403 FORBIDDEN : Access Denied You don't have permission to access.");
            redirect('rooms');
        }

        $this->template->set('title', 'Users - List');
        $this->template->load('default_layout', 'contents' , 'user/list', $this->data);
    }

    public function access() {
        if(count($this->input->post()) > 0) {
            if($this->input->post('signuptype') == 'phone' || $this->input->post('logintype') == 'phone') {
                $user = $this->users_model->checkUser(array( 'phone' => $this->input->post('phonenumber')));
            } else {
                $user = $this->users_model->checkUser(array( 'email' => $this->input->post('email')));
            }
            if(empty($user)) {
                $_POST['signuptype'] = empty($this->input->post('signuptype')) ? $this->input->post('logintype') : $this->input->post('signuptype');
                $this->signup();
            } else {
                $_POST['logintype'] = empty($this->input->post('signuptype')) ? $this->input->post('logintype') : $this->input->post('signuptype');
                $this->login();
            }
        } else {
            if(!empty($code = $this->uri->segment(2))) {
                $this->data['invite_code'] = $code;
            } else {
                $this->data['invite_code'] = '';
            }

            $this->data["facebook_button"] = $this->lang->line("access_facebook_button");
            $this->template->set('title', 'Access');
            $this->template->load('plain_layout', 'contents' , 'user/access', $this->data);    
        }
    }

    public function login() {
        if(count($this->input->post()) > 0) {
            $logintype = $this->input->post('logintype');
            switch($logintype) {
                case 'phone':
                    $this->form_validation->set_rules('phonenumber', 'Phone', 'required|min_length[10]|max_length[12]');
                    $data['phone'] = $this->input->post('phonenumber');
                    break;
                case 'fb':
                case 'google':
                    $this->form_validation->set_rules('email', 'Email', 'trim|required');
                    $data['email'] = $this->input->post('email');
                    break;
                case 'normal':
                    $this->form_validation->set_rules('email', 'Email', 'trim|required');
                    $this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[5]');
                    $data['email'] = $this->input->post('email');
                    break;
                default:
                    $data['email'] = 'somebody@lost.com'; // default case of invalid login
                    break;
            }
            
            if($this->form_validation->run() == TRUE) {
                // check in database
                $user = $this->users_model->checkUser($data);
                if(is_null($user)) {
                    $this->session->set_flashdata('error', 'These was an error logging you in, please try again.');
                    redirect("login", "refresh");
                } else if(!$user->active) {
                    $this->session->set_flashdata('error', 'Please verify your email to access your account.');
                    redirect("login", "refresh");
                }
                if($logintype == 'normal' && !$this->bcrypt->verify($this->input->post('password'), $user->password)) {
                    $this->session->set_flashdata('error', 'Invalid credentials.');
                    redirect("access", "refresh");
                }

                $this->session->set_userdata('user_logged', TRUE);
                $this->session->set_userdata('username', $user->userid);
                $this->session->set_userdata('useremail', $user->email);
                $this->session->set_userdata('fullname', $user->fullname);
                $this->session->set_userdata('role', $user->type);

                $data = array(
                    "ip_address" => $this->input->ip_address(), 
                    "last_login" => time(),
                    "profile_image_url" => empty($user->profile_image_url) ? $this->input->post('profileimg') : $user->profile_image_url
                );
                $this->users_model->updateUserInfo($data, $user->id);

                // add user login info
                $login_activity = array();
                $login_activity['userid'] = $user->userid;
                $login_activity['ip'] = $this->input->ip_address();
                $login_activity['user_agent'] = $this->input->server('HTTP_USER_AGENT');
                $login_activity['created_at'] = time();
                $this->users_model->saveLoginActivity($login_activity);
                $this->session->set_userdata('userid', $user->id);
                redirect("dashboard", "refresh");
            }
        }

        redirect('/access','refresh');

        $this->data['country_codes'] = json_decode(file_get_contents(DOCROOT . '/database/country_code.json'), true);
        $this->template->set('title', 'Login');
        $this->template->load('plain_layout', 'contents' , 'user/login', $this->data);
    }

    public function signup($access = false) {
        if(count($this->input->post()) > 0) {
            $signuptype = $this->input->post('signuptype');
            switch($signuptype) {
                case 'phone':
                    $this->form_validation->set_rules('phonenumber', 'Phone', 'required|min_length[10]|max_length[12]');
                    $data['phone'] = $this->input->post('phonenumber');
                    break;
                case 'fb':
                case 'google':
                    $this->form_validation->set_rules('email', 'Email', 'trim|required');
                    $data['email'] = $this->input->post('email');
                    break;
                case 'normal':
                    $this->form_validation->set_rules('email', 'Email', 'trim|required');
                    $this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[5]');
                    $data['email'] = $this->input->post('email');
                    break;
                default:
                    $data['email'] = 'somebody@lost.com'; // default case of invalid login
                    break;
            }
            
            if($this->form_validation->run() == TRUE) {
                //generate simple random code
			    $set = '123456789abcdefghijklmnopqrstuvwxyz';
			    $numbers = '123456789';
                $code = substr(str_shuffle($set), 0, 12);
                $password = !empty($this->input->post('password')) ? $this->input->post('password') : substr(str_shuffle($set), 0, 12);
                $user = array(
                    'userid' => !empty($this->input->post('userid')) ? $this->input->post('userid') : substr(str_shuffle($set), 0, 15), 
                    'fullname' => !empty($this->input->post('fullname')) ? $this->input->post('fullname') : substr(str_shuffle($numbers), 0, 15),
                    'username' => $this->input->post('email'),
                    'email' => $this->input->post('email'),
                    'profile_image_url' => !empty($this->input->post('profileimg')) ? $this->input->post('profileimg') : '',
                    'signup_type' => $this->input->post('signuptype'),
                    'phonecode' => !empty($this->input->post('phonecode')) ? $this->input->post('phonecode') : '',
                    'phone' => !empty($this->input->post('phonenumber')) ? $this->input->post('phonenumber') : '',
                    'invite_code' => !empty($this->input->post('invite_code')) ? $this->input->post('invite_code') : '',
                    'code' => substr(str_shuffle($set), 0, 15),
                    'password' => $this->bcrypt->hash($password),
                    'created_on' => time(),
                    'activation_code' => $code,
                    'active' => TRUE,
                    'type' => 'user'
                );

                if(!empty($this->input->post('email'))) {
                    $userdata = $this->users_model->checkUser(array('email' => $this->input->post('email')));
                } else {
                    $userdata = $this->users_model->checkUser(array('phone' => $this->input->post('phonenumber')));
                }
                if($userdata) {
                    $message = 'User with these details already exists. Please Login instead.';
                    $this->session->set_flashdata("error", $message);
                    redirect("login", "refresh");
                } else {
                    $id = $this->users_model->insert($user);
                }
                // $qstring = $this->base64url_encode($code);
                // $subject="Covideo Verify Email";
                // $message= 'Please click this link to verify your email address 
                // ' . base_url() . 'user/verify/' . $qstring;	      
                //$this->mail($this->input->post('email'), $subject, $message);

                // generate room name
                if(strlen($user['fullname']) == 0) {
                    $name = substr(str_shuffle($numbers), 0, 15);
                } else if(strlen($user['fullname']) < 15) {
                    $name = str_replace(' ', '', $user['fullname']);
                } else {
                    $name = explode(" ", $user['fullname'])[0];
                }
                $roomname = $name.substr(str_shuffle($numbers), 0, 15);
                $roomid = strtolower(substr($roomname, 0, 15));

                // api call to generate room
                $this->load->library('covideoapi');
                $covideo_api = new CovideoAPI();
                $response = $covideo_api->createRoom(!empty($user['fullname']) ? $user['fullname'] : $name, $user['userid'], $roomid);
                if(isset($response['status']) && $response['status'] == 'error') {
                    $message = "ERR01 : There was some problem registering your account. Please try again.";
                    $this->session->set_flashdata("error", $message);
                    redirect("signup", "refresh");    
                }

                // add user login info
                $login_activity = array();
                $login_activity['userid'] = $user['userid'];
                $login_activity['ip'] = $this->input->ip_address();
                $login_activity['user_agent'] = $this->input->server('HTTP_USER_AGENT');
                $login_activity['created_at'] = time();
                $this->users_model->saveLoginActivity($login_activity);

                // call DRIP API to add user as a subscriber if user provided email
                if(!empty($user['email'])) {
                    $fullname = !empty($user['fullname']) ? $user['fullname'] : substr($user['email'], 0, strpos($user['email'], '@'));
                    $this->load->library('dripapi');
                    $drip_api = new DripAPI();
                    $subscriber_details = $drip_api->createSubscriber($user['email'], ucfirst($fullname));
                    $subscriber = array();
                    $subscriber['userid'] = $user['userid'];
                    $subscriber['dripid'] = $subscriber_details['subscribers'][0]['id'];
                    $subscriber['link'] = $subscriber_details['subscribers'][0]['href'];
                    $subscriber['status'] = $subscriber_details['subscribers'][0]['status'];
                    $subscriber['ip'] = $this->input->ip_address();
                    $subscriber['user_agent'] = $this->input->server('HTTP_USER_AGENT');
                    $subscriber['created_at'] = time();

                    $details = $this->users_model->getDripSubscriber($user['userid']);
                    if(empty($details)) {
                        $this->users_model->saveDripSubscriber($subscriber);
                    }
                }
        
                $this->session->set_userdata('user_logged', TRUE);
                $this->session->set_userdata('username', $user['userid']);
                $this->session->set_userdata('useremail', $user['email']);
                $this->session->set_userdata('fullname', $user['fullname']);
                $this->session->set_userdata('role', $user['type']);
                $this->session->set_userdata('userid', $id);
                redirect('/dashboard', 'refresh');
            } else {

                $error = $this->form_validation->error_array();
                if(array_key_exists('email', $error)) {
                    $message = 'User with this email address already exists. Please Login instead.';
                    $this->session->set_flashdata("error", $message);
                    redirect("login", "refresh");    
                } else {
                    $message = "There was some problem registering your account. Please try again.";
                    $this->session->set_flashdata("error", $message);
                    redirect("signup", "refresh");
                }    
            }
        }
        if(!empty($code = $this->uri->segment(2))) {
            // check invite code using api to admin
            // $this->load->library('covideoadminapi');
            // $covideoadminapi = new Covideoadminapi();
            // $user = $covideoadminapi->fetchUsersByInviteCode($code);
            // $user = json_decode($user, TRUE);
            // if(isset($user['error'])) {
            //     $this->template->set('title', 'Register');
            //     $this->template->load('plain_layout', 'contents' , 'user/interstitial');
            //     return;
            // }
            $this->data['invite_code'] = $code;
        } else {
            $this->data['invite_code'] = '';
            // $this->template->set('title', 'Register');
            // $this->template->load('plain_layout', 'contents' , 'user/interstitial');
            // return;
        }
        redirect('/access','refresh');

        $this->data['country_codes'] = json_decode(file_get_contents(DOCROOT . '/database/country_code.json'), true);
        $this->template->set('title', 'Register');
        $this->template->load('plain_layout', 'contents' , 'user/register', $this->data);
    }

    public function add() {
        if(!in_array($this->session->userdata('role'), array('admin','super'))) {
            $this->session->set_flashdata("error", "403 FORBIDDEN : Access Denied You don't have permission to access.");
            redirect('rooms');
        }
        if(!empty($this->input->post('add'))) {
            $this->form_validation->set_rules('first_name', 'first_name', 'trim|required');
            $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
            $this->form_validation->set_rules('username', 'Username', 'trim|required');
            $this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[5]');

            if($this->form_validation->run() == TRUE) {
                //generate simple random code
			    $set = '123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
			    $code = substr(str_shuffle($set), 0, 12);

                $user = array(
                    'username' => $this->input->post('username'),
                    'email' => $this->input->post('email'),
                    'password' => md5($this->input->post('password')),
                    'first_name' => $this->input->post('first_name'),
                    'last_name' => $this->input->post('last_name'),
                    'company' => $this->input->post('company'),
                    'phone' => $this->input->post('phone'),
                    'created_on' => time(),
                    'activation_code' => $code,
                    'active' => TRUE,
                    'type' => 'user',
                    'entity_id' => $this->data['userid']
                );
                $id = $this->users_model->insert($user);

                $qstring = $this->base64url_encode($code);
                $subject="Covideo Verify Email";
                $message= 'Please click this link to verify your email address 
                ' . base_url() . 'user/verify/' . $qstring;	      
                $this->mail($this->input->post('email'), $subject, $message);

                $this->session->set_flashdata("success", "User added successfully.");
                redirect("user", "refresh");
            }
        }
        $this->template->set('title', 'User - Add');
        $this->template->load('default_layout', 'contents' , 'user/add');
    }

    public function support() {
        $this->template->set('title', 'Support');
        $this->template->load('default_layout', 'contents' , 'user/support');
    }

    public function delete() {
        if(!in_array($this->session->userdata('role'), array('admin','super'))) {
            $this->session->set_flashdata("error", "403 FORBIDDEN : Access Denied You don't have permission to access.");
            redirect('dashboard');
        }
        $userid = json_decode($this->input->post('userid'));
        $this->users_model->deleteById($userid);
        $this->session->set_flashdata("success", "User deleted successfully.");
        echo json_encode(array("success" => "User deleted successfully."));
    }

    public function change() {
        if(!isset($_SESSION['user_logged'])) {
            $this->session->set_flashdata("error", "Please login first to view this page");
            redirect("access", "refresh");
        }
        if(!empty($this->input->post())) {
            $this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[5]');
            if($this->form_validation->run() == TRUE) {
                $userdata['password'] = $this->bcrypt->hash($this->input->post('password'));
                $this->users_model->updateUserInfo($userdata, $this->input->post('userid'));
                $this->session->set_flashdata("success", 'Password changed successfully.');
                redirect('user/view', 'refresh');
            }
        } else {
            $this->data['userid'] = $this->data['userid'];

            $this->template->set('title', 'Change Password');
            $this->template->load('default_layout', 'contents' , 'user/change', $this->data);
        }
    }

    public function reset() {

        if(!empty($this->input->post('reset'))) {
            $this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[5]');
            $this->form_validation->set_rules('confirmPassword', 'Confirm Password', 'trim|required|min_length[5]|matches[password]');

            if($this->form_validation->run() == TRUE) {
                $this->data['password'] = md5($this->input->post('password'));
                $this->data['forgotten_password_code'] = '';
                $this->users_model->updateUserInfo($this->data, $this->input->post('userid'));
                $this->session->set_flashdata("success", 'Your password has been reset. You can login now. <a href="'.base_url(). 'user/login/" class="alert-link"> Login </a>');
                $this->data['show_input'] = FALSE;
                
                $this->template->set('title', 'Reset Password');
                $this->template->load('plain_layout', 'contents' , 'user/reset-password', $this->data);
            }
        } else {
            $token = base64_decode($this->uri->segment(3));
            $userInfo = $this->users_model->checkToken($token);
            if(empty($userInfo)) {
                $this->session->set_flashdata("error", "Invalid token or token has expired.");
                $this->data['show_input'] = FALSE;
                $this->template->set('title', 'Reset Password');
                $this->template->load('plain_layout', 'contents' , 'user/reset-password', $this->data);
            } else {
                $this->data['userid'] = $userInfo->id;
                $this->data['show_input'] = TRUE;
                $this->template->set('title', 'Reset Password');
                $this->template->load('plain_layout', 'contents' , 'user/reset-password', $this->data);
            }
        }
    }

    public function verify() {
        $code = base64_decode($this->uri->segment(3));
        if($this->users_model->checkActivationCode($code)) {
            $this->session->set_flashdata("success", 'Your email address has been verified successfully. You can login now. <a href="'.base_url(). 'user/login/" class="alert-link"> Login </a>');
            $this->data['show_input'] = FALSE;
            $this->template->set('title', 'Verify Email');
            $this->template->load('plain_layout', 'contents' , 'user/reset-password', $this->data);
        } else {
            redirect('login', 'refresh');
        }
    }

    public function forgot() {
        if(!empty($this->input->post('forgotPassword'))) {
            $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');

            if($this->form_validation->run() == TRUE) {

                //generate simple random code
			    $set = '123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
			    $code = substr(str_shuffle($set), 0, 12);

                $userInfo = $this->users_model->getUserByEmail($this->input->post('email'));
                if(!$userInfo){
                    $this->session->set_flashdata('error', 'We cant find your email address');
                    redirect('user/forgot', 'refresh');
                }

                $token = substr(sha1(rand()), 0, 30); 
                $data = array('forgotten_password_code' => $token);
                $this->users_model->updateForgotPasswordCode($data, $userInfo->id);
                $qstring = $this->base64url_encode($token);
                $url = base_url() . 'user/reset/' . $qstring;
                $link = '<a href="' . $url . '">' . $url . '</a>';
                $subject="Covideo Reset Password"; //subject
                $message= 'Please click this link to reset your password 
                ' . base_url() . 'user/reset/' . $qstring;	      
                $this->mail($this->input->post('email'), $subject, $message);
                $this->session->set_flashdata("success", "We have sent you an email with a link to reset your password.");
                redirect("user/forgot", "refresh");
            } else {
                $this->session->set_flashdata("error", "Please provide an valid email address.");
                redirect("user/forgot", "refresh");
            }
        }
        $this->template->set('title', 'Forgot Password');
        $this->template->load('plain_layout', 'contents' , 'user/forgot');
    }

    public function logout() {
        unset($_SESSION);
        session_destroy();
        redirect("login", "refresh");
    }

    public function mail($address, $subject, $message) {
        $this->load->library('email'); 
        $this->email->from('no-reply@covideo.in', 'Covideo.in'); //sender's email
        $this->email->to($address);
        $this->email->subject($subject);
        $this->email->message($message);
        if($this->email->send()) {
            return TRUE;
        }
    }

    public function base64url_encode($data) { 
        return rtrim(strtr(base64_encode($data), '+/', '-_'), '='); 
    }

    public function base64url_decode($data) { 
        return base64_decode(str_pad(strtr($data, '-_', '+/'), strlen($data) % 4, '=', STR_PAD_RIGHT)); 
    } 
}