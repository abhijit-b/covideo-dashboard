
    <main role="main" class="col-md-10 ml-sm-auto col-lg-10 px-4">
      <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h4 class="h4">Change Password</h4>
      </div>  
      <div class="col-md-6">
        <form method="POST">
          <div class="form-group">
            <input class="form-control" name="password" id="password" type="password"  value="<?php echo set_value('password'); ?>" placeholder="Password">
          </div>
          <div class="form-group">
              <input class="form-control" name="userid" type="hidden"  value="<?php echo $userid; ?>">
          </div>
          <div>
              <button class="btn btn-primary" name="change">Change</button>
          </div>
        </form>
      </div>
    </main>
<div class="spacer10"></div>

