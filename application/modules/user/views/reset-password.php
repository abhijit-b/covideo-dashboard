
<div class="centereddiv">
    <div class="shadow rounded">
        <div class="col-lg-12">
            <img class="img-fluid" src="<?php echo base_url(); ?>assets/images/logo.png">
            <div class="spacer10"></div>
        </div>
        <div class="col-lg-12">
            <?php if(isset($_SESSION['error'])) { ?>
                <div class="alert alert-danger"><?php echo $_SESSION['error']; ?></div>
            <?php } ?>
            <?php if(isset($_SESSION['success'])) { ?>
                <div class="alert alert-success"><?php echo $_SESSION['success']; ?></div>
            <?php } ?>
            <?php echo validation_errors('<div class="alert alert-danger">', '</div>'); ?>
            <?php if($show_input) { ?>
              <p class="lead">Please enter your new password</p>
              <form method="POST">
                  <div class="form-group">
                      <input class="form-control" name="password" id="password" type="password"  value="<?php echo set_value('password'); ?>" placeholder="Password" autofocus>
                  </div>
                  <div class="form-group">
                      <input class="form-control" name="confirmPassword" id="confirmPassword" type="password"  value="<?php echo set_value('confirmPassword');?>" placeholder="Confirm Password">
                  </div>
                  <div class="form-group">
                      <input class="form-control" name="userid" type="hidden"  value="<?php echo $userid; ?>">
                  </div>
                  <div>
                      <button class="btn btn-primary" name="reset">Reset</button>
                  </div>
              </form>
            <?php } ?>
            <div class="spacer10"></div>
        </div>
    </div>
</div>
