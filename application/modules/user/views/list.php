
    <main role="main" class="col-md-12 ml-sm-auto col-lg-12 px-4">
      <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h3><i class="fa fa-users"></i> List of Users</h3>
      </div>
      <div class="spacer10"></div>
      <?php if(isset($_SESSION['error'])) { ?>
          <div class="alert alert-danger"><?php echo $_SESSION['error']; ?></div>
      <?php } ?>
      <?php if(isset($_SESSION['success'])) { ?>
          <div class="alert alert-success"><?php echo $_SESSION['success']; ?></div>
      <?php } ?>
      <?php echo validation_errors('<div class="alert alert-danger">', '</div>'); ?>
      <div class="spacer10"></div>      
      <p>      
        <div class="btn-toolbar">
          <div class="btn-group">
              <label class="btn btn-primary" id="edit"><i class="fa fa-edit"></i> Edit</label>
              <label class="btn btn-primary" id="view"><i class="fa fa-file"></i> View</label>
              <label class="btn btn-primary" id="add"><i class="fa fa-plus"></i> Add</label>
              <label class="btn btn-primary" id="delete"><i class="fa fa-trash"></i> Delete</label>
          </div>
        </div>
      </p>
      <div class="spacer10"></div>
      <div class="alert alert-danger hidden" id="error-message">No user selected.</div>
      <div class="spacer10"></div>
      <div class="alert alert-danger hidden" id="delete-confirmation-message"> 
        <p class="lead">Are you sure you want to delete this user?</p> 
        <p>
          <button type="button" class="btn btn-danger" id="delete-confirmation">Delete</button> 
          <button type="button" class="btn btn-default" id="cancel">Cancel</button> 
        </p> 
      </div>
      <div class="table-responsive">
        <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive" cellspacing="0" width="100%">
          <thead>
            <tr class="thead-dark">
              <th>#</th>
              <th>Username</th>
              <th>Email</th>
              <th>IP Address(s)</th>
              <th>Company</th>
              <th>Last Login</th>
              <th>&nbsp;</th>
            </tr>
          </thead>
          <tbody>
          <?php
            foreach($users as $index => $row){
              ?>
              <tr>
                <td>
                  <div class="form-check">
                    <?php if($row->type != 'super') { ?>
                      <input class="form-check-input" type="checkbox" value="<?php echo $row->id; ?>">
                    <?php } ?>
                  </div>
                </td>
                <td><?php echo $row->username; ?></td>
                <td><?php echo $row->email; ?></td>
                <td><?php echo $row->ip_address; ?></td>
                <td><?php echo $row->company; ?></td>
                <td><?php echo empty($row->last_login) ? '-' : date("d-m-Y H:i:s", $row->last_login); ?></td>
                <td><a class="badge badge-primary" href="<?php echo base_url().'user/change/'.$row->id; ?>">Change Password</a></td>
              </tr>
              <?php
            }
          ?>
          </tbody>
        </table>
      </div>
    </main>
<script src="<?php echo base_url(); ?>assets/js/user.js"></script>

