<br><br>
<?php if(isset($_SESSION['error'])) { ?>
    <div class="alert alert-danger"><?php echo $this->session->flashdata('error'); ?></div>
<?php } ?>
<?php if(isset($_SESSION['success'])) { ?>
    <div class="alert alert-success"><?php echo $this->session->flashdata('success'); ?></div>
<?php } ?>

<div class="card text-center">
  <h3>Welcome to CoVideo!</h3>
  Signup is by invite only. <br> Please go here <a href="https://covideo.in" target="_blank" style="text-decoration: underline;color:#FFA365;font-weight:bold;margin:0px">https://covideo.in/</a> and register yourself for an invite. Thank you.
  <br><br>
</div>

