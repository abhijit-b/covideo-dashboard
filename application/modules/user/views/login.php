<div class="card">
    <div class="card-body login-card-body">
        <form name="login" method="POST" id="login">
            <?php if(isset($_SESSION['error'])) { ?>
            <div class="alert alert-danger"><?php echo $this->session->flashdata('error'); ?></div>
            <?php } ?>
            <?php if(isset($_SESSION['success'])) { ?>
            <div class="alert alert-success"><?php echo $this->session->flashdata('success'); ?></div>
            <?php } ?>
            <?php echo validation_errors('<div class="alert alert-danger">', '</div>'); ?>
            <br>
            <div class="product_social">
                <ul class="list-inline">
                    <a href="javascript:;" class="fb btn-social" onclick="fb_login();">
                        <i class="fab fa-facebook-f mr-2" style="padding-right: 10px;"></i> Login with Facebook
                    </a>
                    <!-- <a href="javascript:;" class="twitter btn-social">
                            <i class="fab fa-twitter mr-2"></i> Login with Twitter
                        </a> -->
                    <a href="javascript:;" class="google btn-social" onclick="googleLoginIn()">
                        <i class="fab fa-google mr-2" style="padding-right: 10px;"></i> Login with Google
                    </a>
                    <a class="phone btn-social" data-toggle="modal" data-target="#phoneModal">
                        <i class="fa fa-mobile fa-lg mr-2" style="padding-right: 10px;"></i> Login with Phone
                    </a>
                </ul>
            </div>
            <input type="hidden" name="logintype" value="">
            <input type="hidden" name="userid" value="">
            <input type="hidden" name="email" value="">
            <input type="hidden" name="phonecode" value="">
            <input type="hidden" name="phonenumber" value="">

            <hr>
            <div class="text-center">
                <p>Don't have an account?
                    <a href="<?php echo base_url(); ?>signup" class="to_register"  style="text-decoration: underline;color:#80C06F;font-weight:bold;margin:0px">Sign up for free</a>
                </p>
            </div>
        </form>
    </div>
    <!-- /.login-card-body -->
</div>
<div class="modal fade" id="phoneModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4>Enter your phone number</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="phoneloginform" method="POST">
                    <div class="container-fluid" style="padding-left: 0px">
                        <div class="row">
                            <div class="col-md-4">
                                <label>Code</label><br>
                                <select class="form-control" name="countrycode">
                                    <?php
                                        foreach($country_codes as $country) {
                                            if(!empty($country['code']) && ($country['code'] == 'IN')) {
                                    ?>
                                    <option value="<?php echo $country['dial_code'] ?>"
                                        <?php echo ($country['code'] == 'IN') ? 'selected' : ''; ?>>
                                        <?php echo $country['dial_code'] ?> </option>
                                    <?php
                                            }
                                        }
                                    ?>
                                </select>
                            </div>
                            <div class="col-md-8">
                                <label>Number</label><br>
                                <input type="text" name="phonenumber" class="form-control" value="" onkeyup="checkNumber();">
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="phonelogin">Verify</button>
                <label class="small text-secondary">
                    By clicking verify, an SMS will be sent to the phone number. Message and data rates may apply.
                </label>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="verifyModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4>Verify your phone number</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="phoneverifyform" method="POST">
                    <div class="container-fluid" style="padding-left: 0px">
                        <div class="row">
                            <div class="col-md-12">
                                <label>Enter the 6 digit code we sent to <span id="verify-no" class="text-primary" style="color:#80C06F !important"></span></label>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-6">
                                <input type="text" name="otp" class="form-control" value="" placeholder="6 digit code">
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="phoneverify">Continue</button>
                <label class="small text-secondary">
                    <!-- Resend Code. -->
                </label>
            </div>
        </div>
    </div>
</div>
<script>
function init() {
    gapi.load('auth2', function() {
        gUser = gapi.auth2.init({
            'client_id': '879497195593-730scmceuq7ee0eb5m6hfrffh346cbhs.apps.googleusercontent.com',
            fetch_basic_profile: true,
            scope: 'profile'
        });
    });
}

function googleLoginIn() {
    if(typeof gUser == "undefined"){
        init();
    }
    gUser.signIn().then(function(googleUser) {
        loginWithGoogle(googleUser);
    }, function() {
        console.log('User denied access');
    });
}

function loginWithGoogle(googleUser) {
    var profile = googleUser.getBasicProfile();
    $('#login input[name=userid]').val(profile.getId());
    $('#login input[name=email]').val(profile.getEmail());
    $('#login input[name=logintype]').val('google');
    $('#login').submit();
}

$('#phonelogin').on('click', function() {
    var code = $('#phoneloginform select[name="countrycode"]').val(),
        phone = $('#phoneloginform input[name="phonenumber"]').val();
    if (code != '' && phone != '') {
        $.ajax({
            url: "/sms/send",
            type: "POST",
            data: {
                "phone": phone,
                "code": code
            },
            dataType: "JSON",
            success: function(data) {
                if (data.status == 'success') {
                    $('#phoneModal').modal('hide');
                    $('#verify-no').html(code + phone);
                    $('#verifyModal').modal('show');
                }
            },
            error: function(e) {}
        });
    }
});

$('#phoneverify').on('click', function() {
    var otp = $('#phoneverifyform input[name="otp"]').val(),
        code = $('#phoneloginform select[name="countrycode"]').val(),
        phone = $('#phoneloginform input[name="phonenumber"]').val();
    if (otp != '' && phone != '') {
        $.ajax({
            url: "/sms/verify",
            type: "POST",
            data: {
                "phone": phone,
                "code": otp
            },
            dataType: "JSON",
            success: function(data) {
                if (data.status == 'success') {
                    $('#verifyModal').modal('hide');
                    $('#login input[name=phonecode]').val(code);
                    $('#login input[name=phonenumber]').val(phone);
                    $('#login input[name=logintype]').val('phone');
                    $('#login').submit();
                }
            },
            error: function(e) {}
        });
    }
});

function checkNumber() {
    var phoneRegex = /^\d{10}$/,
        phone = $('#phoneloginform input[name="phonenumber"]');
    if ((phone.val().match(phoneRegex))) {
        phone.removeClass('has-error');
        return true;
    } else {
        phone.addClass('has-error');
        return false;
    }
}
</script>