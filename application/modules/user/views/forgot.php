
<div>
      <div class="login_wrapper">
        <div class="form login_form">
          <section class="login_content">
            <img class="img-fluid" src="<?php echo base_url(); ?>assets/images/logo.png">
            <form method="POST">
                <h1>Reset Password</h1>
                <?php if(isset($_SESSION['error'])) { ?>
                    <div class="alert alert-danger"><?php echo $this->session->flashdata('error'); ?></div>
                <?php } ?>
                <?php if(isset($_SESSION['success'])) { ?>
                    <div class="alert alert-success"><?php echo $this->session->flashdata('success'); ?></div>
                <?php } ?>
                <?php echo validation_errors('<div class="alert alert-danger">', '</div>'); ?>
                <p class="lead">Enter the email address associated with your account, and we’ll email you a link to reset your password.</p>

              <div>
                <input type="text" class="form-control" name="email" id="email" value="<?php echo set_value('email'); ?>" required="" placeholder="Email Address"/>
              </div>
              <div>
                <button class="btn btn-default submit" name="forgotPassword">Forgot</button>
              </div>
              <div class="clearfix"></div>
              <div class="separator">
                <p class="change_link">Already have an account?
                    <a href="<?php echo base_url(); ?>user/login" class="to_register"> Login </a>
                </p>
                <div class="clearfix"></div>
              </div>
            </form>
          </section>
        </div>
    </div>
</div>