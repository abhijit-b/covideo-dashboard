

    <main role="main" class="col-md-10 ml-sm-auto col-lg-10 px-4">
      <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h4 class="h4">Edit user details</h4>
      </div>  
      <div class="col-md-6">
        <?php if(isset($_SESSION['error'])) { ?>
            <div class="alert alert-danger"><?php echo $this->session->flashdata('error'); ?></div>
        <?php } ?>
        <?php if(isset($_SESSION['success'])) { ?>
            <div class="alert alert-success"><?php echo $this->session->flashdata('success'); ?></div>
        <?php } ?>
        <?php echo validation_errors('<div class="alert alert-danger">', '</div>'); ?>
        <form method="POST" action="/user/edit">
            <input type="hidden" name="userid" value="<?php echo $userinfo->id; ?>">
            <div class="form-group">
              <label>First Name</label>
              <input class="form-control" name="first_name" id="FirstName" type="text" value="<?php echo set_value('first_name', $userinfo->first_name); ?>" placeholder="First Name" autofocus required>
            </div>
            <div class="form-group">
              <label>Last Name</label>
              <input class="form-control" name="last_name" id="lastName" type="text" value="<?php echo set_value('last_name', $userinfo->last_name); ?>" placeholder="Last Name" >
            </div>
            <div class="form-group">
              <label>Company</label>
              <input class="form-control" name="company" id="company" type="text" value="<?php echo set_value('company', $userinfo->company); ?>" placeholder="Company (optional)">
            </div>
            <div class="form-group">
              <label>Phone</label>
              <input class="form-control" name="phone" id="phone" type="tel" value="<?php echo set_value('phone', $userinfo->phone); ?>" placeholder="Phone (format: xxxxxxxxxx, Optional)" pattern="^\d{10}$">
            </div>
            <div class="form-group">
              <label>Username</label>
              <input class="form-control" name="username" id="username" type="text" value="<?php echo set_value('username', $userinfo->username); ?>" placeholder="Username" required>
            </div>
            <div class="form-group">
              <label>Email</label>
              <input class="form-control" name="email" id="email" type="text" value="<?php echo set_value('email', $userinfo->email); ?>" placeholder="Email" required>
            </div>
            <div class="spacer10"></div>
            <div>
                <button class="btn btn-primary mr-2" name="save">Save</button>
                <a href="/user" class="btn btn-outline-primary" name="cancel">Cancel</a>
            </div>
        </form>
      </div>
    </main>
<div class="spacer10"></div>
