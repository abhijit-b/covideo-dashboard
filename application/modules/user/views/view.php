<br><br>
<?php if(isset($_SESSION['success'])) { ?>
	<div class="alert alert-success"><?php echo $_SESSION['success']; ?></div>
<?php } ?>
<div class="card card-primary card-outline card-outline-tabs col-md-12" style="box-shadow: none;border-top:none;">
    <div class="card-header p-0 border-bottom-0">
        <ul class="nav nav-tabs" role="tablist">
            <li class="nav-item"><a class="nav-link active" href="#tab_1" data-toggle="tab"><?php echo $this->lang->line('profile_tab'); ?></a></li>
            <li class="nav-item"><a class="nav-link" href="#tab_2" data-toggle="tab"><?php echo $this->lang->line('profile_login_activity'); ?></a></li>
        </ul>
        <div class="card-body">
            <div class="tab-content">
                <div class="tab-pane active" id="tab_1" role="tabpanel">
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-3">
                                <!-- Profile Image -->
                                <div class="card card-outline">
                                    <div class="card-body box-profile">
                                        <div class="text-center">
                                            <?php if(!empty($userinfo->profile_image_url)) { ?>
                                                <img class="img-fluid profile-pic" src="<?php echo $userinfo->profile_image_url;?>" alt="User profile picture">
                                            <?php } else { ?>
                                                <?php $initials = strtoupper($userinfo->fullname[0].$userinfo->fullname[1]);?>
                                                <img class="img-fluid profile-pic" src="https://placehold.it/100x100/80C06F/000000?text=<?php echo $initials;?>" alt="User profile picture">
                                            <?php } ?>
                                        </div>
                                        <h3 class="profile-username text-center"><?php echo $userinfo->fullname; ?></h3>
                                    </div>
                                </div>
                            </div>
                            <!-- /.col -->
                            <div class="col-md-9">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="tab-content">
                                            <div class="tab-pane active" id="settings">
                                                <form class="form-horizontal" method="POST" action="/user/edit">
                                                    <div class="form-group row">
                                                        <label for="inputName" class="col-sm-2 col-form-label"><?php echo $this->lang->line('profile_name'); ?></label>
                                                        <div class="col-sm-10">
                                                            <input name="fullname" type="text" class="form-control" id="inputName" value="<?php echo $userinfo->fullname; ?>">
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label for="inputEmail" class="col-sm-2 col-form-label"><?php echo $this->lang->line('profile_email'); ?></label>
                                                        <div class="col-sm-10">
                                                            <input name="email" type="email" class="form-control" id="inputEmail" value="<?php echo $userinfo->email; ?>">
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label for="phone" class="col-sm-2 col-form-label"><?php echo $this->lang->line('profile_phone_number'); ?></label>
                                                        <div class="col-sm-10">
                                                            <input name="phone" type="text" class="form-control" id="phone" value="<?php echo $userinfo->phone; ?>">
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <div class="offset-sm-2 col-sm-10">
                                                            <button type="submit" class="btn btn-primary"><?php echo $this->lang->line('profile_submit'); ?></button>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane" id="tab_2">
                    <div class="box-body table-responsive ">
                        <table id="login-activity" class="table table-bordered table-hover">
                        <thead>
                            <tr class="thead-dark">
                                <th><?php echo $this->lang->line('profile_la_table_header1'); ?></th>
                                <th><?php echo $this->lang->line('profile_la_table_header2'); ?></th>
                                <th><?php echo $this->lang->line('profile_la_table_header3'); ?></th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php
                            $parser = new Parseuseragent();
                            foreach($login_activity as $index => $row){
                            ?>
                            <tr>
                                <td><?php 
                                    $parser->parseUserAgentString($row->user_agent); 
                                    echo $parser->fullname; 
                                ?></td>
                                <td><?php echo $row->ip; ?></td>
                                <td><?php echo empty($row->created_at) ? '-' : date("d-m-Y H:i:s", $row->created_at); ?></td>
                            </tr>
                            <?php
                            }
                        ?>
                        </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function(){
        $('#login-activity').DataTable({
            "pageLength" : 50,
            "language" : {
                "emptyTable":     "<?php echo $this->lang->line('emptyTable'); ?>",
                "info":           "<?php echo $this->lang->line('info'); ?>",
                "infoEmpty":      "<?php echo $this->lang->line('infoEmpty'); ?>",
                "infoFiltered":   "<?php echo $this->lang->line('infoFiltered'); ?>",
                "lengthMenu":     "<?php echo $this->lang->line('lengthMenu'); ?>",
                "search":         "<?php echo $this->lang->line('search'); ?>",
                "zeroRecords":    "<?php echo $this->lang->line('zeroRecords'); ?>",
                "paginate": {
                    "first":      "<?php echo $this->lang->line('first'); ?>",
                    "last":       "<?php echo $this->lang->line('last'); ?>",
                    "next":       "<?php echo $this->lang->line('next'); ?>",
                    "previous":   "<?php echo $this->lang->line('previous'); ?>"
                }
            }
        });
    });
</script>