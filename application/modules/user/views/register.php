<div class="card">
    <div class="card-body login-card-body">
        <form method="POST" id="signup" name="signup">
            <?php if(isset($_SESSION['error'])) { ?>
            <div class="alert alert-danger"><?php echo $this->session->flashdata('error'); ?></div>
            <?php } ?>
            <?php if(isset($_SESSION['success'])) { ?>
            <div class="alert alert-success"><?php echo $this->session->flashdata('success'); ?></div>
            <?php } ?>
            <?php echo validation_errors('<div class="alert alert-danger">', '</div>'); ?>
            <br>
            <div class="product_social">
                <ul class="list-inline">
                    <!-- <li><div class="g-signin2" data-onsuccess="onSignIn"></div></li> -->

                    <a href="javascript:;" class="fb btn-social" onclick="fb_login();">
                        <i class="fab fa-facebook-f mr-2"></i> Signup with Facebook
                    </a>
                    <!-- <a href="javascript:;" class="twitter btn-social">
                        <i class="fab fa-twitter mr-2"></i> Signup with Twitter
                    </a> -->
                    <a href="javascript:;" class="google btn-social" onclick="googleSignIn()">
                        <i class="fab fa-google mr-2"></i> Signup with Google
                    </a>
                    <a class="phone btn-social" data-toggle="modal" data-target="#phoneModal">
                        <i class="fa fa-mobile fa-lg mr-2" style="padding-right: 10px;"></i> Signup with Phone
                    </a>
                </ul>
            </div>
            <input type="hidden" name="invite_code" value="<?php echo !empty($invite_code) ? $invite_code : ''; ?>">
            <input type="hidden" name="userid" value="">
            <input type="hidden" name="fullname" value="">
            <input type="hidden" name="email" value="">
            <input type="hidden" name="signuptype" value="">
            <input type="hidden" name="phonecode" value="">
            <input type="hidden" name="phonenumber" value="">

            <div class="text-center">
                <label>
                    By signing up on CoVideo.in, you accept the <a href="https://covideo.in/terms-conditions/" style="text-decoration: underline;color:#FFA365;font-weight:bold;margin:0px" target="_blank">CoVideo Terms of Service</a> and the <a href="https://covideo.in/privacy-policy/" style="text-decoration: underline;color:#80C06F;font-weight:bold;margin:0px" target="_blank">Privacy Policy</a>.
                </label>
            </div>
            <hr>
            <div class="text-center">
                <label>Already have an account?
                    <a href="<?php echo base_url(); ?>login" class="to_register" style="color:#FFA365;font-weight:bold;margin:0px"> Login </a>
                </label>
            </div>
        </form>

		<div class="modal fade" id="phoneModal" tabindex="-1" role="dialog" aria-hidden="true">
			<div class="modal-dialog modal-dialog-centered" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h4>Signup using your name and phone number</h4>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<div class="modal-body">
						<form id="phoneloginform" method="POST">
							<div class="container-fluid" style="padding-left: 0px">
								<div class="row">
									<div class="col-md-9">
										<label>Name</label><br>
										<input type="text" name="fullname" class="form-control" value="">
									</div>
								</div>
								<br>
								<div class="row">
									<div class="col-md-4">
										<label>Code</label><br>
										<select class="form-control" name="countrycode">
											<?php
												foreach($country_codes as $country) {
													if(!empty($country['code']) && ($country['code'] == 'IN')) {
											?>
											<option value="<?php echo $country['dial_code'] ?>"
												<?php echo ($country['code'] == 'IN') ? 'selected' : ''; ?>>
												<?php echo $country['dial_code'] ?> </option>
											<?php
													}
												}
											?>
										</select>
									</div>
									<div class="col-md-8">
										<label>Number</label><br>
										<input type="text" name="phonenumber" class="form-control" value="" onkeyup="checkNumber();">
									</div>
								</div>
							</div>
						</form>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
						<button type="button" class="btn btn-primary" id="phonelogin">Verify</button>
						<label class="small text-secondary">
							By clicking verify, an SMS will be sent to the phone number. Message and data rates may apply.
						</label>
					</div>
				</div>
			</div>
		</div>

		<div class="modal fade" id="verifyModal" tabindex="-1" role="dialog" aria-hidden="true">
			<div class="modal-dialog modal-dialog-centered" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h4>Verify your phone number</h4>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<div class="modal-body">
						<form id="phoneverifyform" method="POST">
							<div class="container-fluid" style="padding-left: 0px">
								<div class="row">
									<div class="col-md-9">
										<label>Enter the 6 digit code we sent to <span id="verify-no" class="text-primary" style="color:#80C06F !important"></span></label>
									</div>
								</div>
								<br>
								<div class="row">
									<div class="col-md-6">
										<input type="text" name="otp" class="form-control" value="" placeholder="6 digit code">
									</div>
								</div>
							</div>
						</form>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
						<button type="button" class="btn btn-primary" id="phoneverify">Continue</button>
						<label class="small text-secondary">
							<!-- Resend Code. -->
						</label>
					</div>
				</div>
			</div>
        </div>
    </div>
</div>
<script>
function init() {
    gapi.load('auth2', function() {
        gAuth = gapi.auth2.init({
            'client_id': '879497195593-730scmceuq7ee0eb5m6hfrffh346cbhs.apps.googleusercontent.com',
            fetch_basic_profile: true,
            scope: 'profile'
        });
    });
}

function googleSignIn() {
    gAuth.signIn().then(function(googleUser) {
        signupWithGoogle(googleUser);
    }, function() {
        console.log('User denied access');
    });
}

function signupWithGoogle(googleUser) {
	var profile = googleUser.getBasicProfile();
	$('#signup input[name=signuptype]').val('google');
    $('#signup input[name=userid]').val(profile.getId());
    $('#signup input[name=fullname]').val(profile.getName());
    $('#signup input[name=email]').val(profile.getEmail());
    $('#signup').submit();
}

$('#twitter-button').on('click', function() {
    // Initialize with your OAuth.io app public key
    OAuth.initialize('YOUR_OAUTH_KEY');
    // Use popup for OAuth
    OAuth.popup('twitter').then(twitter => {
        console.log('twitter:', twitter);
        // Prompts 'welcome' message with User's email on successful login
        // #me() is a convenient method to retrieve user data without requiring you
        // to know which OAuth provider url to call
        twitter.me().then(data => {
            console.log('data:', data);
            alert('Twitter says your email is:' + data.email +
                ".\nView browser 'Console Log' for more details");
        });
        // Retrieves user data from OAuth provider by using #get() and
        // OAuth provider url    
        twitter.get('/1.1/account/verify_credentials.json?include_email=true').then(data => {
            console.log('self data:', data);
        })
    });
})
$('#phonelogin').on('click', function() {
    var fullname = $('#phoneloginform input[name="fullname"]').val(),
        code = $('#phoneloginform select[name="countrycode"]').val(),
        phone = $('#phoneloginform input[name="phonenumber"]').val();
    if (fullname != '' && code != '' && phone != '') {
        $.ajax({
            url: "/sms/send",
            type: "POST",
            data: {
                "name": fullname,
                "phone": phone,
                "code": code
            },
            dataType: "JSON",
            success: function(data) {
                if (data.status == 'success') {
					$('#phoneModal').modal('hide');
					$('#verify-no').html(code+phone);
					$('#verifyModal').modal('show');
                }
            },
            error: function(e) {}
        });
    }
});

$('#phoneverify').on('click', function() {
	var otp = $('#phoneverifyform input[name="otp"]').val(),
		fullname = $('#phoneloginform input[name="fullname"]').val(),
		code = $('#phoneloginform select[name="countrycode"]').val(),
        phone = $('#phoneloginform input[name="phonenumber"]').val();
    if (otp != '' && phone != '') {
        $.ajax({
            url: "/sms/verify",
            type: "POST",
            data: {
                "phone": phone,
                "code": otp
            },
            dataType: "JSON",
            success: function(data) {
                if (data.status == 'success') {
					$('#verifyModal').modal('hide');
					$('#signup input[name=fullname]').val(fullname);
					$('#signup input[name=phonecode]').val(code);
					$('#signup input[name=phonenumber]').val(phone);
					$('#signup input[name=signuptype]').val('phone');
					$('#signup').submit();
                }
            },
            error: function(e) {}
        });
    }
});

function checkNumber() {
	var phoneRegex = /^\d{10}$/,
		phone = $('#phoneloginform input[name="phonenumber"]');
  	if((phone.val().match(phoneRegex))){
		phone.removeClass('has-error');
      	return true;
	} else {
        phone.addClass('has-error');
        return false;
	}
}
</script>