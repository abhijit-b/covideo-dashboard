<?php

class Users_model extends CI_Model {
    public function __construct() {
        parent::__construct();
        $this->load->database();
    }

    public function getAllUsers() {
        $query = $this->db->get('users');
        return $query->result();
    }

    public function insert($user) {
        $this->db->insert('users', $user);
        return $this->db->insert_id();
    }

    public function getUser($id) {
        $this->db->from('users');
        $this->db->where(array('id' => $id));
        $result = $this->db->get();
        return $result->row();
    }

    public function getUserByCode($id) {
        $this->db->from('users');
        $this->db->where(array('userid' => $id));
        $result = $this->db->get();
        return $result->row();
    }

    public function checkUser($data) {
        $this->db->select('*');
        $this->db->from('users');
        $this->db->where($data);
        $result = $this->db->get();
        return $result->row();
    }

    public function getUserByEmail($email) {
        $this->db->from('users');
        $this->db->where(array('email' => $email));
        $result = $this->db->get();
        return $result->row();
    }

    public function updateUserInfo($data, $id) {
        $this->db->where('users.id', $id);
        return $this->db->update('users', $data);
    }

    public function updateForgotPasswordCode($data, $id) {
        $this->db->where('users.id', $id);
        return $this->db->update('users', $data);
    }

    public function checkToken($token){
        $this->db->from('users');
        $this->db->where(array('forgotten_password_code' => $token));
        $result = $this->db->get();
        return $result->row();

    }

    public function checkActivationCode($code) {
        $this->db->from('users');
        $this->db->where(array('activation_code' => $code));
        $result = $this->db->get();
        return ($result->row()) ? TRUE : FALSE;
    }

    public function deleteById($userid) {
        $this->db->where('id', $userid);
        $this->db->delete('users');
    }

    public function getAllFeedback() {
        $query = $this->db->get('feedback');
        return $query->result();
    }
    
    public function saveLoginActivity($activity) {
        $this->db->insert('login_activity', $activity);
        return $this->db->insert_id();
    }

    public function fetchLoginActivity($userid) {
        $this->db->where(array('userid' => $userid));
        $query = $this->db->get('login_activity');
        return $query->result();
    }

    public function saveDripSubscriber($details) {
        $this->db->insert('drip_api_responses', $details);
        return $this->db->insert_id();
    }

    public function getDripSubscriber($id) {
        $this->db->where(array('userid' => $id));
        $query = $this->db->get('drip_api_responses');
        return $query->row_array();
    }

}
