
<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
    &nbsp;
    <br>
    <h3><i class="fa fa-file-o"></i> SMS</h3>
</div>
<br>
<div class="alert alert-info alert-dismissible hidden">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
    <div id="status"></div>
</div>
<br>
<p>
    <div class="x_content">
        <br />
        <form name="smsForm" id="smsForm" class="form-horizontal form-label-left" action="/sms/send" method="post">
            <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="mobile-number">Mobile Number <span class="required">*</span>
                </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <input name="mobile-number" type="text" id="mobile-number" required="required" class="form-control col-md-7 col-xs-12" data-inputmask="'mask': '9999999999'">
                    <span class="fa fa-user form-control-feedback right" aria-hidden="true"></span>
                </div>
            </div>
            <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="message">Message <span class="required">*</span>
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
                <textarea id="message" name="message" required="required" class="form-control col-md-7 col-xs-12"></textarea>
            </div>
            </div>

            <div class="ln_solid"></div>
            <div class="form-group">
            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                <button class="btn btn-primary" type="submit" id="send-sms">Send</button>
            </div>
            </div>
        </form>
    </div>
</p>
<script src="<?php echo base_url(); ?>assets/js/sms.js"></script>

