<?php

class Sms_model extends CI_Model {
    
    public function __construct() {
        parent::__construct();
        $this->load->database();
    }

    public function storeLog($data) {
        $this->db->insert('sms_reports', $data);
        return $this->db->insert_id();
    }

    public function updateLog($data) {
        $this->db->where('message_id', $data['message_id']);
        $this->db->update('sms_reports', $data);
        return $this->db->insert_id();
    }

    public function updateSmsReports($data) {
        $this->db->update_batch('sms_reports', $data, 'message_id');
    }

    public function storeSmsCode($data) {
        $data['sent_on'] = time();
        $this->db->insert('sms_codes', $data);
        return $this->db->insert_id();
    }

    public function getSmsCode($phone_number, $code) {
        $this->db->from('sms_codes');
        $this->db->where(array('code' => $code, 'phone_number' => $phone_number, 'is_verified' => 0));
        $result = $this->db->get();
        return $result->row();
    }

    public function updateSmsCode($code) {
        $data = array('is_verified' => 1);
        $this->db->where('code', $code);
        return $this->db->update('sms_codes', $data);
    }
    
    public function disableSmsCode($data) {
        $this->db->where('phone_number', $data['phone_number']);
        return $this->db->update('sms_codes', array('is_verified' => 1));
    }

}