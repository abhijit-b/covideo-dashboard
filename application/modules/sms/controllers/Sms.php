<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sms extends MX_Controller {
    
    public $data;
    
    public function __construct() {
        parent::__construct();
        $this->load->library('mobinextapi');
        $this->load->library('csvreader');
        $this->load->model('sms_model');
    }

    public function index() {
        $this->template->set('title', 'SMS');
        $this->template->load('default_layout', 'contents' , 'sms/index', $this->data);
    }

    public function send() {
        if(!empty($this->input->post())) {
            // $this->load->library('mobinextapi');
            // $smsObj = new Mobinextapi();
            $this->load->library('routemobile');
            $smsObj = new Routemobile();
            $code = mt_rand(100000, 999999);
            $message = "{$code} is your verification code for CoVideo.in.";
            $phone_number = $this->input->post('phone');
            $response = $smsObj->sendSms($phone_number, $message);

            $data = array('phone_number' => $phone_number, 'code' => $code);
            $this->sms_model->disableSmsCode($data);
            $this->sms_model->storeSmsCode($data);
            foreach($response as $key => $message) {
                if($message['status'] == 'success') {
                    // $response_data['message_id'] = $message['message_id'];
                    $response_data['message_id'] = explode(':', $message['message_id'])[1];
                    $response_data['message'] = $message['text'];
                    $response_data['status'] = 'Sent';
                } else {
                    $response_data['message_id'] = $message['code'];
                    $response_data['message'] = $message['message'];
                    $response_data['status'] = 'Error';
                }
                // $response_data['mobile_number'] = explode('-', $message['message_id'])[0];
                $response_data['mobile_number'] = explode(':', $message['message_id'])[0];
                $this->sms_model->storeLog($response_data);
            }

            echo json_encode(array('status' => 'success', 'message' => $message));
        } else {
            echo json_encode(array('status' => 'error', 'message' => 'Can talk using POST only.'));
        }
    }
    
    public function verify() {
        if(!empty($this->input->post())) {
            $phone_number = $this->input->post('phone');
            $code = $this->input->post('code');
            $status = $this->sms_model->getSmsCode($phone_number, $code);
            if(!empty($status)) {
                $this->sms_model->updateSmsCode($code);
                echo json_encode(array('status' => 'success'));
            } else {
                echo json_encode(array('status' => 'failure', 'message' => 'Could not verify code. Please try again'));
            }
        } else {
            echo json_encode(array('status' => 'error', 'message' => 'Can talk using POST only.'));
        }
    }

    public function update_status() {
        if(!empty($this->input->post())) {
            if(empty($_FILES["csv_file"]['name'])) {
                $this->session->set_flashdata("error", "No file selected.");
                redirect('/sms/update_status', "refresh");
            }
            $file_data = $this->csvreader->parse_file($_FILES["csv_file"]["tmp_name"]);
            foreach($file_data as $row) {
                if (strpos($row['donedate'], '/')) {
                    $datearr = explode(" ", $row["donedate"]);
                    $date = implode("-", array_reverse(explode("/", $datearr[0]))) . ' ' . $datearr[1];
                    $newdate = date("Y-m-d H:i:s", strtotime($date));
                } else {
                    $newdate = date("Y-m-d H:i:s", strtotime($row["donedate"]));
                }
                $data[] = array(
                    'message_id' => $row["messageid"],
                    'updated' => $newdate,
                    'status' => ($row["status"] == 'DELIVRD') ? 'DELIVERED' : $row["status"],
                );
            }
            $this->sms_model->updateSmsReports($data);
            $this->session->set_flashdata("success", "Data updated successfully.");
        } else {
            $this->session->set_flashdata("info", "Be very careful while using this upload form as it will make changes to the live SMS reports.");
        }
        $this->template->set('title', 'SMS Management');
        $this->template->load('default_layout', 'contents' , 'sms/update_status', $this->data);
    }

    public function dlr() {
        $data = array();
    
        if(!empty($this->input->get('data'))) {
            $param = str_replace('?data=', '', $this->input->get('data'));
            $xml_data = simplexml_load_string($param);
        }

        foreach($xml_data as $dlr) {
            $data['message_id'] = (string)$dlr->MessageId;
            unset($dlr->MessageId);
            $data['status'] = (string)$dlr->MessageStatus;
            unset($dlr->MessageStatus);
            $data['errorcode'] = (string)$dlr->ErrorCode;
            unset($dlr->ErrorCode);
            if(!empty($dlr)) {
                $data['vars'] = json_encode($dlr);
			}
            $this->admin_model->updateSmsLog($data);
		}
		
        echo json_encode(array('status' => 'success', 'message' => 'SMS status stored successfully.'));
    }

    public function dlr_value_first() {
        $data = array();
    
        $params = $this->input->get();
        if(!empty(implode('', $params))) {
            $data['message_id'] = (!empty($params['MessageId'])) ? $params['MessageId'] : '';
            unset($params['MessageId']);
            $data['status'] = (!empty($params['MessageStatus'])) ? $params['MessageStatus'] : '';
            unset($params['MessageStatus']);
            $data['code'] = (!empty($params['ErrorCode'])) ? $params['ErrorCode'] : '';
            unset($params['ErrorCode']);
            if(!empty($params)) {
                $data['vars'] = json_encode($params);
			}
            $data['updated'] = time();
            $this->sms_model->updateSmsLog($data);
            echo json_encode(array('status' => 'success', 'message' => 'SMS status stored successfully.'));
		} else {
		    echo json_encode(array('status' => 'error', 'message' => 'no data to update'));
        }
    }

}