<?php

class Thankyou_model extends CI_Model {
    public function __construct() {
        parent::__construct();
        $this->load->database();
    }

    public function saveFeedback($data) {
        $this->db->insert('feedback', $data);
        return $this->db->insert_id();
    }
}
