<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Thankyou extends CI_Controller {

    public $data;
    
    public function __construct() {
        parent::__construct();
        $this->load->model('thankyou_model');
        $this->data['username'] = $this->session->userdata('username');
    }

	public function index()	{
        if(!empty($code = $this->uri->segment(2))) {
            // check encoded data
            $data = $this->base64url_decode($code);
            $params = explode('&', $data);
            $this->data['params'] = $params;
        }
        
        if(!empty($this->input->post())){
            if(!empty($this->input->post('params'))) {
                $params = implode('&', $this->input->post('params'));
                parse_str($params, $params);
            }
            $params['ip'] = $this->input->server('REMOTE_ADDR');
            $params['user_agent'] = $this->input->server('HTTP_USER_AGENT');
            
            $data = array();
            $data['joiningExp'] = $this->input->post('joiningExp');
            $data['audioQuality'] = $this->input->post('audioQuality');
            $data['videoQuality'] = $this->input->post('videoQuality');
            $data['overallExp'] = $this->input->post('overallExp');
            $data['feedback'] = $this->input->post('feedback');
            $data['created_at'] = time();
            $data['username'] = $this->data['username'];
            $data['params'] = (!empty($params)) ? json_encode($params) : '';

            $id = $this->thankyou_model->saveFeedback($data);
            if(!empty($id)) {
                $message = "\n";
                $message .= "Joining Experience = {$this->input->post('joiningExp')}"."\n";
                $message .= "Audio Quality = {$this->input->post('audioQuality')}"."\n";
                $message .= "Video Quality = {$this->input->post('videoQuality')}"."\n";
                $message .= "Overall Experience = {$this->input->post('overallExp')}"."\n";
                $message .= "Overall Feedback = {$this->input->post('feedback')}"."\n";
                $message .= "Room Details = "."\n";
                foreach($this->input->post('params') as $param){
                    $message .= implode(' -> ', explode('=', $param)) . "\n";
                }
                $message .= 'Remote IP : ' . $this->input->server('REMOTE_ADDR') . "\n";
                $message .= 'User Agent : ' . $this->input->server('HTTP_USER_AGENT') . "\n";

                // send email
                $headers[] = 'From: CoVideo India • <noreply@covideo.in>';
                $headers[] = 'Reply-To: noreply@covideo.in';
                mail('connect@covideo.in,mitul@enterux.in,abhijit@enterux.in,chirag@enterux.in', 'CoVideo.in Feedback', $message, implode("\r\n", $headers));
                echo json_encode(array("status" => "success", "message" =>  "Thank you for providing your valuable feedback."));
                exit;
            } else {
                echo json_encode(array("status" => "error", "message" =>  "Thank you for providing your valuable feedback."));
                exit;
            }
        }

        $this->data['user_logged_in'] = !empty($this->session->userdata('user_logged')) ? $this->session->userdata('user_logged') : FALSE;

        $this->template->set('title', 'Thank you');
        $this->template->load('layout', 'contents' , 'index', $this->data);
    }
    
    public function base64url_encode($data) { 
        return rtrim(strtr(base64_encode($data), '+/', '-_'), '='); 
    }

    public function base64url_decode($data) { 
        return base64_decode(str_pad(strtr($data, '-_', '+/'), strlen($data) % 4, '=', STR_PAD_RIGHT)); 
    } 
}
