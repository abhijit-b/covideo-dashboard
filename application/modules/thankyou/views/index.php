<br><br>
<?php if(isset($_SESSION['error'])) { ?>
    <div class="alert alert-danger"><?php echo $this->session->flashdata('error'); ?></div>
<?php } ?>
<?php if(isset($_SESSION['success'])) { ?>
    <div class="alert alert-success"><?php echo $this->session->flashdata('success'); ?></div>
<?php } ?>
<form role="form" method="POST" id="feedbackForm">
  <?php if(!empty($params)) {
  foreach($params as $index => $param) { ?>
    <input type="hidden" name="params[]" value="<?php echo $param;?>">
  <?php }
  }?>
  <div class="card-body">
    <div class="form-group">
      <h4>Thank you for using CoVideo.in! Please take a moment to fill out this feedback form, it will help us deliver an amazing experience everytime.</h4>
      <fieldset class="rating">
        <legend>Joining Experience<span class="text-red">*</span></legend>
        <input type="radio" id="jx_star5" name="joiningExp" value="5" /><label for="jx_star5" title="Very Satisfied!">5 stars</label>
        <input type="radio" id="jx_star4" name="joiningExp" value="4" /><label for="jx_star4" title="Satisfied">4 stars</label>
        <input type="radio" id="jx_star3" name="joiningExp" value="3" /><label for="jx_star3" title="Neutral">3 stars</label>
        <input type="radio" id="jx_star2" name="joiningExp" value="2" /><label for="jx_star2" title="Unsatisfied">2 stars</label>
        <input type="radio" id="jx_star1" name="joiningExp" value="1" /><label for="jx_star1" title="Very Unsatisfied">1 star</label>
      </fieldset>
    </div>
    <div class="form-group">
      <fieldset class="rating">
        <legend>Audio Quality<span class="text-red">*</span></legend>
        <input type="radio" id="aq_star5" name="audioQuality" value="5" /><label for="aq_star5" title="Very Satisfied!">5 stars</label>
        <input type="radio" id="aq_star4" name="audioQuality" value="4" /><label for="aq_star4" title="Satisfied">4 stars</label>
        <input type="radio" id="aq_star3" name="audioQuality" value="3" /><label for="aq_star3" title="Neutral">3 stars</label>
        <input type="radio" id="aq_star2" name="audioQuality" value="2" /><label for="aq_star2" title="Unsatisfied">2 stars</label>
        <input type="radio" id="aq_star1" name="audioQuality" value="1" /><label for="aq_star1" title="Very Unsatisfied">1 star</label>
      </fieldset>
    </div>
    <div class="form-group">
      <fieldset class="rating">
        <legend>Video Quality<span class="text-red">*</span></legend>
        <input type="radio" id="vq_star5" name="videoQuality" value="5" /><label for="vq_star5" title="Very Satisfied!">5 stars</label>
        <input type="radio" id="vq_star4" name="videoQuality" value="4" /><label for="vq_star4" title="Satisfied">4 stars</label>
        <input type="radio" id="vq_star3" name="videoQuality" value="3" /><label for="vq_star3" title="Neutral">3 stars</label>
        <input type="radio" id="vq_star2" name="videoQuality" value="2" /><label for="vq_star2" title="Unsatisfied">2 stars</label>
        <input type="radio" id="vq_star1" name="videoQuality" value="1" /><label for="vq_star1" title="Very Unsatisfied">1 star</label>
      </fieldset>
    </div>
    <div class="form-group">
      <fieldset class="rating">
        <legend>Overall Experience<span class="text-red">*</span></legend>
        <input type="radio" id="ox_star5" name="overallExp" value="5" /><label for="ox_star5" title="Very Satisfied!">5 stars</label>
        <input type="radio" id="ox_star4" name="overallExp" value="4" /><label for="ox_star4" title="Satisfied">4 stars</label>
        <input type="radio" id="ox_star3" name="overallExp" value="3" /><label for="ox_star3" title="Neutral">3 stars</label>
        <input type="radio" id="ox_star2" name="overallExp" value="2" /><label for="ox_star2" title="Unsatisfied">2 stars</label>
        <input type="radio" id="ox_star1" name="overallExp" value="1" /><label for="ox_star1" title="Very Unsatisfied">1 star</label>
      </fieldset>
    </div>
    <br><br><br><br><br><br>
    <div class="form-group">
      <label> <h5>How can we improve our service? </h5></label>
      <div>
        <textarea class="form-control" name="feedback" cols="40" rows="4"></textarea>
      </div>
    </div>
    <div class="form-group" id="form-error" style="display:none;"><p class="text-red">Please provide feedback.</p></div>
    <div class="form-group">
      <button type="button" class="btn btn-primary" id="feedback">Submit Feedback</button>
      <span>
      <?php if(!empty($username)) { ?>
        <a href="/dashboard" style="text-decoration: underline;color:#80C06F;font-weight:bold;margin-left:10px">Go to Dashboard</a>
      <?php } else { ?>
        <a href="/access" style="text-decoration: underline;color:#80C06F;font-weight:bold;margin-left:10px">Access CoVideo.in</a>
      <?php } ?>
      </span>
    </div>
  </div>
</form>
<script>
  $('#feedback').on('click', function() {
    $('#form-error').hide();
    var joiningExp = $('input[name=joiningExp]:checked').val(),
    audioQuality = $('input[name=audioQuality]:checked').val(),
    videoQuality = $('input[name=videoQuality]:checked').val(),
    overallExp = $('input[name=overallExp]:checked').val();
    if(typeof joiningExp == "undefined" || typeof audioQuality == "undefined" || typeof videoQuality == "undefined"
      || typeof overallExp == "undefined") {
        $('#form-error').show();
        return;
    }
    $(this).html('Submit Feedback <i class="fas fa-sync fa-spin"></i>');
    $.ajax({
      url: "/thank-you",
      type: "POST",
      dataType: "JSON",
      data: $('#feedbackForm').serialize(),
      success: function(data) {
        <?php if($user_logged_in) { ?>
          location.href = '/dashboard';
        <?php } else { ?>
          location.href = '/access';
        <?php } ?>
      },
      error: function(e) {}
    });
  });
</script>