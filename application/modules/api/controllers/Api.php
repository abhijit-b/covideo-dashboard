<?php
require APPPATH.'/libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';
use Restserver\Libraries\REST_Controller;

class Api extends REST_Controller{
    
    public function __construct() {
        parent::__construct();
    }

    function user_get() {
        if(!$this->get('id')) {
            $this->response('User Id not provided', 400);
        }
        $this->load->model('user/users_model');
        $user = $this->users_model->getUserByCode($this->get('id'));
        
        // remove sensitive data from response
        unset($user->password);
        unset($user->id);
        unset($user->ip_address);
        unset($user->salt);
        unset($user->type);
        unset($user->entity_id);
        unset($user->remember_code);
        unset($user->forgotten_password_code);
        unset($user->forgotten_password_time);
        unset($user->activation_code);

        if($user) {
            $this->response($user, 200);
        } else {
            $this->response('User not found', 404);
        }
    }
     
    function users_get() {
        $this->load->model('user/users_model');
        $users = $this->users_model->getAllUsers();
        foreach($users as $index => $user) {
            if($user->id == 1) {
                unset($users[$index]);
                continue;
            }
            unset($user->password);
            unset($user->id);
            unset($user->ip_address);
            unset($user->salt);
            unset($user->type);
            unset($user->entity_id);
            unset($user->remember_code);
            unset($user->forgotten_password_code);
            unset($user->forgotten_password_time);
            unset($user->activation_code);
        }
        if($users) {
            $this->response($users, 200);
        } else{
            $this->response(NULL, 404);
        }
    }

    function feedback_get() {
        $this->load->model('user/users_model');
        $feedback = $this->users_model->getAllFeedback();
        if($feedback) {
            $this->response($feedback, 200);
        } else{
            $this->response(NULL, 404);
        }
    }

}
