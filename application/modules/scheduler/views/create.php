<br><br>
<div class="container">
    <div class="card">
        <div class="card-header">
            <div class="page-header m-0 ">
                <h1 class="page-title">
                    Schedule a New Conference </h1>
            </div>
        </div>
        <div class="card-body">
            <div class="row row-cards row-deck">

                <div class="col-12">
                    <form method="post" id="meeting_form">
                        <h3 class="card-title">Conference Details</h3>
                        <br><br>
                        <div class="row">
                            <div class="col-md-12 col-lg-12">
                                <div class="form-group">
                                    <label class="form-label">Title<span class="form-required text-red">*</span></label>
                                    <input type="text" class="form-control" value="" name="meeting_title" id="meeting_title" placeholder="Title" required="">
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-2 col-lg-2">
                                <div class="form-group">
                                    <label class="form-label">Start Date<span class="datetime form-required text-red">*</span> </label>
                                    <input name="meeting_start_date" type="text" class="form-control" data-inputmask-alias="datetime" data-inputmask-inputformat="dd-mm-yyyy" data-mask id="datemask" placeholder="dd-mm-yyyy" required=""  value="<?php echo date('d-m-Y', time()); ?>">
                                </div>
                            </div>
                            <div class="col-md-2 col-lg-2">
                                <div class="form-group datetimepicker">
                                    <label class="form-label">Start Time<span class="datetime form-required text-red">*</span> </label>
                                    <input type="text" name="meeting_start_time" placeholder="HH:MM" id="timemask" class="form-control" required=""  value="<?php echo date('H:i', time()); ?>">
                                </div>
                            </div>

                            <div class="col-md-4 col-lg-4">
                                <div class="form-group">
                                    <label class="form-label">Duration<span class="form-required text-red">*</span> </label>
                                    <select name="meeting_duration" id="meeting_duration" class="form-control">
                                        <option value="5">5 minutes</option>
                                        <option value="15" selected="">15 minutes</option>
                                        <option value="30">30 minutes</option>
                                        <option value="45">45 minutes</option>
                                    </select>
                                </div>
                            </div>  

                            <div class="col-md-4 col-lg-4">
                                <div class="form-group">
                                    <label class="form-label">Number of Participants<span class="form-required text-red">*</span> </label>
                                    <select name="number_of_participants" id="number_of_participants" class="form-control">
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                        <option value="5">5</option>
                                        <option value="6">6</option>
                                        <option value="7">7</option>
                                        <option value="8">8</option>
                                    </select>
                                </div>
                            </div>

                        </div>
                        <div class="row">
                            <div class="col-md-12 col-lg-12">
                                <div class="form-group">
                                    <label class="form-label">Description(Optional)</label>
                                    <textarea name="meeting_description" id="meeting_description" class="form-control" rows="5"></textarea>
                                </div>
                            </div>
                        </div>
                        <!-- <div class="row">
                            <div class="col-md-8 col-lg-8">
                                <div class="form-group">
                                    <div class="form-label">Public</div>
                                    <label class="custom-switch">
                                        <input type="checkbox" name="meeting_public" id="meeting_public" value="no" class="custom-switch-input">
                                        <span class="custom-switch-indicator"></span>
                                    </label>
                                </div>
                                <div id="user_email">
                                    <div class="form-label">Enter guest email id (you can enter multiple email ids with comma separated)</div>
                                    <textarea placeholder="email1@example.com,email2@example.com" name="invited_user" id="invited_user" class="form-control" rows="5"></textarea>
                                    <div class="form-label">Enter guest mobile number (you can enter multiple number with comma separated)</div>
                                    <textarea placeholder="1234567890,0123456789" name="invited_user_mobile" id="invited_user_mobile" class="form-control" rows="5"></textarea>
                                </div>
                            </div>
                            <div class="col-md-3 col-lg-3 offset-lg-1 offset-md-1">
                                <div class="form-group">
                                    <div class="form-label">Notify to guest(s)?</div>
                                    <label class="custom-switch">
                                        <input type="checkbox" checked="" name="notify_to_guest" id="notify_to_guest" value="yes" class="custom-switch-input">
                                        <span class="custom-switch-indicator"></span>
                                    </label>
                                </div>
                            </div>
                        </div> -->
                        <hr>
                        <div class="text-right">
                            <div class="d-flex">
                                <a href="/scheduler" class="btn btn-link">Cancel</a>
                                <button type="submit" name="save_meeting" value="save_meeting" id="save_meeting" class="btn btn-primary ml-auto">Save</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="<?php echo base_url(); ?>assets/js/jquery.inputmask.min.js"></script>
<script>
    //Datemask dd/mm/yyyy
    $('#datemask').inputmask('dd-mm-yyyy', { 'placeholder': 'dd-mm-yyyy' });
    $('#timemask').inputmask('99:99', { 'placeholder': 'HH:MM' });
</script>