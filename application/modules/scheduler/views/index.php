<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/fullcalendar.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/fullcalendar.daygrid.css">
<script src="<?php echo base_url(); ?>assets/js/fullcalendar.js"></script>
<script src="<?php echo base_url(); ?>assets/js/fullcalendar.daygrid.js"></script>
<script src="<?php echo base_url(); ?>assets/js/fullcalendar.interaction.js"></script>

<div class="pt-3 pb-2 mb-3 border-bottom">
  <h3><i class="fa fa-user"></i> Scheduler</h3>
</div>
<?php if (isset($_SESSION['success'])) { ?>
  <div class="alert alert-success"><?php echo $_SESSION['success']; ?></div>
<?php } ?>

<br>
<div class="container-fluid">
	<div class="row">
		<div class="col-md-12">
			<a class="btn btn-primary" href="/scheduler/create"><i class="fa fa-calendar"></i> Schedule a room</a>
		</div>
	</div>
	<br>
	<div class="row">
		<div class="col-md-7">
			<table id="rooms-list" class="table table-striped table-bordered dt-responsive dataTable no-footer dtr-inline">
				<thead>
					<th>#</th>
					<th>Name</th>
					<th>Date and Time</th>
					<th>Duration</th>
					<th>Description</th>
					<th>&nbsp;</th>
				</thead>
				<tbody>
					<?php foreach($scheduled_rooms as $index => $room) { ?>
						<td><?php echo $index + 1; ?></td>
						<td><?php echo $room->meeting_title; ?></td>
						<td><?php echo $room->meeting_start_date . ' ' . $room->meeting_start_time ?></td>
						<td><?php echo $room->meeting_duration; ?> minutes</td>
						<td><?php echo $room->meeting_description; ?></td>
						<td><a href="/<?php echo $room->roomid; ?>" class="btn btn-primary" style="margin: 5px;"> <?php echo $this->lang->line('room_link'); ?> </a>&nbsp;</td>
					<?php } ?>
				</tbody>
			</table>
		</div>
		<div class="col-md-5" style="background-color: #ffffff !important;">
			<div id="calendar">
			</div>
		</div>

	</div>
	<br>
	<div class="row">
	</div>
</div>
<br><br>
<script type="text/javascript">
$(document).ready(function() {
    var calendarEl = document.getElementById('calendar');
    var calendar = new FullCalendar.Calendar(calendarEl, {
	  plugins: [ 'interaction', 'dayGrid' ],
	  header: {
        left: 'prev,next today',
        center: 'title',
        right: 'dayGridMonth'
      },
	  navLinks: true, // can click day/week names to navigate views
      selectable: true,
      editable: true,
      eventLimit: true, // allow "more" link when too many events
	  events: <?php echo json_encode($calendar_events); ?>
    });
	calendar.render();
	
	$('#rooms-list').DataTable({
		ordering: false
	});
});
</script>
