<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Scheduler extends CI_Controller {

    public $data;
    
    public function __construct() {
        parent::__construct();
        if(!isset($_SESSION['user_logged'])) {
            $this->session->set_flashdata("error", "Please login first to view this page");
            redirect("login");
        }
        $this->load->model('scheduler_model');
        $this->userid = $this->session->userdata('userid');
        $this->username = $this->session->userdata('username');
        $this->email = $this->session->userdata('useremail');
        $this->fullname = $this->session->userdata('fullname');
        $this->lang->load("default_layout", $this->session->userdata('site_lang'));
        $this->lang->load("dashboard", $this->session->userdata('site_lang'));
    }

	public function index()	{
        $this->data['scheduled_rooms'] = $this->scheduler_model->getAll($this->userid);

        $calendar_events = array();
        foreach($this->data['scheduled_rooms'] as $index => $room) {
            $date = date_create($room->meeting_start_date);
            $details = array();
            $details['title'] = $room->meeting_title;
            $details['start'] = date_format($date,"Y-m-d") . 'T' . $room->meeting_start_time.':00';;
            $calendar_events[] = $details;
        }

        $this->data['calendar_events'] = $calendar_events; 
        $this->template->set('title', 'Scheduler');
        $this->template->load('default_layout', 'contents' , 'index', $this->data);
    }

	public function create() {
        if(!empty($this->input->post())) {
            // create room for user
            $numbers = '123456789';
            if(strlen($this->fullname) == 0) {
                $name = substr(str_shuffle($numbers), 0, 15);
            } else if(strlen($this->fullname) < 15) {
                $name = str_replace(' ', '', $this->fullname);
            } else {
                $name = explode(" ", $this->fullname)[0];
            }
            $roomname = $name.substr(str_shuffle($numbers), 0, 15);
            $roomid = strtolower(substr($roomname, 0, 15));

            // api call to generate room
            $this->load->library('covideoapi');
            $covideo_api = new CovideoAPI();
            $response = $covideo_api->createRoom(!empty($this->fullname) ? $this->fullname : $name, $this->username, $roomid);

            // save room schedule details
            $data = array();
            $data['userid'] = $this->userid;
            $data['roomid'] = $roomid;
            $data['meeting_title'] = $this->input->post('meeting_title');
            $data['meeting_start_date'] = $this->input->post('meeting_start_date');
            $data['meeting_start_time'] = $this->input->post('meeting_start_time');
            $data['meeting_duration'] = $this->input->post('meeting_duration');
            $data['number_of_participants'] = $this->input->post('number_of_participants');
            $data['meeting_description'] = $this->input->post('meeting_description');
            $data['created_at'] = time();

            $this->scheduler_model->insert($data);
            $this->session->set_flashdata('success', 'Meeting scheduled Successfully');
            redirect('scheduler', 'refresh');

        }
        $this->template->set('title', 'Scheduler');
        $this->template->load('default_layout', 'contents' , 'create', $this->data);
    }

}
