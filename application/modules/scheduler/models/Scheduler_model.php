<?php

class Scheduler_model extends CI_Model {
    public function __construct() {
        parent::__construct();
        $this->load->database();
    }

    public function getAll($userid) {
        $this->db->where('userid', $userid);
        return $this->db->get('room_scheduler')->result();
    }

    public function insert($data) {
        $this->db->insert('room_scheduler', $data);
        return $this->db->insert_id();
    }
}
