<?php 

if (!defined('BASEPATH')) exit('No direct script access allowed');

class Covideoadminapi {

    public function __construct() {
		$this->url = 'https://admin.covideo.in/api/';
        $this->api_key = 'PBS0NIQ9VCsOinxg6KM7hzfabcw13WUZH2t4kDFl';
    }
    		
    public function fetchUsersByInviteCode($code) {
        $curl = curl_init();

        // OPTIONS:
		curl_setopt($curl, CURLOPT_URL, $this->url.'user/code/'.$code);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array(  
			"api-key: {$this->api_key}"
		));	
		curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);

        // EXECUTE:
        $result = curl_exec($curl);
        return $result;
    }
}