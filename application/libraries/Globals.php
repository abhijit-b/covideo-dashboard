<?php
/*
* This is a class to dynamically load variables from database and create constants
*/

class Globals {

    public function __construct() {
        $db =& DB();
        $query = $db->get('master_config');
        $constants = $query->result();
        foreach($constants as $constant) {
            define(strtoupper($constant->config_key), htmlentities($constant->config_val));
        }
    }

}   