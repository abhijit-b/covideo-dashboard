<?php 
if (!defined('BASEPATH')) exit('No direct script access allowed');

class CovideoAPI{

    public function __construct() {
		$this->url = COVIDEO_API_URL;
		$this->access_token = COVIDEO_API_TOKEN;
    }
    		
  	public function fetchRooms( $username = "") {
        // rooms specific api url
        // curl -X GET --header 'Accept: application/json' 'https://api.covideo.in/api/rooms?filter=%7B%22where%22%3A%7B%22username%22%3A%2210157954797950950%22%7D%7D&access_token=xYFfemyxhaTI0MYvykYPbJ1s4kSU4ZT3d7cZf3WnyC3WvFyYumdkim6Bs8GDpVKW'

        $url = $this->url.'rooms?';
        if(!empty($username)){
            $filter = "{\"where\":{\"username\":\"{$username}\"}}";
            $url .= "filter=".rawurlencode($filter);
        }
    	$curl = curl_init();

		// OPTIONS:
		curl_setopt($curl, CURLOPT_URL, $url . "&access_token=" . $this->access_token);
		curl_setopt($curl, CURLOPT_HTTPHEADER, array(  
			'Content-Type: application/json'
		));	
    	curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
		//setting post variables

        // EXECUTE:
        $result = curl_exec($curl);
        $result = json_decode($result, true);

		if(isset($result['error'])) {
			$result = array('status' => 'error', 'code' => $result['error']['code'], 'message' => $result['error']['message']);
		}

    	curl_close($curl);
    	return $result;
    }

  	public function find( $roomid ) {
        // rooms specific api url
        // curl -X GET --header 'Accept: application/json' 'https://api.covideo.in/api/rooms?filter=%7B%22where%22%3A%7B%22username%22%3A%2210157954797950950%22%7D%7D&access_token=xYFfemyxhaTI0MYvykYPbJ1s4kSU4ZT3d7cZf3WnyC3WvFyYumdkim6Bs8GDpVKW'

        $url = $this->url.'rooms/findOne?';
        if(!empty($roomid)){
            $filter = "{\"where\":{\"code\":\"{$roomid}\"}}";
            $url .= "filter=".rawurlencode($filter);
        }
    	$curl = curl_init();

		// OPTIONS:
		curl_setopt($curl, CURLOPT_URL, $url . "&access_token=" . $this->access_token);
		curl_setopt($curl, CURLOPT_HTTPHEADER, array(  
			'Content-Type: application/json'
		));	
    	curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
		//setting post variables

        // EXECUTE:
        $result = curl_exec($curl);
        $result = json_decode($result, true);

		if(isset($result['error'])) {
			$result = array('status' => 'error', 'code' => $result['error']['code'], 'message' => $result['error']['message']);
		}

    	curl_close($curl);
    	return $result;
    }
      
    public function createRoom($name, $userid, $roomid, $desc = ""){
        $params = array(
            "code" =>  strtolower($roomid),
            "name"=>  !empty($desc) ? $desc : 'My meeting room',
            "maxUsersPerRoom"=>  MAX_USERS_PER_ROOM,
            "username" => $userid,
            "displayName" => $name,
            "endurl" => "https://at.covideo.in/thank-you",
            "domain" => "covideo.in"
        );

        $url = $this->url.'rooms?access_token='.$this->access_token;
        $curl = curl_init();

        // OPTIONS:
        curl_setopt($curl, CURLOPT_POST, 1);
        //setting post variables
        curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($params));

        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array(  
            'Content-Type: application/json'
        ));	
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        
        // EXECUTE:
        $result = curl_exec($curl);
        if(isset($result['error'])) {
			$result = array('status' => 'error', 'code' => $result['error']['code'], 'message' => $result['error']['message']);
		}
    	curl_close($curl);
    	return $result;
    }

}