<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Elasticemail { 
    public function __construct() {
        $this->api_key = ELASTICEMAIL_API_KEY;
        $this->public_account_id = ELASTICEMAIL_PUBLIC_ACCOUNT_ID;
        $this->email_url = ELASTICEMAIL_API_URL;
    }

    public function sendMail($message, $email_addresses, $campaign_details) {
        //build the array for sending email
        $params = array();
        $params['apikey'] = $this->api_key;
        $params['to'] = $email_addresses['to'];
        $params['from'] = $email_addresses['from'];
        $params['fromName'] = 'KYC.BOT';
        $params['replyTo'] = $email_addresses['reply'];
        $params['replyToName'] = 'KYC.BOT';
        $params['trackClicks'] = 'TRUE';
        $params['trackOpens'] = 'TRUE';
        $params['subject'] = $campaign_details->email_subject;
        $params['bodyText'] = $campaign_details->email_template;
        
        $curl = curl_init();

        // OPTIONS:
        curl_setopt($curl, CURLOPT_URL, $this->email_url.'email/send');
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($params));


        // EXECUTE:
        $result = curl_exec($curl);
        $result = json_decode($result);

        if (!$result->success) {
            return array(
                'error' => 'Could not complete the API call', 
                'description' => $result->error
            );
        }
        curl_close($curl);
        return array(
            'success' => $result
        );
    }

    public function registerEmail($email_address) {
        //build the array for registering the email
        $data = array();
        $data['publicAccountID'] = $this->public_account_id;
        $data['email'] = $email_address;

        $curl = curl_init();
        // OPTIONS:
        curl_setopt($curl, CURLOPT_URL,  $this->email_url.'contact/add');
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($data));
        

        // EXECUTE:
        $result = curl_exec($curl);
        $result = json_decode($result);

        if (!$result->success) {
            return array(
                'error' => 'Could not complete the API call', 
                'description' => $result->error
            );
        }
        curl_close($curl);
        return array(
            'success' => $result
        );    
    }

    public function status() {

    }
}