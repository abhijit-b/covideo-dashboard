<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Routemobile {
    public function __construct() {
        $this->url = ROUTEMOBILE_URL;
        $this->username = ROUTEMOBILE_USERNAME;
        $this->password = ROUTEMOBILE_PASSWORD;
        $this->senderId = ROUTEMOBILE_SENDER_ID;
        $this->messageType = 0; // 0 = text message, 2 = Unicode message, 5 = Plain Text (ISO-8859-1 Character encoding) 
        $this->dlr = 1; // 0 = no DLR, 1 = DLR
    }

    public function sendSms($number, $message) {
        // http://sms6.rmlconnect.net:8080/bulksms/bulksms?username=enteruxotp1&password=PktYqQ4R&type=0&dlr=1&destination=919819209855&source=COVDEO&message=Your%20OTP%20for%20CoVideo.in%20is%20123456

        $data = array(
            'username' => $this->username,
            'password' => $this->password,
            'type' => $this->messageType,
            'dlr' => $this->dlr,
            'destination' => $number,
            'source' => $this->senderId,
            'message' => $message
        );

        $url = sprintf("%s?%s", $this->url, http_build_query($data));

    	$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, $url);
    	curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);

        // EXECUTE:
        $result = curl_exec($curl);
        $result = explode("|", $result);

        $response = [];
        if($result[0] == 1701) {
            $response['status'] = 'success';
            $response['message_id'] = $result[1];
            $response['text'] = $message;
        } else {
            $response['status'] = 'error';
            $response['code'] = $this->messageFromCode($result[0]);
            $response['message'] = $message;
        }
        return array($response);
    }

    public function messageFromCode($code) {
        $errorCodes = array(
            "1701" => "Success",
            "1702" => "Invalid URL",
            "1703" => "Invalid value in username or password parameter.",
            "1704" => "Invalid value in type parameter.",
            "1705" => "Invalid message.",
            "1706" => "Invalid destination.",
            "1707" => "Invalid source (Sender).",
            "1708" => "Invalid value for dlr parameter.",
            "1709" => "User validation failed",
            "1710" => "Internal error.",
            "1025" => "Insufficient credit.",
            "1715" => "Response timeout."
        );
        return $errorCodes[$code];
    }
}
