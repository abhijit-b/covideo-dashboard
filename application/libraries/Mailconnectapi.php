<?php 

if (!defined('BASEPATH')) exit('No direct script access allowed');


class Mailconnectapi {

    public function __construct() {
        $this->api_key = MAILCONNECT_API_KEY;
        $this->email_url = MAILCONNECT_API_URL;
    }
    		
    public function sendMail($message, $email_addresses, $campaign_details, $method = 'POST') {
        //build the array for sending email
        $params = array();
        $params['api_key'] = $this->api_key;
        $params['to_address'] = $email_addresses['to'];
        $params['campaign_name'] = $campaign_details->name;
        $params['campaign_desc'] = $campaign_details->description;
        $params['from_email'] = $email_addresses['from'];
        $params['reply_to_email'] = $email_addresses['reply'];
        $params['message_template'] = $campaign_details->sms_template;
        $api_xml = $this->getXMLForCall($params);
        $curl = curl_init();

        switch ($method) {
            case "POST":
                curl_setopt($curl, CURLOPT_POST, 1);
                if ($api_xml)
                    curl_setopt($curl, CURLOPT_POSTFIELDS, $api_xml);
                break;
            default:
                return 'not doing anything';
        }

        // OPTIONS:
        curl_setopt($curl, CURLOPT_URL, $this->email_url.'Campaign/ShootCampaignNow');
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

        // EXECUTE:
        $result = curl_exec($curl);
        if (!$result) {
                return array(
                    'error' => 'Could not complete the API call',
                    'description' => get_curl_error(curl_errno($curl))
                );
        }
        curl_close($curl);
        return array(
            'success' => $result
        );
    }

    public function registerEmail($email_address) {
        //build the array for registering the email
        $data = array();
        $data['Api_Key'] = $this->api_key;
        $data['FromEmail'] = $email_address;
        $data['FromName'] = $email_address;

        $curl = curl_init();
        // OPTIONS:
        curl_setopt($curl, CURLOPT_URL,  $this->email_url.'EmailRegistration/RegisterEmailNow?Api_key='.$this->api_key.'&FromEmail='.$email_address.'&FromName='.$email_address);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, '');
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        // EXECUTE:
        
        $result = curl_exec($curl);
        $result = json_decode($result);

        if (!empty($result['ERROR_MSG'])) {
                return array(
                    'error' => 'Could not complete the API call', 
                    'description' => $result['ERROR_MSG']
                );
        }
        curl_close($curl);
        return array(
            'success' => $result
        );
    }

    public function getXMLForCall($params) {
        $xml = "";

        $xml .= "<root>";
        $xml .= "<UserInfo>";
        $xml .= "<API_Key>{$params['api_key']}</API_Key>";
        $xml .= "<RecipientData>";

        $xml .= "<Recipient>";
        $xml .= "<Email>{$params['to_address']}</Email> ";       
        $xml .= "<AttributeData>";
        $xml .= "</AttributeData> ";
        $xml .= "</Recipient>";

        $xml .= "</RecipientData>";
        $xml .= "<CampaignName>{$params['campaign_name']}</CampaignName> ";
        $xml .= "<Subject>";
        $xml .= "<![CDATA[ {$params['campaign_desc']} ]]>";
        $xml .= "</Subject> ";
        $xml .= "<FromEmailId>{$params['from_email']}</FromEmailId>";
        $xml .= "<ReplyToEmailId>{$params['reply_to_email']}</ReplyToEmailId> ";
        $xml .= "<MessageContent>";
        $xml .= "<![CDATA[ {$params['message_template']} ]]>";
        $xml .= "</MessageContent> ";
        $xml .= "</UserInfo>";
        $xml .= "</root>";

        return $xml;
    }

    function get_curl_error($error_no) {
        $error_codes = array (
            'CURLE_UNSUPPORTED_PROTOCOL',
            'CURLE_FAILED_INIT',
            'CURLE_URL_MALFORMAT', 
            'CURLE_URL_MALFORMAT_USER', 
            'CURLE_COULDNT_RESOLVE_PROXY', 
            'CURLE_COULDNT_RESOLVE_HOST', 
            'CURLE_COULDNT_CONNECT', 
            'CURLE_FTP_WEIRD_SERVER_REPLY',
            'CURLE_REMOTE_ACCESS_DENIED',
            'CURLE_FTP_WEIRD_PASS_REPLY',
            'CURLE_FTP_WEIRD_PASV_REPLY',
            'CURLE_FTP_WEIRD_227_FORMAT',
            'CURLE_FTP_CANT_GET_HOST',
            'CURLE_FTP_COULDNT_SET_TYPE',
            'CURLE_PARTIAL_FILE',
            'CURLE_FTP_COULDNT_RETR_FILE',
            'CURLE_QUOTE_ERROR',
            'CURLE_HTTP_RETURNED_ERROR',
            'CURLE_WRITE_ERROR',
            'CURLE_UPLOAD_FAILED',
            'CURLE_READ_ERROR',
            'CURLE_OUT_OF_MEMORY',
            'CURLE_OPERATION_TIMEDOUT',
            'CURLE_FTP_PORT_FAILED',
            'CURLE_FTP_COULDNT_USE_REST',
            'CURLE_RANGE_ERROR',
            'CURLE_HTTP_POST_ERROR',
            'CURLE_SSL_CONNECT_ERROR',
            'CURLE_BAD_DOWNLOAD_RESUME',
            'CURLE_FILE_COULDNT_READ_FILE',
            'CURLE_LDAP_CANNOT_BIND',
            'CURLE_LDAP_SEARCH_FAILED',
            'CURLE_FUNCTION_NOT_FOUND',
            'CURLE_ABORTED_BY_CALLBACK',
            'CURLE_BAD_FUNCTION_ARGUMENT',
            'CURLE_INTERFACE_FAILED',
            'CURLE_TOO_MANY_REDIRECTS',
            'CURLE_UNKNOWN_TELNET_OPTION',
            'CURLE_TELNET_OPTION_SYNTAX',
            'CURLE_PEER_FAILED_VERIFICATION',
            'CURLE_GOT_NOTHING',
            'CURLE_SSL_ENGINE_NOTFOUND',
            'CURLE_SSL_ENGINE_SETFAILED',
            'CURLE_SEND_ERROR',
            'CURLE_RECV_ERROR',
            'CURLE_SSL_CERTPROBLEM',
            'CURLE_SSL_CIPHER',
            'CURLE_SSL_CACERT',
            'CURLE_BAD_CONTENT_ENCODING',
            'CURLE_LDAP_INVALID_URL',
            'CURLE_FILESIZE_EXCEEDED',
            'CURLE_USE_SSL_FAILED',
            'CURLE_SEND_FAIL_REWIND',
            'CURLE_SSL_ENGINE_INITFAILED',
            'CURLE_LOGIN_DENIED',
            'CURLE_TFTP_NOTFOUND',
            'CURLE_TFTP_PERM',
            'CURLE_REMOTE_DISK_FULL',
            'CURLE_TFTP_ILLEGAL',
            'CURLE_TFTP_UNKNOWNID',
            'CURLE_REMOTE_FILE_EXISTS',
            'CURLE_TFTP_NOSUCHUSER',
            'CURLE_CONV_FAILED',
            'CURLE_CONV_REQD',
            'CURLE_SSL_CACERT_BADFILE',
            'CURLE_REMOTE_FILE_NOT_FOUND',
            'CURLE_SSH',
            'CURLE_SSL_SHUTDOWN_FAILED',
            'CURLE_AGAIN',
            'CURLE_SSL_CRL_BADFILE',
            'CURLE_SSL_ISSUER_ERROR',
            'CURLE_FTP_PRET_FAILED',
            'CURLE_FTP_PRET_FAILED',
            'CURLE_RTSP_CSEQ_ERROR',
            'CURLE_RTSP_SESSION_ERROR',
            'CURLE_FTP_BAD_FILE_LIST',
            'CURLE_CHUNK_FAILED'
        );
        return $error_codes[$error_no];
    }  
}