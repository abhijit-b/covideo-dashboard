<?php 
if (!defined('BASEPATH')) exit('No direct script access allowed');

class DripAPI{

    public function __construct() {
		$this->url = DRIP_API_URL;
        $this->access_token = DRIP_API_TOKEN;
        $this->account_id = DRIP_ACCOUNT_ID;
    }
    		
  	public function createSubscriber( $email = "", $name = "") {
        /* 
        curl -X POST "https://api.getdrip.com/v2/YOUR_ACCOUNT_ID/subscribers" \
            -H 'User-Agent: Your App Name (www.yourapp.com)' \
            -H 'Content-Type: application/json' \
            -u YOUR_API_KEY: \
            -d @- << EOF
            {
            "subscribers": [{
                "email": "john@acme.com",
                "time_zone": "America/Los_Angeles",
                "custom_fields": {
                "shirt_size": "Medium"
                }
            }]
            }
            EOF
        */

        $url = $this->url.$this->account_id.'/subscribers';

        $curl = curl_init();

        // OPTIONS:
		curl_setopt($curl, CURLOPT_URL, $url);
		curl_setopt($curl, CURLOPT_USERAGENT, 'CoVideo Dashboard (at.covideo.in)');
		curl_setopt($curl, CURLOPT_HTTPHEADER, array(
			'Content-Type: application/json'
		));	
    	curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($curl, CURLOPT_USERPWD, $this->access_token);

        $name = explode(' ', $name);
		//setting post variables
        $params = array(
            'subscribers' => array(
                0 => array(
                    'first_name' => $name[0],
                    'last_name' => !empty($name[1]) ? $name[1] : '',
                    'email' => $email,
                    'time_zone' => (date_default_timezone_get()) ? date_default_timezone_get() : 'UTC',
                    'status' => 'active'
                )
            )
        );
        curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($params));

        // EXECUTE:
        $result = curl_exec($curl);
        $result = json_decode($result, true);

		if(isset($result['error'])) {
			$result = array('status' => 'error', 'code' => $result['error']['code'], 'message' => $result['error']['message']);
		}

    	curl_close($curl);
    	return $result;
    }

}