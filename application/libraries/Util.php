<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Util {

    protected $CI;

    public function __construct() {
        $this->CI =& get_instance();
    }

    public function object_to_array($obj) {
        if(is_object($obj)) $obj = (array) $obj;
        if(is_array($obj)) {
            $new = array();
            foreach($obj as $key => $val) {
                $new[$key] = $this->object_to_array($val);
            }
        }
        else $new = $obj;
        return $new;       
    }

    public function object_unique($objects, $type = 'email') {
        $unique_objects = $temp_emails = $temp_numbers = array();
        foreach($objects as $object) {
            if($type == 'sms') {
                if(!in_array($object->mobile_number, $temp_numbers)) {
                    $temp_numbers[]=$object->mobile_number;
                    $unique_objects[] = $object;
                }
            } else {
                if(!in_array($object->email, $temp_emails)) {
                    $temp_emails[]=$object->email;
                    $unique_objects[] = $object;
                }
            }
        }
        return $unique_objects;
    }

    public function get_json($string) {
        // decode the JSON data
        $result = json_decode($string);

        // switch and check possible JSON errors
        switch (json_last_error()) {
            case JSON_ERROR_NONE:
                $error = ''; // JSON is valid // No error has occurred
                break;
            case JSON_ERROR_DEPTH:
                $error = 'The maximum stack depth has been exceeded.';
                break;
            case JSON_ERROR_STATE_MISMATCH:
                $error = 'Invalid or malformed JSON.';
                break;
            case JSON_ERROR_CTRL_CHAR:
                $error = 'Control character error, possibly incorrectly encoded.';
                break;
            case JSON_ERROR_SYNTAX:
                $error = 'Syntax error, malformed JSON.';
                break;
            // PHP >= 5.3.3
            case JSON_ERROR_UTF8:
                $error = 'Malformed UTF-8 characters, possibly incorrectly encoded.';
                break;
            // PHP >= 5.5.0
            case JSON_ERROR_RECURSION:
                $error = 'One or more recursive references in the value to be encoded.';
                break;
            // PHP >= 5.5.0
            case JSON_ERROR_INF_OR_NAN:
                $error = 'One or more NAN or INF values in the value to be encoded.';
                break;
            case JSON_ERROR_UNSUPPORTED_TYPE:
                $error = 'A value of a type that cannot be encoded was given.';
                break;
            default:
                $error = 'Unknown JSON error occured.';
                break;
        }

        if ($error !== '') {
            // throw the Exception or exit // or whatever :)
            return array('status' => 'error', 'message' => $error);
        }

        // everything is OK
        return array('status' => 'success', 'message' => $result);
    }

    public function array_flatten($array = null) {
        $result = array();
    
        if (!is_array($array)) {
            $array = func_get_args();
        }
    
        foreach ($array as $key => $value) {
            if (is_array($value)) {
                $result = array_merge($result, $this->array_flatten($value));
            } else {
                $result = array_merge($result, array($key => $value));
            }
        }
    
        return $result;
    }

    public function getDistance($latitude1, $longitude1, $latitude2, $longitude2) {
        $earth_radius = 6371;

        $dLat = deg2rad($latitude2 - $latitude1);
        $dLon = deg2rad($longitude2 - $longitude1);

        $a = sin($dLat/2) * sin($dLat/2) + cos(deg2rad($latitude1)) * cos(deg2rad($latitude2)) * sin($dLon/2) * sin($dLon/2);
        $c = 2 * asin(sqrt($a));
        $d = $earth_radius * $c;

        return $d;
    }

    public function is_english($str) {
	    if (strlen($str) != strlen(utf8_decode($str))) {
		    return false;
        } else {
		    return true;
	    }
    }

    public function search($array, $key, $value) {
        $results = array();
        $this->search_r($array, $key, $value, $results);
        return $results;
    }

    public function search_r($array, $key, $value, &$results) {
        if (!is_array($array)) {
            return;
        }

        if (isset($array[$key]) && $array[$key] == $value) {
            $results[] = $array;
        }

        foreach ($array as $subarray) {
            $this->search_r($subarray, $key, $value, $results);
        }
    }

    public function array_depth(array $array) {
        $max_depth = 1;
        foreach ($array as $value) {
            if (is_array($value)) {
                $depth = $this->array_depth($value) + 1;
                if ($depth > $max_depth) {
                    $max_depth = $depth;
                }
            }
        }
        return $max_depth;
    }

    public function sanitize_mobile_number($mobile_number) {

        $mobile_number = str_replace('+91','',$mobile_number); // remove country code
        $mobile_number = str_replace('*','',$mobile_number); // remove special chars
        $mobile_number = str_replace(' ','',$mobile_number); // remove spaces in between the numbers eg. 98989 89898
        $mobile_number = str_replace('-','',$mobile_number); // remove hyphens in between the numbers eg. 98989-89898
        $mobile_number = str_replace('\'','',$mobile_number); // remove hyphens in between the numbers eg. 98989-89898

        if(preg_match('/\t/', $mobile_number) !== false) {
            $parts = preg_split('/\s+/', $mobile_number);
            $mobile_number = implode('/', $parts);
        }

        if(strpos($mobile_number, '/') !== false) {
            $numbers = explode('/',$mobile_number);
            $mobile_number = $numbers[0];
        }
        if(strpos($mobile_number, ',') !== false) {
            $numbers = explode(',',$mobile_number);
            $mobile_number = $numbers[0];
        }

        $mobile_number = (int)$mobile_number; // removing starting 0
        $mobile_regex = '/^[6-9][0-9]{9}$/';
        $mobile_number = preg_grep($mobile_regex, array($mobile_number));
        return (is_array($mobile_number) && !empty($mobile_number)) ? $mobile_number[0] : FALSE;
    }

    public function replace_template_vars($item, $bot, $message) {
        $bot_url = (!empty($bot['url_live'])) ? $bot['url_live'] : $bot['url_test']; 
        return str_replace(
            array('{customer-name}','{policy-number}', '{link}', '{code}'),
            array($item->name, $item->reference_id, $bot_url.'/'.$item->code, $item->code),
            $message);
    }

    public function file_download($path, $file) {            
        $full_file_path = $path.$file;
        // Make sure program execution doesn't time out
        // Set maximum script execution time in seconds (0 means no limit)
        set_time_limit(0);
        if(!is_file($full_file_path)) {
            log_message('error', 'File does not exist.');
        }
        $file_size = filesize($full_file_path);
        $mime_type = mime_content_type($full_file_path);

        if ($mime_type == '') {
            $mime_type = "application/force-download";
        }
    
        // set headers
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
        header("Cache-Control: public");
        header("Content-Description: File Transfer");
        header("Content-Type: $mime_type");
        header("Content-Disposition: attachment; filename=\"$file\"");
        header("Content-Transfer-Encoding: binary");
        header("Content-Length: " . $file_size);
    
        // download
        //@readfile($full_file_path);
        $file = @fopen($full_file_path,"rb");
        if ($file) {
            while(!feof($file)) {
                print(fread($file, 1024*8));
                flush();
                if (connection_status()!=0) {
                    @fclose($file);
                    die();
                }
            }
            @fclose($file);
        }
    }

    public function zip_file_download($zip, $path, $files) {
        $file_data = array();
        foreach($files as $file){
            $file_data[$file] = file_get_contents($path.$file);
        }
        $zip->add_data($file_data);
        $zip->archive(FCPATH.'downloads/documents.zip');
        $zip->download('documents.zip');
    }
}