<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

class Googlemapsapi {

    public function __construct() {
        $this->private_key = GOOGLE_MAPS_API_KEY;
        $this->api_url = 'https://maps.googleapis.com/maps/api/geocode/json';
    }
    	
    public function get_coordinates($address) {
        $data['key'] = $this->private_key;
        $data['address'] = $address;

        $query_string = http_build_query($data);
        $ch = curl_init($this->api_url . '?' . $query_string);

        curl_setopt_array($ch, array(
            CURLOPT_RETURNTRANSFER    =>  true,
            CURLOPT_FOLLOWLOCATION    =>  true,
            CURLOPT_MAXREDIRS         =>  10,
            CURLOPT_TIMEOUT           =>  30,
            CURLOPT_CUSTOMREQUEST     =>  'GET',
          ));
      
        $response = curl_exec($ch);
        curl_close($ch);
        $response = json_decode($response, true);
        if(!isset($response['error_message'])) {
            return array('lat'=> $response['results'][0]['geometry']['location']['lat'], 'lon' => $response['results'][0]['geometry']['location']['lng']);
        }
        return array();
    }
}