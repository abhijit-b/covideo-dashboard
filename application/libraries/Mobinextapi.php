<?php 

if (!defined('BASEPATH')) exit('No direct script access allowed');

class Mobinextapi {

    public function __construct() {
		$this->url = MOBINEXT_SMS_URL;
		$this->xml_url = MOBINEXT_XML_URL;
		$this->credit_url = MOBINEXT_CREDIT_URL;
		$this->user = MOBINEXT_USER;
		$this->password = MOBINEXT_PASSWORD;
		$this->sender_id = MOBINEXT_SENDER_ID; // sender ID
		$this->fl = 0; // if flash message then 1 or else 0
		$this->gwid = 2; //gwid: 2 (its for Transactions route.)
		$this->xml = "<SmsQueue>
						<Account>
							<User>{$this->user}</User>
							<Password>{$this->password}</Password>
						</Account>
						<MessageData>
							<SenderId>{$this->sender_id}</SenderId>
							<Gwid>{$this->gwid}</Gwid>
							<DataCoding>8</DataCoding>
						</MessageData>
						<Messages>
							<Message>
								<Number>{number}</Number>
								<Text><![CDATA[{message}]]></Text>
							</Message>
						</Messages>
					</SmsQueue>";
    }
    		
  	public function sendSms($number, $message, $method = 'POST') {
		$xml = $this->xml;
		$xml = str_replace('{number}', $number, $xml);
		$xml = str_replace('{message}', $message, $xml);
		if($this->is_english($message)) {
			$xml = str_replace('<DataCoding>8</DataCoding>', '<DataCoding>0</DataCoding>', $xml);
		}
		
    	$curl = curl_init();

		switch ($method){
			case "POST":
				curl_setopt($curl, CURLOPT_POST, 1);
				curl_setopt($curl, CURLOPT_POSTFIELDS, $xml);
        		break;
			default:
        		if ($data)
					$this->url = sprintf("%s?%s", $this->url, http_build_query($data));
		 }

		// OPTIONS:
		curl_setopt($curl, CURLOPT_URL, $this->xml_url);
		curl_setopt($curl, CURLOPT_HTTPHEADER, array(  
			'Content-Type: application/xml'
		));	
    	curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
		//setting post variables

        // EXECUTE:
		$result = curl_exec($curl);
		$result = json_decode(json_encode(simplexml_load_string($result)), true);
		
		$responses = array();
		if(!empty($result['MessageData'])) {
			if($this->array_depth($result['MessageData']['Messages']['MessageParts']['MessagePart']) == 1) {
				$responses[] = array(
					'status' => 'success', 
					'message_id' => $result['MessageData']['Messages']['MessageParts']['MessagePart']['MsgId'], 
					'text' => $result['MessageData']['Messages']['MessageParts']['MessagePart']['Text']
				);
			} else {
				foreach($result['MessageData']['Messages']['MessageParts']['MessagePart'] as $message) {
					$responses[] = array(
						'status' => 'success', 
						'message_id' => $message['MsgId'], 
						'text' => $message['Text']
					);
				}
			}
		} else {
			$responses[] = array('status' => 'error', 'code' => $result['ErrorCode'], 'message' => $result['ErrorMessage']);
		}

    	curl_close($curl);
    	return $responses;
  	}

  	public function getDeliveryStatus($messageid) {
		//http://vas.mobinext.in/vendorsms/checkdelivery.aspx?user=KYCBOT&password=kyc@123&messageid=messageid
		$data = array();
		$data['user'] = $this->user;
		$data['password'] = $this->password;
		$data['messageid'] = $messageid;

		$curl = curl_init();

		if ($data) {
			$url = sprintf("%s?%s", $this->url, http_build_query($data));
		} 
		
		// OPTIONS:
		curl_setopt($curl, CURLOPT_URL, $url);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);

		// EXECUTE:
		$result = curl_exec($curl);
		if (!$result) {
			return array('error' => 'Could not complete the API call');
		}
		curl_close($curl);
		return $result;
	}
  
	public function getCreditBalance() {
		//http://vas.mobinext.in/vendorsms/CheckBalance.aspx?user=demo&password=demo
		$data = array();
		$data['user'] = $this->user;
		$data['password'] = $this->password;

		$curl = curl_init();

		if ($data) {
			$url = sprintf("%s?%s", $this->credit_url, http_build_query($data));
		} 
		
		// OPTIONS:
		curl_setopt($curl, CURLOPT_URL, $url);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);

		// EXECUTE:
		$result = curl_exec($curl);
		curl_close($curl);
		
		$result = substr($result , strpos($result, '#') + 1); 
		$result = explode('|', $result);
		if (!$result) {
				$result = array('promotional' => 0, 'transactional' => 0);
		} else {
				$result = array('promotional' => explode(':', $result[0])[1], 'transactional' => explode(':', $result[1])[1]);
		}
		return $result;
	}

	function is_english($str) {
	    if (strlen($str) != strlen(utf8_decode($str))) {
		    return false;
        } else {
		    return true;
	    }
	}
	
	public function array_depth(array $array) {
        $max_depth = 1;
        foreach ($array as $value) {
            if (is_array($value)) {
                $depth = $this->array_depth($value) + 1;
                if ($depth > $max_depth) {
                    $max_depth = $depth;
                }
            }
        }
        return $max_depth;
    }

}


/*
Send SMS API :
http://vas.mobinext.in/vendorsms/pushsms.aspx?user=KYCBOT&password=kyc@123&msisdn=919819209855&sid=KYCBOT&msg=test%20messagethisworks&fl=0&gwid=2
Params :
for multiple messages : add comma separated mobile numbers

Response :
{
  "ErrorCode": "000",
  "ErrorMessage": "Success",
  "JobId": "5262896",
  "MessageData": [
    {
      "Number": "919819209855",
      "MessageParts": [
        {
          "MsgId": "919819209855-85ca5394035a4f32949740f8711ba429",
          "PartId": 1,
          "Text": "test message"
        }
      ]
    }
  ]
}

// 20181212211207
// http://vas.mobinext.in/vendorsms/pushsms.aspx?user=KYCBOT&password=kyc@123&msisdn=91981er09855&sid=KYCBOT&msg=test%20messagethisworks&fl=0&gwid=2
Response :
{
  "ErrorCode": "13",
  "ErrorMessage": "Invalid mobile numbers",
  "JobId": null,
  "MessageData": null
}


Check Delivery API :
http://vas.mobinext.in/vendorsms/checkdelivery.aspx?user=demo&password=demo&messageid=messageid
Response : #DELIVRD


*/