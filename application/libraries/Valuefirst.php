<?php 

if (!defined('BASEPATH')) exit('No direct script access allowed');

class Valuefirst {

    public function __construct() {
		$this->url = VALUEFIRST_SMS_URL;
        $this->user = VALUEFIRST_USER;
        $this->password = VALUEFIRST_PASSWORD;
        $this->sender_id = VALUEFIRST_SENDER_ID;
        $this->dlr_url = $_SERVER['REQUEST_SCHEME'].'://'.$_SERVER['HTTP_HOST']."/sms/dlr-value-first?&#77;&#101;&#115;&#115;&#97;&#103;&#101;&#73;&#100;&#61;&#37;&#53;&amp;&#77;&#101;&#115;&#115;&#97;&#103;&#101;&#83;&#116;&#97;&#116;&#117;&#115;&#61;&#37;&#49;&#54;&amp;&#99;&#105;&#114;&#99;&#108;&#101;&#61;&#37;&#56;&amp;&#111;&#112;&#101;&#114;&#97;&#116;&#111;&#114;&#61;&#37;&#57;&amp;&#115;&#116;&#97;&#116;&#117;&#115;&#61;&#37;&#49;&#51;&amp;&#100;&#101;&#108;&#105;&#118;&#101;&#114;&#101;&#100;&#95;&#100;&#97;&#116;&#101;&#61;&#37;&#51;";
        $this->xml = '<?xml version="1.0" encoding="ISO-8859-1"?><!DOCTYPE MESSAGE SYSTEM "http://127.0.0.1:80/psms/dtd/messagev12.dtd"><MESSAGE VER="1.2"><USER USERNAME="{$this->user}" PASSWORD="{$this->password}" /><DLR URL="{dlr_url}"></DLR><SMS UDH="0" CODING="1" TEXT="{message}" ID="1"><ADDRESS FROM="{$this->sender_id}" TO="{number}" SEQ="1" /></SMS></MESSAGE>';
        $this->credit_xml = '<?xml version="1.0" encoding="ISO-8859-1"?><!DOCTYPE REQUESTCREDIT SYSTEM "http://127.0.0.1:80/psms/dtd/requestcredit.dtd"><REQUESTCREDIT USERNAME="{$this->user}" PASSWORD="{$this->password}"></REQUESTCREDIT>';
        $this->status_xml = '<?xml version="1.0" encoding="ISO-8859-1"?><!DOCTYPE STATUSREQUEST SYSTEM "http://127.0.0.1:80/psms/dtd/requeststatus.dtd"><STATUSREQUEST><USER USERNAME="{$this->user}" PASSWORD="{$this->password}"/><GUID GUID="kj3nf491470031f4100141y9n4pxeENTERUX" /></STATUSREQUEST>';
        $this->xml_header = '<?xml version="1.0" encoding="ISO-8859-1"?><!DOCTYPE MESSAGE SYSTEM "http://127.0.0.1:80/psms/dtd/messagev12.dtd"><MESSAGE VER="1.2"><USER USERNAME="{$this->user}" PASSWORD="{$this->password}" /><DLR URL="{dlr_url}"></DLR>';
        $this->xml_body = '<SMS UDH="0" CODING="1" TEXT="{message}" ID="{$id}"><ADDRESS FROM="{$this->sender_id}" TO="{number}" SEQ="1" /></SMS>';
        $this->xml_footer = '</MESSAGE>';
    }
    		
    public function sendSms($number, $message, $sender_id = "", $method = 'POST') {
        $xml = $this->xml;

        // replace special chars in the message
        $message = htmlspecialchars($message, ENT_QUOTES);
        // $message .= random_string('alnum', 12);

        // replace vars in the xml message
        $sender_id = !empty($sender_id) ? $sender_id : $this->sender_id;
        $search = array('{$this->user}','{$this->password}','{$this->sender_id}','{message}','{number}', '{dlr_url}');
        $replace = array($this->user, $this->password, $sender_id , $message, $number, $this->dlr_url);
        $xml = str_replace($search, $replace, $xml);
        
		if(!$this->is_english($message)) {
			$xml = str_replace('CODING="1"', 'CODING="2"', $xml);
        }
        $xml = urlencode($xml);
        //echo htmlentities($xml)."\n";
        $data = 'action=send&data='.$xml;

        $curl = curl_init();

		switch ($method){
			case "POST":
				curl_setopt($curl, CURLOPT_POST, 1);
				curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
			   break;
			default:
			   	if ($data)
					$this->url = sprintf("%s?%s", $this->url, http_build_query($data));
		 }

		// OPTIONS:
		curl_setopt($curl, CURLOPT_URL, $this->url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);

        // EXECUTE:
        $result = curl_exec($curl);
        curl_close($curl);
        
        $XML2Array = new XML2Array();
        $result = $XML2Array->parseXML($result,'');

        if(isset($result['messageack']['guid'])) {
            foreach($result['messageack']['guid'] as $response) {
                if(array_key_exists('error', $response)) {
                    $api_response[] = array(
                        'status' => 'error',
                        'code' => $response['error']['CODE'],
                        'mobile_number' => $number,
                        'message' => $this->get_error_message($response['error']['CODE'])
                    );
                } else {
                    $api_response[] = array(
                        'status' => 'success',
                        'message_id' => $response['GUID'],
                        'mobile_number' => $number,
                        'text' => htmlspecialchars_decode($message, ENT_QUOTES),
                        'sent_on' => strtotime($response['SUBMITDATE'])
                    );
                }
            } 
        } else {
            $api_response[] = array('status' => 'error', 'message' => $this->get_error_message($result['messageack']['err']['CODE']));
        }
        return $api_response;
    }

    public function sendBulkSms($sms_info, $method = 'POST') {
        $xml_header = $this->xml_header;
        // replace vars in the xml header
        $search = array('{$this->user}','{$this->password}', '{dlr_url}');
        $replace = array($this->user, $this->password, $this->dlr_url);
        $xml_header = str_replace($search, $replace, $xml_header);

        $xml_body = '';
        $messages = array();
        foreach($sms_info as $key => $info) {
            // replace special chars in the message
            $message = htmlspecialchars($info['message'], ENT_QUOTES);
            // $message .= random_string('alnum', 12);
            $search = array('{$this->sender_id}','{message}','{number}','{$id}');
            $replace = array($this->sender_id , $message, $info['mobile_number'], ($key + 1));
            $xml_body .= str_replace($search, $replace, $this->xml_body);
            $messages[] = $message;
        }

        $xml = $xml_header.$xml_body.$this->xml_footer;
        if(!$this->is_english($message)) {
			$xml = str_replace('CODING="1"', 'CODING="2"', $xml);
        }

        $xml = urlencode($xml);
        $data = 'action=send&data='.$xml;

        $curl = curl_init();

		switch ($method){
			case "POST":
				curl_setopt($curl, CURLOPT_POST, 1);
				curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
			   break;
			default:
			   	if ($data)
					$this->url = sprintf("%s?%s", $this->url, http_build_query($data));
		 }

		// OPTIONS:
		curl_setopt($curl, CURLOPT_URL, $this->url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);

        // EXECUTE:
        $result = curl_exec($curl);
        curl_close($curl);
        
        $XML2Array = new XML2Array();
        $result = $XML2Array->parseXML($result,'');
        if(isset($result['messageack']['guid'])) {
            foreach($result['messageack']['guid'] as $key => $response) {
                if(array_key_exists('error', $response)) {
                    $api_response[] = array(
                        'status' => 'error',
                        'code' => $response['error']['CODE'],
                        'message' => $this->get_error_message($response['error']['CODE'])
                    );
                } else {
                    $api_response[] = array(
                        'status' => 'success',
                        'message_id' => $response['GUID'],
                        'text' => htmlspecialchars_decode($messages[$key - 1], ENT_QUOTES),
                        'sent_on' => strtotime($response['SUBMITDATE'])
                    );
                }
            } 
        } else {
            $api_response[] = array('status' => 'error', 'message' => $this->get_error_message($result['messageack']['err']['CODE']));
        }
        return $api_response;
    }

    public function getCreditBalance() {

        $search = array('{$this->user}','{$this->password}');
        $replace = array($this->user, $this->password);
        $xml = str_replace($search, $replace, $this->credit_xml);

        $data = 'action=credits&data='.$xml;

		$curl = curl_init();
        
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data);

		// OPTIONS:
		curl_setopt($curl, CURLOPT_URL, $this->url);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);

		// EXECUTE:
        $result = curl_exec($curl);
        curl_close($curl);

        $XML2Array = new XML2Array();
        $result = $XML2Array->parseXML($result,'');

        if (!$result) {
            $result = array('promotional' => 0, 'transactional' => 0);
        } else {
            $result = array('promotional' => 0, 'transactional' => $result['sms-credit']['credit']['LIMIT'] - $result['sms-credit']['credit']['USED']);
        }
		return $result;
	}

    public function getDeliveryStatus() {
        $search = array('{$this->user}','{$this->password}');
        $replace = array($this->user, $this->password);
        $xml = str_replace($search, $replace, $this->status_xml);


        $data = 'action=status&data='.urlencode($xml);

		$curl = curl_init();
        
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data);

		// OPTIONS:
		curl_setopt($curl, CURLOPT_URL, $this->url);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);

		// EXECUTE:
        $result = curl_exec($curl);
        curl_close($curl);

        $XML2Array = new XML2Array();
        $result = $XML2Array->parseXML($result,'');
        
        if (empty($result)) {
            $result = array(
                'status' => 'error',
                'message' => 'no records found'
            );
        } else {
            $result = array(
                'status' => 'success',
                'message' => $this->get_error_message($result['statusack']['guid']['status']['ERR']),
                'delivered_at' => strtotime($result['statusack']['guid']['status']['DONEDATE'])
            );
        }
		return $result;
    }

	function is_english($str) {
	    if (strlen($str) != strlen(utf8_decode($str))) {
		    return false;
        } else {
		    return true;
	    }
    }

    function get_error_message($error_number) {
        $errors = array('52992' => 'Username / Password incorrect',
                        '57089' => 'Contract expired',
                        '57090' => 'User Credit expired',
                        '57091' => 'User disabled',
                        '65280' => 'Service is temporarily unavailable',
                        '65535' => 'The specified message does not conform to DTD',
                        '28673' => 'Destination number not numeric',
                        '28674' => 'Destination number empty',
                        '28675' => 'Sender address empty',
                        '28676' => 'Template Mismatch',
                        '28677' => 'UDH is invalid / SPAM message',
                        '28678' => 'Coding is invalid',
                        '28679' => 'SMS text is empty',
                        '28680' => 'Invalid sender ID',
                        '28681' => 'Invalid message, Duplicate message, Submit failed',
                        '28682' => 'Invalid Receiver ID',
                        '28683' => 'Invalid Date time for message Schedule',
                        '8448' => 'Message delivered successfully',
                        '8449' => 'Message failed',
                        '8450' => 'Message ID is invalid',
                        '13568' => 'Command Completed Successfully',
                        '13569' => 'Cannot update/delete schedule since it has already been processed',
                        '13570' => 'Cannot update schedule since the new date-time parameter is incorrect.',
                        '13571' => 'Invalid SMS ID/GUID',
                        '13572' => 'Invalid Status type for schedule search query',
                        '13573' => 'Invalid date time parameter for schedule search query',
                        '13574' => 'Invalid GUID for GUID search query',
                        '13575' => 'Invalid command action');
        return $errors[$error_number];
    }
}

