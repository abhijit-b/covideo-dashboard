  
  $("#add").click(function() {
    location.href = "/rooms/add";
  });
    
  $("#delete").click(function(){
    $('#error-message').addClass('hidden');
    let count = 0;
    let userid = 0;
    $("input:checkbox:checked").each(function () {
      count++;
      userid = $(this).val();
    });
    if(count == 0 && userid == 0) {
      $('#error-message').removeClass('hidden');
    } else {
      $('#delete-confirmation-message').removeClass('hidden');
    }
  });
  
  $('#delete-confirmation').click(function() {
    let userid = 0;
    userid = $("input:checkbox:checked").val();
    $.ajax({
      url: "/rooms/delete",
      type: "POST",
      data: {"roomid" : JSON.stringify(roomid)},
      dataType: "json",
      success: function(data) {
        if(data.success){
          window.location.href = '/rooms/list';
        }
      },
      error: function(e) {
      }          
    });
  });
  