
window.fbAsyncInit = function() {
    FB.init({
        appId      : '551201092469645',
        cookie     : true,            // Enable cookies to allow the server to access the session.
        xfbml      : true,            // Parse social plugins on this webpage.
        version    : 'v6.0'           // Use this Graph API version for this call.
    });
};

(function(d, s, id) {  // Load the SDK asynchronously
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "https://connect.facebook.net/en_US/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));

function getUserInfo() {
    FB.api('/me', {"fields":"name,email,picture"} ,function(response) {
        submitSignupForm(response);
    });
}

function submitSignupForm(userInfo) {
    if($('#signup').length > 0) {
        $('#signup input[name=signuptype]').val('fb');
        $('#signup input[name=userid]').val(userInfo.id);
        $('#signup input[name=fullname]').val(userInfo.name);
        $('#signup input[name=profileimg]').val(userInfo.picture.data.url);
        $('#signup input[name=email]').val(userInfo.email);
        $('#signup').submit();
    } else {
        $('#login input[name=logintype]').val('fb');
        $('#login input[name=userid]').val(userInfo.id);
        $('#login input[name=fullname]').val(userInfo.name);
        $('#login input[name=profileimg]').val(userInfo.picture.data.url);
        $('#login input[name=email]').val(userInfo.email);
        $('#login').submit();
    }
}
function fb_login(){
    FB.getLoginStatus(function(response) {
        if (response.status === 'connected') {
            // The user is logged in and has authenticated your
            // app, and response.authResponse supplies
            // the user's ID, a valid access token, a signed
            // request, and the time the access token 
            // and signed request each expire.
            if (response.authResponse) {
                getUserInfo();
            }
        } else if (response.status === 'not_authorized') {
            // The user hasn't authorized your application.  They
            // must click the Login button, or you must call FB.login
            // in response to a user gesture, to launch a login dialog.
            FB.login(function(response) {
                if (response.authResponse) {
                    getUserInfo();
                } 
            }, {scope : "email,public_profile"});

        } else {
            // The user isn't logged in to Facebook. You can launch a
            // login dialog with a user gesture, but the user may have
            // to log in to Facebook before authorizing your application.
            FB.login(function(response) {
                getUserInfo();
            }, {scope : "email,public_profile"});

        }
    });
}
