
$("#add").click(function() {
  location.href = "/user/add";
});

$("#edit").click(function(){
  $('#error-message').addClass('hidden');
  let count = 0;
  let userid = 0;
  $("input:checkbox:checked").each(function () {
    count++;
    userid = $(this).val();
  });
  if(count == 0 && userid == 0) {
    $('#error-message').removeClass('hidden');
  } else {
    location.href = '/user/edit/' + userid;
  }
});

$("#view").click(function(){
  $('#error-message').addClass('hidden');
  let count = 0;
  let userid = 0;
  $("input:checkbox:checked").each(function () {
    count++;
    userid = $(this).val();
  });
  if(count == 0 && userid == 0) {
    $('#error-message').removeClass('hidden');
  } else {
    location.href = '/user/view/' + userid;
  }
});

$("#delete").click(function(){
  $('#error-message').addClass('hidden');
  let count = 0;
  let userid = 0;
  $("input:checkbox:checked").each(function () {
    count++;
    userid = $(this).val();
  });
  if(count == 0 && userid == 0) {
    $('#error-message').removeClass('hidden');
  } else {
    $('#delete-confirmation-message').removeClass('hidden');
  }
});

$('#delete-confirmation').click(function() {
  let userid = 0;
  userid = $("input:checkbox:checked").val();
  $.ajax({
    url: "/user/delete",
    type: "POST",
    data: {"userid" : JSON.stringify(userid)},
    dataType: "json",
    success: function(data) {
      if(data.success){
        window.location.href = '/user';
      }
    },
    error: function(e) {
    }          
  });
});
