  $("#add").click(function() {
    location.href = "/settings/add";
  });
  
  $("#edit").click(function(){
    $('#error-message').addClass('hidden');
    let count = 0;
    let settingid = 0;
    $("input:checkbox:checked").each(function () {
      count++;
      settingid = $(this).val();
    });
    if(count == 0 && settingid == 0) {
      $('#error-message').removeClass('hidden');
    } else {
      location.href = '/settings/edit/' + settingid;
    }
  });
  
  $("#view").click(function(){
    $('#error-message').addClass('hidden');
    let count = 0;
    let settingid = 0;
    $("input:checkbox:checked").each(function () {
      count++;
      settingid = $(this).val();
    });
    if(count == 0 && settingid == 0) {
      $('#error-message').removeClass('hidden');
    } else {
      location.href = '/settings/view/' + settingid;
    }
  });
  
  $("#delete").click(function(){
    $('#error-message').addClass('hidden');
    let count = 0;
    let userid = 0;
    $("input:checkbox:checked").each(function () {
      count++;
      settingid = $(this).val();
    });
    if(count == 0 && userid == 0) {
      $('#error-message').removeClass('hidden');
    } else {
      $('#delete-confirmation-message').removeClass('hidden');
    }
  });
  
  $('#delete-confirmation').click(function() {
    let userid = 0;
    userid = $("input:checkbox:checked").val();
    $.ajax({
      url: "/settings/delete",
      type: "POST",
      data: {"settingid" : JSON.stringify(userid)},
      dataType: "json",
      success: function(data) {
        if(data.success){
          window.location.href = '/setting';
        }
      },
      error: function(e) {
      }          
    });
  });
  
  $('#cancel').click(function() {
    $('#delete-confirmation-message').addClass('hidden');
  });
  
$('#generate-key').on('click', function() {
	var report_button = $(this);
	var userid = $('input[name="user_id"]').val();
	if(userid != 0) {
		report_button.button('loading');
		$.ajax({
			url: "/settings/generate-api-key",
			type: "POST",
			data: {"user_id" : userid},
			dataType: "json",
			success: function(data) {
				if(data.status == 'success') {
					$('#api-key').val(data.api_key);
					report_button.button('reset');
				}
			},
			error: function(e) {
			}
		});
	}
});